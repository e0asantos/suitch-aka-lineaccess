require 'test_helper'

class AnetLogsControllerTest < ActionController::TestCase
  setup do
    @anet_log = anet_logs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:anet_logs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create anet_log" do
    assert_difference('AnetLog.count') do
      post :create, anet_log: { arduino_id: @anet_log.arduino_id, connection_type: @anet_log.connection_type, id: @anet_log.id }
    end

    assert_redirected_to anet_log_path(assigns(:anet_log))
  end

  test "should show anet_log" do
    get :show, id: @anet_log
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @anet_log
    assert_response :success
  end

  test "should update anet_log" do
    put :update, id: @anet_log, anet_log: { arduino_id: @anet_log.arduino_id, connection_type: @anet_log.connection_type, id: @anet_log.id }
    assert_redirected_to anet_log_path(assigns(:anet_log))
  end

  test "should destroy anet_log" do
    assert_difference('AnetLog.count', -1) do
      delete :destroy, id: @anet_log
    end

    assert_redirected_to anet_logs_path
  end
end
