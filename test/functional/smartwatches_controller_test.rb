require 'test_helper'

class SmartwatchesControllerTest < ActionController::TestCase
  setup do
    @smartwatch = smartwatches(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:smartwatches)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create smartwatch" do
    assert_difference('Smartwatch.count') do
      post :create, smartwatch: { id: @smartwatch.id, sys_user_id: @smartwatch.sys_user_id, token: @smartwatch.token, watch_type: @smartwatch.watch_type }
    end

    assert_redirected_to smartwatch_path(assigns(:smartwatch))
  end

  test "should show smartwatch" do
    get :show, id: @smartwatch
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @smartwatch
    assert_response :success
  end

  test "should update smartwatch" do
    put :update, id: @smartwatch, smartwatch: { id: @smartwatch.id, sys_user_id: @smartwatch.sys_user_id, token: @smartwatch.token, watch_type: @smartwatch.watch_type }
    assert_redirected_to smartwatch_path(assigns(:smartwatch))
  end

  test "should destroy smartwatch" do
    assert_difference('Smartwatch.count', -1) do
      delete :destroy, id: @smartwatch
    end

    assert_redirected_to smartwatches_path
  end
end
