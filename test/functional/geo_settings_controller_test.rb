require 'test_helper'

class GeoSettingsControllerTest < ActionController::TestCase
  setup do
    @geo_setting = geo_settings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:geo_settings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create geo_setting" do
    assert_difference('GeoSetting.count') do
      post :create, geo_setting: { earthquakes: @geo_setting.earthquakes, id: @geo_setting.id, user_id: @geo_setting.user_id, volcano: @geo_setting.volcano, water: @geo_setting.water }
    end

    assert_redirected_to geo_setting_path(assigns(:geo_setting))
  end

  test "should show geo_setting" do
    get :show, id: @geo_setting
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @geo_setting
    assert_response :success
  end

  test "should update geo_setting" do
    put :update, id: @geo_setting, geo_setting: { earthquakes: @geo_setting.earthquakes, id: @geo_setting.id, user_id: @geo_setting.user_id, volcano: @geo_setting.volcano, water: @geo_setting.water }
    assert_redirected_to geo_setting_path(assigns(:geo_setting))
  end

  test "should destroy geo_setting" do
    assert_difference('GeoSetting.count', -1) do
      delete :destroy, id: @geo_setting
    end

    assert_redirected_to geo_settings_path
  end
end
