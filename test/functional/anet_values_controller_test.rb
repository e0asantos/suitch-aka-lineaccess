require 'test_helper'

class AnetValuesControllerTest < ActionController::TestCase
  setup do
    @anet_value = anet_values(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:anet_values)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create anet_value" do
    assert_difference('AnetValue.count') do
      post :create, anet_value: { arduino_id: @anet_value.arduino_id, command_id: @anet_value.command_id, id: @anet_value.id, value: @anet_value.value }
    end

    assert_redirected_to anet_value_path(assigns(:anet_value))
  end

  test "should show anet_value" do
    get :show, id: @anet_value
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @anet_value
    assert_response :success
  end

  test "should update anet_value" do
    put :update, id: @anet_value, anet_value: { arduino_id: @anet_value.arduino_id, command_id: @anet_value.command_id, id: @anet_value.id, value: @anet_value.value }
    assert_redirected_to anet_value_path(assigns(:anet_value))
  end

  test "should destroy anet_value" do
    assert_difference('AnetValue.count', -1) do
      delete :destroy, id: @anet_value
    end

    assert_redirected_to anet_values_path
  end
end
