require 'test_helper'

class SysUserPhonesControllerTest < ActionController::TestCase
  setup do
    @sys_user_phone = sys_user_phones(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sys_user_phones)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sys_user_phone" do
    assert_difference('SysUserPhone.count') do
      post :create, sys_user_phone: { number: @sys_user_phone.number, sys_phone_type: @sys_user_phone.sys_phone_type, sys_user: @sys_user_phone.sys_user }
    end

    assert_redirected_to sys_user_phone_path(assigns(:sys_user_phone))
  end

  test "should show sys_user_phone" do
    get :show, id: @sys_user_phone
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sys_user_phone
    assert_response :success
  end

  test "should update sys_user_phone" do
    put :update, id: @sys_user_phone, sys_user_phone: { number: @sys_user_phone.number, sys_phone_type: @sys_user_phone.sys_phone_type, sys_user: @sys_user_phone.sys_user }
    assert_redirected_to sys_user_phone_path(assigns(:sys_user_phone))
  end

  test "should destroy sys_user_phone" do
    assert_difference('SysUserPhone.count', -1) do
      delete :destroy, id: @sys_user_phone
    end

    assert_redirected_to sys_user_phones_path
  end
end
