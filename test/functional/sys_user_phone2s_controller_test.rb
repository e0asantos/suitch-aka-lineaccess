require 'test_helper'

class SysUserPhone2sControllerTest < ActionController::TestCase
  setup do
    @sys_user_phone2 = sys_user_phone2s(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sys_user_phone2s)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sys_user_phone2" do
    assert_difference('SysUserPhone2.count') do
      post :create, sys_user_phone2: { id: @sys_user_phone2.id, number: @sys_user_phone2.number, removed: @sys_user_phone2.removed, sys_phone_type: @sys_user_phone2.sys_phone_type, sys_user: @sys_user_phone2.sys_user }
    end

    assert_redirected_to sys_user_phone2_path(assigns(:sys_user_phone2))
  end

  test "should show sys_user_phone2" do
    get :show, id: @sys_user_phone2
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sys_user_phone2
    assert_response :success
  end

  test "should update sys_user_phone2" do
    put :update, id: @sys_user_phone2, sys_user_phone2: { id: @sys_user_phone2.id, number: @sys_user_phone2.number, removed: @sys_user_phone2.removed, sys_phone_type: @sys_user_phone2.sys_phone_type, sys_user: @sys_user_phone2.sys_user }
    assert_redirected_to sys_user_phone2_path(assigns(:sys_user_phone2))
  end

  test "should destroy sys_user_phone2" do
    assert_difference('SysUserPhone2.count', -1) do
      delete :destroy, id: @sys_user_phone2
    end

    assert_redirected_to sys_user_phone2s_path
  end
end
