require 'test_helper'

class SystemActivitiesControllerTest < ActionController::TestCase
  setup do
    @system_activity = system_activities(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:system_activities)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create system_activity" do
    assert_difference('SystemActivity.count') do
      post :create, system_activity: { command: @system_activity.command, device_token: @system_activity.device_token, raw_content: @system_activity.raw_content, value: @system_activity.value, virtual_assistant: @system_activity.virtual_assistant }
    end

    assert_redirected_to system_activity_path(assigns(:system_activity))
  end

  test "should show system_activity" do
    get :show, id: @system_activity
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @system_activity
    assert_response :success
  end

  test "should update system_activity" do
    put :update, id: @system_activity, system_activity: { command: @system_activity.command, device_token: @system_activity.device_token, raw_content: @system_activity.raw_content, value: @system_activity.value, virtual_assistant: @system_activity.virtual_assistant }
    assert_redirected_to system_activity_path(assigns(:system_activity))
  end

  test "should destroy system_activity" do
    assert_difference('SystemActivity.count', -1) do
      delete :destroy, id: @system_activity
    end

    assert_redirected_to system_activities_path
  end
end
