require 'test_helper'

class AnetDevicesControllerTest < ActionController::TestCase
  setup do
    @anet_device = anet_devices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:anet_devices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create anet_device" do
    assert_difference('AnetDevice.count') do
      post :create, anet_device: { description: @anet_device.description, id: @anet_device.id, id_owner: @anet_device.id_owner, is_public: @anet_device.is_public, location: @anet_device.location, object: @anet_device.object, secret_token: @anet_device.secret_token, token: @anet_device.token }
    end

    assert_redirected_to anet_device_path(assigns(:anet_device))
  end

  test "should show anet_device" do
    get :show, id: @anet_device
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @anet_device
    assert_response :success
  end

  test "should update anet_device" do
    put :update, id: @anet_device, anet_device: { description: @anet_device.description, id: @anet_device.id, id_owner: @anet_device.id_owner, is_public: @anet_device.is_public, location: @anet_device.location, object: @anet_device.object, secret_token: @anet_device.secret_token, token: @anet_device.token }
    assert_redirected_to anet_device_path(assigns(:anet_device))
  end

  test "should destroy anet_device" do
    assert_difference('AnetDevice.count', -1) do
      delete :destroy, id: @anet_device
    end

    assert_redirected_to anet_devices_path
  end
end
