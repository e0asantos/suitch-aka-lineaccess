require 'test_helper'

class AnetCommandsControllerTest < ActionController::TestCase
  setup do
    @anet_command = anet_commands(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:anet_commands)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create anet_command" do
    assert_difference('AnetCommand.count') do
      post :create, anet_command: { arduino_id: @anet_command.arduino_id, command: @anet_command.command, id: @anet_command.id, invoke_every: @anet_command.invoke_every, is_ondemand: @anet_command.is_ondemand, is_only_question: @anet_command.is_only_question }
    end

    assert_redirected_to anet_command_path(assigns(:anet_command))
  end

  test "should show anet_command" do
    get :show, id: @anet_command
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @anet_command
    assert_response :success
  end

  test "should update anet_command" do
    put :update, id: @anet_command, anet_command: { arduino_id: @anet_command.arduino_id, command: @anet_command.command, id: @anet_command.id, invoke_every: @anet_command.invoke_every, is_ondemand: @anet_command.is_ondemand, is_only_question: @anet_command.is_only_question }
    assert_redirected_to anet_command_path(assigns(:anet_command))
  end

  test "should destroy anet_command" do
    assert_difference('AnetCommand.count', -1) do
      delete :destroy, id: @anet_command
    end

    assert_redirected_to anet_commands_path
  end
end
