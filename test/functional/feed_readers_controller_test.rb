require 'test_helper'

class FeedReadersControllerTest < ActionController::TestCase
  setup do
    @feed_reader = feed_readers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:feed_readers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create feed_reader" do
    assert_difference('FeedReader.count') do
      post :create, feed_reader: { descripcion: @feed_reader.descripcion, id: @feed_reader.id, last_notification: @feed_reader.last_notification, titulo: @feed_reader.titulo, url_source: @feed_reader.url_source }
    end

    assert_redirected_to feed_reader_path(assigns(:feed_reader))
  end

  test "should show feed_reader" do
    get :show, id: @feed_reader
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @feed_reader
    assert_response :success
  end

  test "should update feed_reader" do
    put :update, id: @feed_reader, feed_reader: { descripcion: @feed_reader.descripcion, id: @feed_reader.id, last_notification: @feed_reader.last_notification, titulo: @feed_reader.titulo, url_source: @feed_reader.url_source }
    assert_redirected_to feed_reader_path(assigns(:feed_reader))
  end

  test "should destroy feed_reader" do
    assert_difference('FeedReader.count', -1) do
      delete :destroy, id: @feed_reader
    end

    assert_redirected_to feed_readers_path
  end
end
