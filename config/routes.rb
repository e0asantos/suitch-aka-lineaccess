LineAccess::Application.routes.draw do
  get "layout_builder/create"

  get "layout_builder/destroy"

  get "layout_builder/update"

  # match "*path" => redirect("https://www.suitch.network/%{path}"), :constraints => { :protocol => "http://" }
  # match "*path" => redirect("https://www.suitch.network/%{path}"), :constraints => { :subdomain => "" }
  
  get "metric/show"
  get "metric/logInJSON"
  post "metric/logInJSON"
  get "metric/execJSON"
  post "metric/execJSON"

  match '/glasses/jarvixInterpreter', :controller => 'glasses', :action => 'jarvixInterpreter'
  resources :glasses


  resources :smartwatches


  match '/summaries/setLocation', :controller => 'summaries', :action => 'setLocation'
  match '/summaries/geoSettings', :controller => 'summaries', :action => 'geoSettings'
  match '/summaries/simulateMe', :controller => 'summaries', :action => 'simulateMe'
  match '/summaries/newOrigin', :controller => 'summaries', :action => 'newOrigin'
  match '/summaries/sendToAll', :controller => 'summaries', :action => 'sendToAll'
  match '/bot/callback', :controller => 'anet_devices', :action => 'fbbot'
  get '/summaries/call/:id', :controller => 'summaries', :action => 'showCallContents'
  match '/logout', :controller => 'summaries', :action => 'logout'
  resources :summaries

  
  resources :accounts


  resources :earthquakes


  resources :subscriptions

  resources :community


  resources :anet_logs

  resources :feed_readers


  resources :anet_values


  resources :anet_commands

  resources :dashboards

  get '/devices/claim', :controller=>'anet_devices', :action=> 'showClaim'
  get '/devices/add_board', :controller=>'anet_devices', :action=> 'showAdd'
  get '/devices/add_phone', :controller=>'anet_devices', :action=> 'showPhone'
  get '/devices/add_swatch', :controller=>'anet_devices', :action=> 'showSWatch'
  get '/devices/browser/:device', :controller=>'anet_devices', :action=> 'showBrowser'
  post '/devices/open/:device', :controller=>'anet_devices', :action=> 'showOpenFile'
  post '/devices/save/:device', :controller=>'anet_devices', :action=> 'showSaveFile'
  post '/devices/sync', :controller=>'anet_devices', :action=> 'alexa_sync'
  post '/devices/scan', :controller=>'anet_devices', :action=> 'alexa_scanning'
  post '/devices/dhcp', :controller=>'anet_devices', :action=> 'dhcp_process'
  match '/devices/send', :controller => 'anet_devices', :action => 'communication'
  match '/devices/camera/download', :controller => 'anet_devices', :action => 'camera_download'
  post '/devices/schedule', :controller=>'anet_devices', :action=> 'manage_schedule'
  match '/devices/pubsub', :controller=>'anet_devices', :action=> 'manage_pubsub'
  resources :anet_devices do
      post :sendToDevice, on: :collection 
  end
  
  resources :sys_user_phones

  # get '/personal_accounts/thirdparties', :controller=>'anet_devices', :action=> 'showClaim'
  # get '/personal_accounts/thirdparties', :controller=>'anet_devices', :action=> 'showClaim'
  resources :personal_accounts do
    post :changeLanguage, on: :collection
    post :likeIt, on: :collection
  end


  resources :authentications
  # math '/auth/failure' => 'authentications#failed'
  match '/auth/signup', :controller => 'authentications', :action => 'signup'
  get '/auth/signup/backwards', :controller => 'authentications', :action => 'backwards'
  match '/auth/webLogin', :controller => 'authentications', :action => 'webLogin'
  
  match '/auth/tokenize', :controller => 'authentications', :action => 'google_tokenize'
  match '/auth/v2/alexa/tokenize', :controller => 'authentications', :action => 'alexa_tokenize'
  post '/auth/cookie', :controller => 'authentications', :action => 'internal_token'
  match '/auth/assistant', :controller => 'authentications', :action => 'google_auth'
  match '/auth/alexapoc', :controller => 'authentications', :action => 'alexa_poc'
  match '/auth/alexa', :controller => 'authentications', :action => 'alexa_auth'
  get '/auth/messengerbot', :controller => 'authentications', :action => 'messenger_auth'
  post '/auth/messengerbot', :controller => 'authentications', :action => 'messenger_messages'
  match '/auth/:provider/callback' => 'authentications#create'

  get "/policy", to: "welcome#policy"
  get "/policy-es", to: "welcome#policy-es"

  # V2 API, all the next APIs are used for the new version
  match '/auth/v2/login', :controller => 'authentications', :action => 'loginv2'
  match '/auth/v2/verify', :controller => 'authentications', :action => 'internal_token'
  post '/auth/v2/user/avatar', :controller => 'authentications', :action => 'post_avatar_v2'
  post '/auth/v2/user/forgot', :controller => 'authentications', :action => 'post_forgot_v2'
  get '/auth/v2/user', :controller => 'authentications', :action => 'get_user_v2'
  put '/auth/v2/user', :controller => 'authentications', :action => 'put_user_v2'
  put '/auth/v2/user/pwd', :controller => 'authentications', :action => 'put_user_pwd_v2'
  match '/devices/v2/show', :controller=>'anet_devices', :action=> 'get_devices_v2'
  match '/devices/v2/recent_activity', :controller=>'anet_devices', :action=> 'get_recent_activity_v2'
  match '/location/v2/update', :controller=>'summaries', :action=> 'set_location_v2'
  match '/devices/v2/command/alexa', :controller=>'anet_devices', :action=> 'receive_alexa_payload_v2'
  match '/devices/v2/command/nest', :controller=>'anet_devices', :action=> 'receive_nest_payload_v2'
  match '/devices/v2/command/google', :controller=>'anet_devices', :action=> 'receive_google_payload_v2'
  match '/devices/v2/command/send', :controller=>'anet_devices', :action=> 'receive_api_payload_v2'
  match '/devices/v2/command/sendv2', :controller=>'anet_devices', :action=> 'sendToDevicev2'
  match '/devices/v2/public', :controller=>'anet_devices', :action=> 'get_public_devices_v2'
  match '/webhook/v2/from_email', :controller=>'anet_devices', :action=> 'receive_email_payload_v2'
  match '/assistant/v2/activity', :controller=>'system_activities', :action=> 'get_assistant_activity_v2'
  post '/assistant/v2/link', :controller=>'authentications', :action=> 'post_assistant_link_v2'
  get '/assistant/v2/link', :controller=>'authentications', :action=> 'get_assistant_link_v2'
  match '/energy/v2/consumption/1day', :controller=>'system_activities', :action=> 'get_assistant_activity_v2'
  match '/energy/v2/consumption/7day', :controller=>'system_activities', :action=> 'get_assistant_activity_v2'
  match '/energy/v2/consumption/1month', :controller=>'system_activities', :action=> 'get_assistant_activity_v2'
  match '/energy/v2/consumption/12months', :controller=>'system_activities', :action=> 'get_assistant_activity_v2'
  match '/test/v2/test_1', :controller=>'anet_devices', :action=> 'random_test_v2'
  match '/test/v2/test_2', :controller=>'authentications', :action=> 'random_test2_v2'
  match '/devices/v2/:token/logs', :controller=>'anet_devices', :action=> 'get_device_logs_v2'
  post  '/devices/v2/:token/schedule', :controller=>'anet_devices', :action=> 'manage_schedule'
  post  '/devices/v2/:token/command', :controller=>'anet_commands', :action=> 'create'
  get  '/devices/v2/:token/status', :controller=>'anet_devices', :action=> 'get_status_v2'
  get  '/devices/v2/:token/state', :controller=>'anet_devices', :action=> 'get_state_v2'
  get  '/devices/v2/:token/screensaver', :controller=>'anet_devices', :action=> 'get_screensaver_v2'
  post  '/devices/v2/:token/screensaver', :controller=>'anet_devices', :action=> 'post_screensaver_v2'
  get  '/devices/v2/:token/props', :controller=>'anet_devices', :action=> 'get_props_v2'
  get  '/devices/v2/:token/prop/:id', :controller=>'anet_devices', :action=> 'get_prop_v2'
  get  '/devices/v2/:token/prop/:id/chart', :controller=>'anet_devices', :action=> 'get_prop_chart_v2'
  get  '/devices/v2/:token/prop/:id/chart/24hour', :controller=>'anet_devices', :action=> 'get_prop_chart_24h_v2'
  get  '/devices/v2/:token/prop/:id/chart/7day', :controller=>'anet_devices', :action=> 'get_prop_chart_7day_v2'
  get  '/devices/v2/:token/prop/:id/chart/monthly', :controller=>'anet_devices', :action=> 'get_prop_chart_month_v2'
  get  '/devices/v2/:token/command/show', :controller=>'anet_commands', :action=> 'show'
  get  '/devices/v2/:token/schedule/show', :controller=>'anet_devices', :action=> 'get_schedules_v2'
  delete  '/devices/v2/:token/schedule/:id', :controller=>'anet_devices', :action=> 'delete_schedule_v2'
  delete  '/devices/v2/:token/command/:id', :controller=>'anet_commands', :action=> 'destroy'
  post  '/layouts/v2', :controller=>'layout_builder', :action=> 'create'
  get  '/layouts/v2', :controller=>'layout_builder', :action=> 'show'
  delete  '/layouts/v2/:id', :controller=>'layout_builder', :action=> 'destroy'
  put  '/layouts/v2/:id', :controller=>'layout_builder', :action=> 'update'
  post  '/devices/v2/:token/fav', :controller=>'anet_devices', :action=> 'post_fav_v2'
  get  '/devices/v2/favs', :controller=>'anet_devices', :action=> 'get_fav_v2'
  post '/devices/v2/state/alexa', :controller=>'anet_devices', :action=> 'alexa_state_report_v2'
  get '/devices/v2/:token/state/suitch', :controller=>'anet_devices', :action=> 'suitch_state_report_v2'
  get '/edgar/test', :controller=>'anet_devices', :action=> 'test_api'
  post  '/chain/v2', :controller=>'chain_reaction', :action=> 'create'
  get  '/chain/v2', :controller=>'chain_reaction', :action=> 'show'
  delete  '/chain/v2/:id', :controller=>'chain_reaction', :action=> 'destroy'
  put  '/chain/v2/:id', :controller=>'chain_reaction', :action=> 'update'

  get '/devices/ssid/availables', :controller=>'anet_devices', :action=> 'getAvailableDevicePerSsid'
  post '/devices/ssid/owner', :controller=>'anet_devices', :action=> 'setIdOwnerToDevices'

  get '/devices/notifications', :controller=>'anet_devices', :action=> 'getDeviceNotifications'
  post '/devices/notifications/dismiss', :controller=>'anet_devices', :action=> 'post_notifications_dismiss'

  post '/devices/v2/findmy/create', :controller=>'anet_devices', :action=> 'findmy_create'
  post '/devices/v2/findmy/sensor', :controller=>'anet_devices', :action=> 'findmy_create_sensor'
  get '/devices/v2/findmy/all', :controller=>'anet_devices', :action=> 'get_findmy_all'
  get '/devices/v2/findmy/sensors', :controller=>'anet_devices', :action=> 'get_findmy_sensors'
  post '/devices/v2/findmy/located', :controller=>'anet_devices', :action=> 'findmy_located'
  get '/devices/v2/findmy/:token/soil', :controller=>'anet_devices', :action=> 'get_findmy_soil'
  get '/devices/v2/findmy/:token/location', :controller=>'anet_devices', :action=> 'get_findmy_location'
  post '/devices/v2/findmy/sensors/upload', :controller=>'anet_devices', :action=> 'post_findmy_sensors'

  get '/devices/v2/:token/battery', :controller=>'anet_devices', :action=> 'get_battery_details'
  post '/devices/v2/:token/battery/reset', :controller=>'anet_devices', :action=> 'post_battery_update'
  get '/batteries/v2/show', :controller=>'anet_devices', :action=> 'get_batteries'
  #get "welcome/index" 

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # mount Facebook::Messenger::Server, at: 'bot'
  root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
