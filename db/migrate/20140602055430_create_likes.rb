class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.integer :id
      t.integer :command_id
      t.integer :device_id
      t.integer :user_liked

      t.timestamps
    end
  end
end
