class CreateAnetLessSensors < ActiveRecord::Migration
  def change
    create_table :anet_less_sensors do |t|
      t.integer :id_sensor
      t.string :value
      t.string :priv_key
      t.string :pub_key
      t.string :adv_key

      t.timestamps
    end
  end
end
