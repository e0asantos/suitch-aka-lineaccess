class AddIsActiveToFeedReaders < ActiveRecord::Migration
  def change
    add_column :feed_readers, :is_active, :integer
  end
end
