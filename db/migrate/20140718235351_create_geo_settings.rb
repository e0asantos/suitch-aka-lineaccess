class CreateGeoSettings < ActiveRecord::Migration
  def change
    create_table :geo_settings do |t|
      t.integer :id
      t.integer :user_id
      t.integer :earthquakes
      t.integer :volcano
      t.integer :water

      t.timestamps
    end
  end
end
