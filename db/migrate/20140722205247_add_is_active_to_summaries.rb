class AddIsActiveToSummaries < ActiveRecord::Migration
  def change
    add_column :summaries, :is_active, :integer
  end
end
