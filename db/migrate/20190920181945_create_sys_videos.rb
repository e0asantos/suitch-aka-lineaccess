class CreateSysVideos < ActiveRecord::Migration
  def change
    create_table :sys_videos do |t|
      t.datetime :created_at
      t.string :filename
      t.string :token

      t.timestamps
    end
  end
end
