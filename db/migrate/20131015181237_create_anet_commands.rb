class CreateAnetCommands < ActiveRecord::Migration
  def change
    create_table :anet_commands do |t|
      t.integer :id
      t.integer :arduino_id
      t.string :command
      t.integer :is_only_question
      t.integer :is_ondemand
      t.integer :invoke_every

      t.timestamps
    end
  end
end
