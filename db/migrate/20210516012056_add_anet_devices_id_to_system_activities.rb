class AddAnetDevicesIdToSystemActivities < ActiveRecord::Migration
  def change
    add_column :system_activities, :anet_device_id, :integer
  end
end
