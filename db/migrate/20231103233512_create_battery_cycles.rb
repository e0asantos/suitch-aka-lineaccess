class CreateBatteryCycles < ActiveRecord::Migration
  def change
    create_table :battery_cycles do |t|
      t.integer :arduino_id
      t.integer :battery_id

      t.timestamps
    end
  end
end
