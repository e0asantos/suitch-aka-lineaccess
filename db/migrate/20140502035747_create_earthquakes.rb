class CreateEarthquakes < ActiveRecord::Migration
  def change
    create_table :earthquakes do |t|
      t.integer :id
      t.string :latitude
      t.string :longitude
      t.string :location
      t.string :reportedTime
      t.string :magnitude
      t.text :rawData

      t.timestamps
    end
  end
end
