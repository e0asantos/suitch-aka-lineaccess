class CreateSmartwatches < ActiveRecord::Migration
  def change
    create_table :smartwatches do |t|
      t.integer :id
      t.integer :watch_type
      t.string :token
      t.integer :sys_user_id

      t.timestamps
    end
  end
end
