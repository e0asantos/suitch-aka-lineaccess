class AddGoogleOauthToSysUsers < ActiveRecord::Migration
  def change
    add_column :sys_users, :google_oauth, :string
  end
end
