class AddCreatedAtToSysUserPhones < ActiveRecord::Migration
  def change
    add_column :sys_user_phones, :created_at, :datetime
  end
end
