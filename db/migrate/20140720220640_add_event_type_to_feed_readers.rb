class AddEventTypeToFeedReaders < ActiveRecord::Migration
  def change
    add_column :feed_readers, :event_type, :integer
  end
end
