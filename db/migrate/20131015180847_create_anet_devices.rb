class CreateAnetDevices < ActiveRecord::Migration
  def change
    create_table :anet_devices do |t|
      t.integer :id
      t.text :description
      t.text :location
      t.text :object
      t.text :token
      t.text :secret_token
      t.integer :id_owner
      t.integer :is_public

      t.timestamps
    end
  end
end
