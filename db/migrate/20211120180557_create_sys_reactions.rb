class CreateSysReactions < ActiveRecord::Migration
  def change
    create_table :sys_reactions do |t|
      t.integer :device_id
      t.text :reactor
      t.integer :user_id

      t.timestamps
    end
  end
end
