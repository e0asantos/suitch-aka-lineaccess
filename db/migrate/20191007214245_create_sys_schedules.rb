class CreateSysSchedules < ActiveRecord::Migration
  def change
    create_table :sys_schedules do |t|
      t.integer :device_id
      t.datetime :schedule

      t.timestamps
    end
  end
end
