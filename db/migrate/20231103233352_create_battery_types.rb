class CreateBatteryTypes < ActiveRecord::Migration
  def change
    create_table :battery_types do |t|
      t.string :name
      t.string :capacity
      t.string :voltage
      t.string :device_type
      t.integer :days

      t.timestamps
    end
  end
end
