class AddDeviceIdsToSysReactions < ActiveRecord::Migration
  def change
    add_column :sys_reactions, :device_ids, :string
  end
end
