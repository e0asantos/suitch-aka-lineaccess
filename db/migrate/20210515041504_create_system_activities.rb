class CreateSystemActivities < ActiveRecord::Migration
  def change
    create_table :system_activities do |t|
      t.string :device_token
      t.integer :virtual_assistant
      t.string :command
      t.string :value
      t.text :raw_content

      t.timestamps
    end
  end
end
