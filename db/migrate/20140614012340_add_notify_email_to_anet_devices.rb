class AddNotifyEmailToAnetDevices < ActiveRecord::Migration
  def change
    add_column :anet_devices, :notify_email, :integer
  end
end
