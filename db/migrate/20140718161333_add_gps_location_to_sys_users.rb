class AddGpsLocationToSysUsers < ActiveRecord::Migration
  def change
    add_column :sys_users, :gps_location, :string
  end
end
