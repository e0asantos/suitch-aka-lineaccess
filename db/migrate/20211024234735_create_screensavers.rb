class CreateScreensavers < ActiveRecord::Migration
  def change
    create_table :screensavers do |t|
      t.string :version
      t.integer :amount
      t.string :name

      t.timestamps
    end
  end
end
