class CreateLayoutBuilders < ActiveRecord::Migration
  def change
    create_table :layout_builders do |t|
      t.string :name
      t.text :content
      t.string :description
      t.integer :owner_id
      t.datetime :created_at

      t.timestamps
    end
  end
end
