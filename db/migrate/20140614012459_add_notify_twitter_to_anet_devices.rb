class AddNotifyTwitterToAnetDevices < ActiveRecord::Migration
  def change
    add_column :anet_devices, :notify_twitter, :integer
  end
end
