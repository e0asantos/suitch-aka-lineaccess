class CreateFeedReaders < ActiveRecord::Migration
  def change
    create_table :feed_readers do |t|
      t.integer :id
      t.string :url_source
      t.string :last_notification
      t.string :titulo
      t.string :descripcion

      t.timestamps
    end
  end
end
