class CreateAnetValues < ActiveRecord::Migration
  def change
    create_table :anet_values do |t|
      t.integer :id
      t.integer :arduino_id
      t.integer :command_id
      t.string :value

      t.timestamps
    end
  end
end
