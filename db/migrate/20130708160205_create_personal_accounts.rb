class CreatePersonalAccounts < ActiveRecord::Migration
  def change
    create_table :personal_accounts do |t|
      t.string :note
      t.string :sound_file
      t.string :user_id

      t.timestamps
    end
  end
end
