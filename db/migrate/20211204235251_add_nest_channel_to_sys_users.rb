class AddNestChannelToSysUsers < ActiveRecord::Migration
  def change
    add_column :sys_users, :nest_channel, :string
  end
end
