class CreateAnetLogs < ActiveRecord::Migration
  def change
    create_table :anet_logs do |t|
      t.integer :id
      t.integer :arduino_id
      t.integer :connection_type

      t.timestamps
    end
  end
end
