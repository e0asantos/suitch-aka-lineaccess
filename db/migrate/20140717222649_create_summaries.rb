class CreateSummaries < ActiveRecord::Migration
  def change
    create_table :summaries do |t|
      t.integer :id
      t.integer :user_id

      t.timestamps
    end
  end
end
