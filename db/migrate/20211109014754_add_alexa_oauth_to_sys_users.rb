class AddAlexaOauthToSysUsers < ActiveRecord::Migration
  def change
    add_column :sys_users, :alexa_oauth, :string
  end
end
