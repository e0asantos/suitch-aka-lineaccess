class AddDeviceIdToScreensavers < ActiveRecord::Migration
  def change
    add_column :screensavers, :device_id, :integer
  end
end
