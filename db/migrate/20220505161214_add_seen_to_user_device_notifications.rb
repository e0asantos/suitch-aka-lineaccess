class AddSeenToUserDeviceNotifications < ActiveRecord::Migration
  def change
    add_column :user_device_notifications, :seen, :integer
  end
end
