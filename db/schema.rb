# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20231103233512) do

  create_table "Country", :primary_key => "CountryId", :force => true do |t|
    t.string "CountryCode", :limit => 5
    t.string "CountryName", :limit => 1500
  end

  create_table "anet_commands", :force => true do |t|
    t.integer  "arduino_id"
    t.string   "command"
    t.integer  "is_only_question"
    t.string   "command_data"
    t.integer  "is_ondemand"
    t.integer  "invoke_every"
    t.time     "on_time"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "unit"
  end

  add_index "anet_commands", ["arduino_id"], :name => "arduino_id"

  create_table "anet_devices", :force => true do |t|
    t.text     "description"
    t.text     "location"
    t.text     "object"
    t.string   "token",           :limit => 20, :default => ""
    t.string   "secret_token",    :limit => 20, :default => ""
    t.integer  "id_owner"
    t.integer  "is_public"
    t.integer  "likes",                         :default => 0
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.integer  "notify_hangout"
    t.integer  "notify_fb"
    t.integer  "notify_email"
    t.integer  "notify_twitter"
    t.integer  "notify_fbinbox"
    t.string   "mip"
    t.integer  "cliente"
    t.string   "ssid",            :limit => 50
    t.string   "version",         :limit => 20
    t.integer  "notify_telegram"
    t.integer  "fav"
  end

  add_index "anet_devices", ["id_owner"], :name => "index_anet_devices_on_id_owner"
  add_index "anet_devices", ["token"], :name => "idx_token"

  create_table "anet_less_sensors", :force => true do |t|
    t.integer  "id_sensor"
    t.string   "value"
    t.string   "priv_key"
    t.string   "pub_key"
    t.string   "adv_key"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "anet_logs", :force => true do |t|
    t.integer  "arduino_id",      :null => false
    t.integer  "connection_type"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "mip"
  end

  add_index "anet_logs", ["arduino_id"], :name => "anet_devices_id"

  create_table "anet_values", :force => true do |t|
    t.integer  "arduino_id"
    t.integer  "command_id"
    t.string   "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "anet_values", ["arduino_id", "command_id", "created_at"], :name => "idx_values_arduino_command_date"
  add_index "anet_values", ["arduino_id"], :name => "arduino_id"
  add_index "anet_values", ["command_id"], :name => "command_id"
  add_index "anet_values", ["created_at"], :name => "index_anet_values_on_created_at"

  create_table "api_interfaces", :force => true do |t|
    t.string  "name",           :limit => 45
    t.string  "command",        :limit => 45
    t.string  "gema"
    t.string  "url"
    t.boolean "removed",                      :default => false, :null => false
    t.text    "token"
    t.text    "token_secret"
    t.integer "visible"
    t.text    "icon"
    t.text    "pattern"
    t.integer "usersNeedToken"
  end

  create_table "api_methods", :force => true do |t|
    t.integer "api_interface",                                  :null => false
    t.string  "name",          :limit => 45
    t.string  "params",        :limit => 45
    t.string  "oral",          :limit => 30
    t.string  "oralSpanish",   :limit => 30
    t.integer "ischat",                      :default => 0
    t.boolean "removed",                     :default => false, :null => false
  end

  add_index "api_methods", ["api_interface"], :name => "fk_api_method_1_idx"

  create_table "api_metric_method", :force => true do |t|
    t.integer "sys_user",                      :null => false
    t.integer "api_method",                    :null => false
    t.boolean "removed",    :default => false, :null => false
  end

  add_index "api_metric_method", ["api_method"], :name => "fk_api_metric_method_2_idx"
  add_index "api_metric_method", ["sys_user"], :name => "fk_api_metric_method_1_idx"

  create_table "api_metric_words", :force => true do |t|
    t.integer "sys_user",                    :null => false
    t.integer "api_word",                    :null => false
    t.boolean "removed",  :default => false, :null => false
  end

  add_index "api_metric_words", ["sys_user"], :name => "fk_sys_user"

  create_table "api_token_user_interfaces", :force => true do |t|
    t.integer "sys_user",                                        :null => false
    t.integer "api_interface",                                   :null => false
    t.string  "token"
    t.boolean "removed",                      :default => false, :null => false
    t.text    "token_secret"
    t.integer "expires"
    t.string  "uid",           :limit => 300
  end

  add_index "api_token_user_interfaces", ["api_interface"], :name => "fk_api_token_user_interface_2_idx"
  add_index "api_token_user_interfaces", ["sys_user"], :name => "fk_api_token_user_interface_1_idx"

  create_table "api_words", :force => true do |t|
    t.string  "word",    :limit => 25, :default => "",    :null => false
    t.boolean "removed",               :default => false, :null => false
  end

  create_table "authentications", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "battery_cycles", :force => true do |t|
    t.integer  "arduino_id"
    t.integer  "battery_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "battery_types", :force => true do |t|
    t.string   "name"
    t.string   "capacity"
    t.string   "voltage"
    t.string   "device_type"
    t.integer  "days"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "dashboards", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "earthquakes", :force => true do |t|
    t.string   "latitude"
    t.string   "longitude"
    t.string   "location"
    t.string   "reportedTime"
    t.string   "magnitude"
    t.text     "rawData"
    t.string   "gateway",      :limit => 11
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "feed_readers", :force => true do |t|
    t.string   "url_source"
    t.string   "last_notification"
    t.string   "titulo"
    t.string   "descripcion"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "is_active"
    t.integer  "event_typer"
  end

  create_table "geo_settings", :force => true do |t|
    t.integer  "sys_user_id"
    t.integer  "earthquakes"
    t.integer  "volcano"
    t.integer  "water"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "add_distancia"
  end

  add_index "geo_settings", ["sys_user_id"], :name => "sys_user_id"

  create_table "glasses", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "jarvix_abstract_objects", :force => true do |t|
    t.string   "object_name",       :limit => 200
    t.string   "object_properties", :limit => 400
    t.string   "superclasses",      :limit => 400
    t.datetime "updated_at"
    t.datetime "created_at"
  end

  create_table "jarvix_conversation_logs", :force => true do |t|
    t.string   "chat_id",       :limit => 100
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "original_line", :limit => 500
    t.string   "broken_lines",  :limit => 500
    t.string   "text_to_api",   :limit => 500
    t.string   "response",      :limit => 200
    t.integer  "user_id"
  end

  add_index "jarvix_conversation_logs", ["user_id"], :name => "user_id_idx"

  create_table "jarvix_custom_objects", :force => true do |t|
    t.integer  "id_abstract_object"
    t.integer  "id_owner"
    t.string   "custom_object_properties", :limit => 400
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "is_active",                               :default => 1
  end

  add_index "jarvix_custom_objects", ["id_abstract_object"], :name => "id_abstract_object"
  add_index "jarvix_custom_objects", ["id_owner"], :name => "id_owner"

  create_table "jarvix_custom_properties", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "property_name",    :limit => 100
    t.string   "property_value",   :limit => 100
    t.integer  "is_enabled"
    t.integer  "id_user"
    t.integer  "id_custom_object"
  end

  add_index "jarvix_custom_properties", ["id_custom_object"], :name => "id_custom_object"
  add_index "jarvix_custom_properties", ["id_user"], :name => "id_user"

  create_table "jarvix_intentions", :force => true do |t|
    t.integer "user_id"
    t.string  "intention", :limit => 400
  end

  create_table "jarvix_methods_inwords", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "method_name",   :limit => 50
    t.integer  "word_type"
    t.string   "position_word", :limit => 5
  end

  create_table "jarvix_personalities", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "my_name",              :limit => 100
    t.string   "my_status",            :limit => 200
    t.string   "my_description",       :limit => 400
    t.string   "external_description", :limit => 500
    t.string   "my_property",          :limit => 300
    t.string   "my_dates",             :limit => 500
    t.string   "my_noun",              :limit => 10,  :default => "I"
    t.string   "sex",                  :limit => 11,  :default => "m"
    t.datetime "last_login",                          :default => '2013-08-01 15:35:59'
    t.integer  "suitch_user_id"
  end

  create_table "jarvix_salutes", :force => true do |t|
    t.string   "salute_name",       :limit => 100
    t.string   "conversation_step", :limit => 100
    t.integer  "confidence"
    t.string   "time_barrier",      :limit => 100
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  create_table "jarvix_words", :force => true do |t|
    t.string   "word",              :limit => 100
    t.string   "es_word",           :limit => 100
    t.string   "fr_word",           :limit => 100
    t.string   "de_word",           :limit => 100
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "word_type"
    t.string   "is_question",       :limit => 50
    t.string   "is_sentence",       :limit => 50
    t.string   "result_is",         :limit => 100
    t.string   "is_breakline",      :limit => 50
    t.string   "time_usage",        :limit => 100
    t.string   "respect_level",     :limit => 5
    t.string   "conversation_step", :limit => 100
    t.string   "is_method",         :limit => 50
    t.string   "sex",               :limit => 2,   :default => "m"
  end

  create_table "layout_builders", :force => true do |t|
    t.string   "name"
    t.text     "content"
    t.string   "description"
    t.integer  "owner_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "likes", :force => true do |t|
    t.integer  "command_id"
    t.integer  "device_id"
    t.integer  "user_liked"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "nfc_readers", :force => true do |t|
    t.integer  "nfc_type"
    t.string   "last_token",   :default => "c2FsbW85MQ=="
    t.string   "static_token"
    t.integer  "blocked",      :default => 0
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "user_id"
  end

  create_table "nfc_requests", :force => true do |t|
    t.integer  "qiwo_product_id"
    t.integer  "qiwo_enterprise_id"
    t.integer  "sys_user_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "personal_accounts", :force => true do |t|
    t.string   "note"
    t.string   "sound_file"
    t.string   "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "screensavers", :force => true do |t|
    t.string   "version"
    t.integer  "amount"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "device_id"
  end

  create_table "searches", :force => true do |t|
    t.string   "seed"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "smartwatches", :force => true do |t|
    t.integer  "watch_type"
    t.string   "token"
    t.integer  "sys_user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "subscriptions", :force => true do |t|
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "summaries", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "texto"
    t.integer  "is_active"
  end

  create_table "sys_access_type", :force => true do |t|
    t.string  "type",    :limit => 15
    t.boolean "removed",               :default => false
  end

  create_table "sys_dhcps", :force => true do |t|
    t.string   "ip",           :limit => 20
    t.string   "token",        :limit => 20
    t.datetime "created_at"
    t.integer  "blocked"
    t.datetime "blocked_time"
  end

  create_table "sys_event", :force => true do |t|
    t.string  "name",    :limit => 45
    t.boolean "removed",               :default => false, :null => false
  end

  create_table "sys_ignored_words", :force => true do |t|
    t.string  "word",    :limit => 45
    t.boolean "removed",               :default => false, :null => false
  end

  create_table "sys_logs", :force => true do |t|
    t.string   "sys_user",     :limit => 11,   :default => "",    :null => false
    t.date     "date_event"
    t.time     "time_event"
    t.string   "before_event", :limit => 6144
    t.string   "after_event",  :limit => 6144
    t.boolean  "removed",                      :default => false, :null => false
    t.datetime "created_at"
    t.integer  "sys_event"
  end

  create_table "sys_messages", :force => true do |t|
    t.text    "message"
    t.string  "lang",       :limit => 30
    t.integer "id_message"
  end

  create_table "sys_metric_accesses", :force => true do |t|
    t.string   "phoneNumber",     :limit => 30
    t.string   "voipAlias",       :limit => 30
    t.integer  "sys_access_type",                                   :null => false
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "closeReason",     :limit => 200
    t.boolean  "removed",                        :default => false
  end

  add_index "sys_metric_accesses", ["sys_access_type"], :name => "fk_sys_metric_access_2_idx"

  create_table "sys_notes", :force => true do |t|
    t.integer "sys_user",                                        :null => false
    t.string  "phrase",        :limit => 512, :default => "",    :null => false
    t.boolean "removed",                      :default => false, :null => false
    t.integer "api_interface",                :default => 1,     :null => false
  end

  add_index "sys_notes", ["api_interface"], :name => "fk_sys_note2_idx"
  add_index "sys_notes", ["sys_user"], :name => "fk_sys_note1"

  create_table "sys_notes_logs", :id => false, :force => true do |t|
    t.integer "id",                                    :null => false
    t.integer "sys_notes",                             :null => false
    t.integer "sys_log",                               :null => false
    t.integer "removed",   :limit => 1, :default => 0, :null => false
  end

  add_index "sys_notes_logs", ["sys_log"], :name => "fk_sys_notes_log_2_idx"
  add_index "sys_notes_logs", ["sys_notes"], :name => "fk_sys_notes_log_1_idx"

  create_table "sys_phone_types", :force => true do |t|
    t.string  "ptype",       :limit => 35, :default => "",    :null => false
    t.boolean "removed",                   :default => false, :null => false
    t.text    "description"
  end

  create_table "sys_phrases", :force => true do |t|
    t.text     "phrase"
    t.string   "phone",              :limit => 13
    t.string   "soundfile",          :limit => 50
    t.integer  "interface_executed",                :default => 4
    t.text     "translate"
    t.string   "callid",             :limit => 100
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sys_reactions", :force => true do |t|
    t.integer  "device_id"
    t.text     "reactor"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "device_ids"
  end

  add_index "sys_reactions", ["device_id"], :name => "device_id"

  create_table "sys_region_langs", :force => true do |t|
    t.string "country_code", :limit => 10
    t.string "lang",         :limit => 10
  end

  create_table "sys_regions", :force => true do |t|
    t.string "country_code", :limit => 5
    t.string "area_code",    :limit => 5
    t.string "region_name",  :limit => 25
    t.string "access_point", :limit => 25
  end

  create_table "sys_rol", :force => true do |t|
    t.string  "name",    :limit => 50, :default => "",    :null => false
    t.boolean "removed",               :default => false, :null => false
  end

  create_table "sys_schedules", :force => true do |t|
    t.integer  "device_id"
    t.datetime "schedule"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "command"
  end

  create_table "sys_start_commands", :force => true do |t|
    t.text "startCommand"
  end

  create_table "sys_user_audio", :force => true do |t|
    t.integer "sys_user",                                   :null => false
    t.string  "url",      :limit => 128
    t.boolean "removed",                 :default => false, :null => false
  end

  add_index "sys_user_audio", ["sys_user"], :name => "fk_sys_user_audio_1_idx"

  create_table "sys_user_phones", :force => true do |t|
    t.integer  "sys_user",                                        :null => false
    t.integer  "sys_phone_type",                                  :null => false
    t.string   "number",         :limit => 30
    t.boolean  "removed",                      :default => false, :null => false
    t.datetime "created_at"
  end

  add_index "sys_user_phones", ["sys_phone_type"], :name => "fk_sys_phone_type"
  add_index "sys_user_phones", ["sys_user"], :name => "fk_sys_user_phone"

  create_table "sys_users", :force => true do |t|
    t.integer  "sys_rol",                     :default => 2,     :null => false
    t.string   "name",         :limit => 20
    t.string   "last_name",    :limit => 50
    t.string   "user",         :limit => 20
    t.string   "password",     :limit => 20
    t.boolean  "removed",                     :default => false, :null => false
    t.string   "user_id",      :limit => 220
    t.string   "email",        :limit => 220
    t.text     "avatar"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "lang",         :limit => 11,  :default => "es"
    t.integer  "is_technical",                :default => 0
    t.string   "gps_location"
    t.string   "lat"
    t.string   "lon"
    t.string   "telegram_id",  :limit => 100
    t.string   "fb_id",        :limit => 100
    t.string   "google_oauth"
    t.string   "alexa_oauth"
    t.string   "company"
    t.string   "nest_channel"
  end

  add_index "sys_users", ["id"], :name => "fk_geo_settings"
  add_index "sys_users", ["sys_rol"], :name => "fk_sys_rol"

  create_table "sys_videos", :force => true do |t|
    t.datetime "created_at", :null => false
    t.string   "filename"
    t.string   "token"
    t.datetime "updated_at", :null => false
  end

  create_table "system_activities", :force => true do |t|
    t.string   "device_token"
    t.integer  "virtual_assistant"
    t.string   "command"
    t.string   "value"
    t.text     "raw_content"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "anet_device_id"
  end

  add_index "system_activities", ["anet_device_id"], :name => "index_system_activities_on_anet_device_id"
  add_index "system_activities", ["anet_device_id"], :name => "ixn_anet_device_id"
  add_index "system_activities", ["created_at"], :name => "index_system_activities_on_created_at"
  add_index "system_activities", ["device_token"], :name => "index_system_activities_on_device_token"
  add_index "system_activities", ["updated_at"], :name => "index_system_activities_on_updated_at"
  add_index "system_activities", ["virtual_assistant"], :name => "index_system_activities_on_virtual_assistant"

  create_table "tedgar", :id => false, :force => true do |t|
    t.text "txt"
  end

  create_table "telegrams", :force => true do |t|
    t.string   "phone"
    t.text     "telegram_img"
    t.string   "auth_code"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.text     "alias"
  end

  create_table "user_device_notifications", :id => false, :force => true do |t|
    t.integer  "id_owner",   :limit => 8,                   :null => false
    t.integer  "id_device",  :limit => 8,                   :null => false
    t.string   "vcType",     :limit => 200,                 :null => false
    t.string   "device",     :limit => 350,                 :null => false
    t.string   "location",   :limit => 500,                 :null => false
    t.string   "value",      :limit => 500,                 :null => false
    t.string   "decription", :limit => 300
    t.string   "token",      :limit => 20,  :default => "", :null => false
    t.boolean  "isActive"
    t.datetime "created_at"
    t.integer  "seen"
  end

  add_index "user_device_notifications", ["created_at"], :name => "index_user_device_notifications_on_created_at"
  add_index "user_device_notifications", ["id_owner"], :name => "idx_id_owner"
  add_index "user_device_notifications", ["token"], :name => "idx_token"

  create_table "users", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "password"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "email"
    t.string   "lastname"
    t.string   "token"
    t.string   "phonenumber"
    t.string   "avatar"
  end

end
