require 'open-uri'
require 'json'
require 'xmpp4r_facebook'
require 'xmpp4r/roster'
require 'fb_graph'
require "uuidtools"
require "open3"
require 'fileutils'
require 'rubygems'
# require 'bing_translator'
require 'sys_phrases.rb'
require 'sys_region.rb'
require 'sys_region_lang.rb'
require 'sys_user_phone.rb'
require 'sys_phone_type.rb'
require 'api_interface.rb'
require 'api_interface.rb'
require 'sys_user.rb'
require 'anet_device.rb'
require 'likes.rb'
class SummariesController < ApplicationController
  # GET /summaries
  # GET /summaries.json

  def logout
    session[:activeUser]=nil
    redirect_to "/"
    return
  end
  def sendToAll
    nuevo=Summary.new
    nuevo.texto=params[:msg]
    nuevo.is_active=1
    nuevo.save
  end
  def newOrigin
    nuevo=FeedReader.new
    nuevo.is_active=0
    nuevo.event_typer=3
    nuevo.save

    respond_to do |format|
      format.html { redirect_to "/summaries",notice: "Se creó un nuevo origen de datos." }
      format.json { redirect_to "/summaries",notice: "Se creó un nuevo origen de datos." }
    end
    return
    
  end
  def simulateMe
    if session[:activeUser]!=nil
     if params[:kind]=="earthquakes"
       notifyToInbox([session[:activeUser].id],"Esta es una alerta de simulación de terremoto")
     elsif params[:kind]=="volcano"
      notifyToInbox([session[:activeUser].id],"Esta es una alerta de simulación de volcan")
     elsif params[:kind]=="water"
      notifyToInbox([session[:activeUser].id],"Esta es una alerta de simulación de evento meteorologico climatico")
     end
        
    else
      redirect_to "/"
      return
    end


    respond_to do |format|
      format.html { render json: "ok" }
      format.json { render json: "ok" }
    end
  end

  def notifyToInbox(iDList,mensaje)
    token=Api_Token_User_Interface.where(:sys_user=>33,:api_interface=>2).last
    iDList.each do |singleList|
      direccionString=Sys_User.find_by_id(singleList)
      if direccionString!=nil
        cuerpoMensaje='Hola '+direccionString.name.to_s+', '+mensaje
        puts cuerpoMensaje.inspect
        id = '-100006392581606@chat.facebook.com'
        if direccionString.user_id!=nil
          
        
            to = '-'+direccionString.user_id+'@chat.facebook.com'
          #buscar producto
            body = cuerpoMensaje
            subject = 'no titulo'
            message = Jabber::Message.new to, body
            message.subject = subject
            
            client = Jabber::Client.new Jabber::JID.new(id)
            client.connect
            puts token.token
            client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)            
            client.send message
            client.close
          end
        end
    end
  end

  def showCallContents
    # buscar si el id corresponde con un dispositivo del usuario
    @call_contents = Sys_Phrase.find_by_id(params[:id])
    
    if @call_contents == nil
      return
    end
    device = AnetDevice.find_by_token(@call_contents.phone)
    if device == nil
      return
    end
    if device.id_owner != session[:activeUser].id
      return
    end
    respond_to do |format|
      format.html { render "callContents", :layout => false }
      format.json { render json: "ok" }
    end
  end

  def geoSettings
    if session[:activeUser]!=nil
      nsetts=GeoSetting.find_by_sys_user_id(session[:activeUser].id)
        nsetts.add_distancia=params[:distance]
        nsetts.earthquakes=params[:one]
        nsetts.volcano=params[:two]
        nsetts.water=params[:three]
        nsetts.save
      
    else
      redirect_to "https://www.suitch.network"
      return
    end


    respond_to do |format|
      format.html { render json: "ok" }
      format.json { render json: "ok" }
    end
  end

  def setLocation
    if session[:activeUser]!=nil
      if params[:location]=="none"
        # buscar ubicación por ip
        
      else
        usuario=Sys_User.find_by_id(session[:activeUser].id)
        usuario.gps_location=params[:location]
        usuario.lon=params[:location].split(",")[1]
        usuario.lat=params[:location].split(",")[0]
        usuario.save
      end
    else
      redirect_to "https://www.suitch.network"
      return
    end
    respond_to do |format|
      format.html { render json: "ok" }
      format.json { render json: "ok" }
    end
  end
  def index
     if session[:activeUser]==nil
      redirect_to "http://www.suitch.network"
      return
    end
    attachDevices()
    @smartwatch=Smartwatch.new
  # aqui nos aseguramos que hayan registros
  if GeoSetting.where(:sys_user_id=>session[:activeUser].id).length<1
    nsetts=GeoSetting.new
    nsetts.sys_user_id=session[:activeUser].id
    nsetts.add_distancia=20
    nsetts.earthquakes=1
    nsetts.volcano=1
    nsetts.water=1
    nsetts.save
  end
  usuario=Sys_User.find_by_id(session[:activeUser].id)
  begin
    
  
  istatus = Timeout::timeout(3) {
  regionService = JSON.parse(open("http://api.ipstack.com/"+request.remote_ip.to_s+"?access_key=b6cdef9c8216ecc77342f7f704fbe540").read)
  puts "========="
  puts request.remote_ip.to_s
  puts usuario.inspect
  puts regionService.inspect
  puts "=========="
  # hay que evitar que se guarden cosas que estan vacias siempre y cuando no contemos con una posición anterior
  if regionService["city"]=="" and usuario.gps_location==nil
    usuario.gps_location=regionService["latitude"].to_s+","+regionService["longitude"].to_s
    usuario.lon=regionService["longitude"].to_s
    usuario.lat=regionService["latitude"].to_s
  elsif regionService["city"]!="" and usuario.gps_location!=nil
    usuario.gps_location=regionService["latitude"].to_s+","+regionService["longitude"].to_s  
    usuario.lon=regionService["longitude"].to_s
    usuario.lat=regionService["latitude"].to_s
  elsif regionService["city"]!="" and usuario.gps_location==nil
    usuario.gps_location=regionService["latitude"].to_s+","+regionService["longitude"].to_s  
    usuario.lon=regionService["longitude"].to_s
    usuario.lat=regionService["latitude"].to_s
  end
  misDispositivos=AnetDevice.where(:id_owner=>session[:activeUser].id)
  # @ipsfromdevices=AnetLog.select("mip,arduino_id,max(id),id").group("arduino_id").order("id desc")
  @ipsfromdevices=Hash.new
  misDispositivos.each do |singleDevice|
    ultimoEstado=AnetLog.where(:arduino_id=>singleDevice.id).last
    puts ultimoEstado.inspect
    if ultimoEstado!=nil
      if ultimoEstado.mip!=nil
        singlefind = JSON.parse(open("http://api.ipstack.com/"+ultimoEstado.mip+"?access_key=b6cdef9c8216ecc77342f7f704fbe540").read)
        @ipsfromdevices[singleDevice.object]=singlefind["latitude"].to_s+","+singlefind["longitude"].to_s
      end  
    end
    
  end
  }
  rescue Exception => e
    
  end
  puts @ipsfromdevices.inspect
  usuario.save
  session[:activeUser]=usuario
  if usuario.gps_location!=nil
    @googleService = JSON.parse(open("http://maps.googleapis.com/maps/api/geocode/json?latlng="+usuario.gps_location+"&sensor=false").read)  
  end
  

            #if the user is not empty save the data
            # regionService["country_name"],:city=>regionService["region_name"]+","+regionService["city"]
         
        
    
   @anet_device=AnetDevice.new
   @registeredInterfaces=Api_Interface.where("visible=1")
   @open_devices=AnetDevice.where(:is_public=>1)
   @sys_user_phone=SysUserPhone.new



    
    # traemos 10 dispositivos nuevos que esten abiertos
    @open_devices=AnetDevice.where(:is_public=>1)
    
    @registeredInterfaces=Api_Interface.where("visible=1")
    @summaries = Summary.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @summaries }
    end
  end

  def set_location_v2
    if session[:activeUser]!=nil
      if params[:location]=="none"
        # buscar ubicación por ip
        
      else
        usuario=Sys_User.find_by_id(session[:activeUser].id)
        usuario.gps_location=params[:location]
        usuario.lon=params[:location].split(",")[1]
        usuario.lat=params[:location].split(",")[0]
        usuario.save
      end
    else
      redirect_to "https://www.suitch.network"
      return
    end
    # this is part of the claim process, attach, would auto assing devices to user
    # attachDevices()
    # aqui nos aseguramos que hayan registros
    if GeoSetting.where(:sys_user_id=>session[:activeUser].id).length<1
      nsetts=GeoSetting.new
      nsetts.sys_user_id=session[:activeUser].id
      nsetts.add_distancia=20
      nsetts.earthquakes=1
      nsetts.volcano=1
      nsetts.water=1
      nsetts.save
    end
    usuario=Sys_User.find_by_id(session[:activeUser].id)
    begin
      istatus = Timeout::timeout(3) {
        regionService = JSON.parse(open("http://api.ipstack.com/"+request.remote_ip.to_s+"?access_key=b6cdef9c8216ecc77342f7f704fbe540").read)
        puts "========="
        puts request.remote_ip.to_s
        puts usuario.inspect
        puts regionService.inspect
        puts "=========="
        # hay que evitar que se guarden cosas que estan vacias siempre y cuando no contemos con una posición anterior
        if regionService["city"]=="" and usuario.gps_location==nil
          usuario.gps_location=regionService["latitude"].to_s+","+regionService["longitude"].to_s
          usuario.lon=regionService["longitude"].to_s
          usuario.lat=regionService["latitude"].to_s
        elsif regionService["city"]!="" and usuario.gps_location!=nil
          usuario.gps_location=regionService["latitude"].to_s+","+regionService["longitude"].to_s  
          usuario.lon=regionService["longitude"].to_s
          usuario.lat=regionService["latitude"].to_s
        elsif regionService["city"]!="" and usuario.gps_location==nil
          usuario.gps_location=regionService["latitude"].to_s+","+regionService["longitude"].to_s  
          usuario.lon=regionService["longitude"].to_s
          usuario.lat=regionService["latitude"].to_s
        end
        misDispositivos=AnetDevice.where(:id_owner=>session[:activeUser].id)
        @ipsfromdevices=[]
        misDispositivos.each do |singleDevice|
          ultimoEstado=AnetLog.where(:arduino_id=>singleDevice.id).last
          puts ultimoEstado.inspect
          if ultimoEstado!=nil
            if ultimoEstado.mip!=nil
              singlefind = JSON.parse(open("http://api.ipstack.com/"+ultimoEstado.mip+"?access_key=b6cdef9c8216ecc77342f7f704fbe540").read)
              # @ipsfromdevices[singleDevice.object]=singlefind["latitude"].to_s+","+singlefind["longitude"].to_s
              device_location = Hash.new
              device_location["location"]=Hash.new
              device_location["location"]["lat"]=singlefind["latitude"]
              device_location["location"]["lng"]=singlefind["longitude"]
              device_location["type"]=singleDevice.version
              device_location["device"]=singleDevice.object
              @ipsfromdevices.push(device_location)
            end  
          end
        end
      }
    rescue Exception => e
    end
    usuario.save
    session[:activeUser]=usuario
    if usuario.gps_location!=nil
      @googleService = JSON.parse(open("http://maps.googleapis.com/maps/api/geocode/json?latlng="+usuario.gps_location+"&sensor=false").read)  
    end

    # @anet_device=AnetDevice.new
    # @registeredInterfaces=Api_Interface.where("visible=1")
    # @open_devices=AnetDevice.where(:is_public=>1)
    # @sys_user_phone=SysUserPhone.new
    # traemos 10 dispositivos nuevos que esten abiertos
    # @open_devices=AnetDevice.where(:is_public=>1)
    
    # @registeredInterfaces=Api_Interface.where("visible=1")
    # @summaries = Summary.all
    respond_to do |format|
      format.html { render json: @ipsfromdevices }
      format.json { render json: @ipsfromdevices }
    end
  end

  # GET /summaries/1
  # GET /summaries/1.json
  def show
    @summary = Summary.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @summary }
    end
  end

  # GET /summaries/new
  # GET /summaries/new.json
  def new
    @summary = Summary.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @summary }
    end
  end

  # GET /summaries/1/edit
  def edit
    @summary = Summary.find(params[:id])
  end

  # POST /summaries
  # POST /summaries.json
  def create
    @summary = Summary.new(params[:summary])

    respond_to do |format|
      if @summary.save
        format.html { redirect_to @summary, notice: 'Summary was successfully created.' }
        format.json { render json: @summary, status: :created, location: @summary }
      else
        format.html { render action: "new" }
        format.json { render json: @summary.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /summaries/1
  # PUT /summaries/1.json
  def update
    @summary = Summary.find(params[:id])

    respond_to do |format|
      if @summary.update_attributes(params[:summary])
        format.html { redirect_to @summary, notice: 'Summary was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @summary.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /summaries/1
  # DELETE /summaries/1.json
  def destroy
    @summary = Summary.find(params[:id])
    @summary.destroy

    respond_to do |format|
      format.html { redirect_to summaries_url }
      format.json { head :no_content }
    end
  end
end
