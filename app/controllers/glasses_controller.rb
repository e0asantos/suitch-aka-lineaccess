require 'google/api_client'
require 'google/api_client/client_secrets'
require 'google/api_client/auth/installed_app'
require '/usr/src/jarvixServer/jarvixSystem/Jarvix.rb'
class GlassesController < ApplicationController

  # this is the version from lineaccess.mx 

  def jarvixInterpreter
    # {"collection"=>"timeline", "itemId"=>"d18113ef-9d4d-4936-9f8c-1002b6428f23", "operation"=>"INSERT", "userToken"=>"user_10", "userActions"=>[{"type"=>"REPLY"}], "controller"=>"glasses", "action"=>"notify", "format"=>"json", "glass"=>{}}
    corrected=params[:response].gsub("=>", ":")
    objeto=JSON.parse(corrected)
    puts "====>"+objeto.to_json
    puts ".....>"+objeto.inspect
    if objeto['itemId']!="latest"
      
    
        client = Google::APIClient.new(:application_name=>"Suitch glass",:application_version=>"1.0.0")
        client.authorization.client_id = '356453616759.apps.googleusercontent.com'
        client.authorization.client_secret = 'OTWKozU0YTptxA4yqn4AeNuc'
        client.authorization.scope = 'https://www.googleapis.com/auth/glass.timeline'

        usuario=Sys_User.find_by_id(objeto['userToken'][5..-1].to_i)
        apitoken=Api_Token_User_Interface.where(:sys_user=>objeto['userToken'][5..-1].to_i,:api_interface=>6).last
        apitoken.forceUpdateToken()

        calendar = client.discovered_api('mirror', 'v1')
        msession=Hash.new
        msession[:access_token] = apitoken.token
        msession[:refresh_token] = apitoken.token_secret
        @authorization ||= (
            auth = client.authorization.dup
            auth.update_token!(msession)
            auth
          )


      result = client.execute(:api_method => calendar.timeline.get,
                                                    :parameters => {'id'=>objeto['itemId']},
                                    :authorization => @authorization)
      jsonObjeto=JSON.parse(result.body)
      puts jsonObjeto
      # ahora lo metemos en jarvix

      telefonoUsuario=SysUserPhone.find_by_sys_user(objeto['userToken'][5..-1].to_i)
      jarvixServer=Jarvix.new(telefonoUsuario.number,"en")
      jarvixServer.openPort
      jarvixServer.openTransmission
      jarvixServer.payAttention(jsonObjeto['text'])

        respond_to do |format|
          format.html # index.html.erb
          format.json { render json: "OK".to_json }
        end

      else

        respond_to do |format|
          format.html # index.html.erb
          format.json { render json: "OK".to_json }
        end

      end
  end
  # GET /glasses
  # GET /glasses.json
  def index
    @glasses = Glass.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @glasses }
    end
  end

  # GET /glasses/1
  # GET /glasses/1.json
  def show
    @glass = Glass.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @glass }
    end
  end

  # GET /glasses/new
  # GET /glasses/new.json
  def new
    @glass = Glass.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @glass }
    end
  end

  # GET /glasses/1/edit
  def edit
    @glass = Glass.find(params[:id])
  end

  # POST /glasses
  # POST /glasses.json
  def create
    @glass = Glass.new(params[:glass])

    respond_to do |format|
      if @glass.save
        format.html { redirect_to @glass, notice: 'Glass was successfully created.' }
        format.json { render json: @glass, status: :created, location: @glass }
      else
        format.html { render action: "new" }
        format.json { render json: @glass.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /glasses/1
  # PUT /glasses/1.json
  def update
    @glass = Glass.find(params[:id])

    respond_to do |format|
      if @glass.update_attributes(params[:glass])
        format.html { redirect_to @glass, notice: 'Glass was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @glass.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /glasses/1
  # DELETE /glasses/1.json
  def destroy
    @glass = Glass.find(params[:id])
    @glass.destroy

    respond_to do |format|
      format.html { redirect_to glasses_url }
      format.json { head :no_content }
    end
  end
end
