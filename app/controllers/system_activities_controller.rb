class SystemActivitiesController < ApplicationController
  # GET /system_activities
  # GET /system_activities.json
  def index
    @system_activities = SystemActivity.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @system_activities }
    end
  end

  # GET /system_activities/1
  # GET /system_activities/1.json
  def show
    @system_activity = SystemActivity.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @system_activity }
    end
  end

  # GET /system_activities/new
  # GET /system_activities/new.json
  def new
    @system_activity = SystemActivity.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @system_activity }
    end
  end

  # GET /system_activities/1/edit
  def edit
    @system_activity = SystemActivity.find(params[:id])
  end

  # POST /system_activities
  # POST /system_activities.json
  def create
    @system_activity = SystemActivity.new(params[:system_activity])

    respond_to do |format|
      if @system_activity.save
        format.html { redirect_to @system_activity, notice: 'System activity was successfully created.' }
        format.json { render json: @system_activity, status: :created, location: @system_activity }
      else
        format.html { render action: "new" }
        format.json { render json: @system_activity.errors, status: :unprocessable_entity }
      end
    end
  end

  def get_assistant_activity_v2
    if session[:activeUser] != nil
      device_tokens = AnetDevice.where(id_owner: session[:activeUser].id).pluck(:token)
      
      # constants mapping
      assistants_mapping = {
        0 => { "assistant" => "suitch", "provider" => "Consultware" },
        1 => { "assistant" => "siri", "provider" => "Apple" },
        2 => { "assistant" => "home", "provider" => "Google" },
        3 => { "assistant" => "alexa", "provider" => "Amazon" },
        4 => { "assistant" => "nest", "provider" => "Google" },
        5 => { "assistant" => "acurite", "provider" => "Acurite" }
      }
  
      assistants = SystemActivity
        .where(virtual_assistant: [0,1,2,3,5], device_token: device_tokens)
        .where('updated_at > ?', DateTime.now-7.days)
        .group(:virtual_assistant)
        .count
        .map do |assistant, count|
          assistants_mapping[assistant].merge("count" => count)
        end
  
      # special case for nest assistant
      if session[:activeUser].nest_channel != nil
        nest_count = SystemActivity
          .where(virtual_assistant: 4, device_token: session[:activeUser].nest_channel)
          .where('updated_at > ?', DateTime.now-7.days)
          .count
        assistants << assistants_mapping[4].merge("count" => nest_count)
      else
        assistants << assistants_mapping[4].merge("count" => 0)
      end
  
      # special case for acurite assistant
      acurite_count = SystemActivity
        .where(virtual_assistant: 5, device_token: "#{session[:activeUser].user_id}-acurite")
        .count
      assistants << assistants_mapping[5].merge("count" => acurite_count)
    else
      assistants = []
    end
  
    respond_to do |format|
      format.html { render json: assistants.to_json }
      format.json { render json: assistants.to_json }
    end
  end
  

  def get_assistant_activity_v2_old
    assistants = []
    # virtual assistant constants
    # suitch = 0
    # siri = 1
    # google = 2
    # alexa = 3
    # nest = 4
    # acurite = 5
    if session[:activeUser] != nil
      device_tokens = AnetDevice.where(:id_owner=>session[:activeUser].id).pluck(:token)
      singleAssistant_0 = Hash.new
      singleAssistant_0["assistant"] = "suitch"
      singleAssistant_0["provider"] = "Consultware"
      singleAssistant_0["count"] = SystemActivity.where(:virtual_assistant=>0,:device_token=>device_tokens).where('updated_at > ?', DateTime.now-7.days).count

      singleAssistant_1 = Hash.new
      singleAssistant_1["assistant"] = "siri"
      singleAssistant_1["provider"] = "Apple"
      singleAssistant_1["count"] = SystemActivity.where(:virtual_assistant=>1,:device_token=>device_tokens).where('updated_at > ?', DateTime.now-7.days).count

      singleAssistant_2 = Hash.new
      singleAssistant_2["assistant"] = "home"
      singleAssistant_2["provider"] = "Google"
      singleAssistant_2["count"] = SystemActivity.where(:virtual_assistant=>2,:device_token=>device_tokens).where('updated_at > ?', DateTime.now-7.days).count

      singleAssistant_3 = Hash.new
      singleAssistant_3["assistant"] = "alexa"
      singleAssistant_3["provider"] = "Amazon"
      singleAssistant_3["count"] = SystemActivity.where(:virtual_assistant=>3,:device_token=>device_tokens).where('updated_at > ?', DateTime.now-7.days).count


      singleAssistant_4 = Hash.new
      singleAssistant_4["assistant"] = "nest"
      singleAssistant_4["provider"] = "Google"
      if session[:activeUser].nest_channel!=nil
        singleAssistant_4["count"] = SystemActivity.where(:virtual_assistant=>4,:device_token=>session[:activeUser].nest_channel).where('updated_at > ?', DateTime.now-7.days).count
      else
        singleAssistant_4["count"] = 0
      end

      singleAssistant_5 = Hash.new
      singleAssistant_5["assistant"] = "acurite"
      singleAssistant_5["provider"] = "Acurite"
      singleAssistant_5["count"] = SystemActivity.where(:virtual_assistant=>5,:device_token=>session[:activeUser].user_id+"-acurite").count

      assistants.push(singleAssistant_0)
      assistants.push(singleAssistant_1)
      assistants.push(singleAssistant_2)
      assistants.push(singleAssistant_3)
      assistants.push(singleAssistant_4)
      assistants.push(singleAssistant_5)

      
    end
    respond_to do |format|
      format.html { render json: assistants.to_json }
      format.json { render json: assistants.to_json }
    end
  end

  # PUT /system_activities/1
  # PUT /system_activities/1.json
  def update
    @system_activity = SystemActivity.find(params[:id])

    respond_to do |format|
      if @system_activity.update_attributes(params[:system_activity])
        format.html { redirect_to @system_activity, notice: 'System activity was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @system_activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /system_activities/1
  # DELETE /system_activities/1.json
  def destroy
    @system_activity = SystemActivity.find(params[:id])
    @system_activity.destroy

    respond_to do |format|
      format.html { redirect_to system_activities_url }
      format.json { head :no_content }
    end
  end
end
