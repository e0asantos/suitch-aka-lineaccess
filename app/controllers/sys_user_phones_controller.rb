require 'sys_user.rb'
class SysUserPhonesController < ApplicationController
  # GET /sys_user_phones
  # GET /sys_user_phones.json
  def index
    @sys_user_phones = SysUserPhone.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sys_user_phones }
    end
  end

  # GET /sys_user_phones/1
  # GET /sys_user_phones/1.json
  def show
    @sys_user_phone = SysUserPhone.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sys_user_phone }
    end
  end

  # GET /sys_user_phones/new
  # GET /sys_user_phones/new.json
  def new
    @sys_user_phone = SysUserPhone.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sys_user_phone }
    end
  end

  # GET /sys_user_phones/1/edit
  def edit
    @sys_user_phone = SysUserPhone.find(params[:id])
    render :layout => false
  end

  # POST /sys_user_phones
  # POST /sys_user_phones.json
  def create
    puts "----------------PHONE CREATE----------------"
    @sys_user_phone = SysUserPhone.new(params[:sys_user_phone])
    @sys_user_phone.sys_user=session[:activeUser].id
    respond_to do |format|
      if @sys_user_phone.save
        format.html { redirect_to "/personal_accounts", notice: 'A new phone was successfully created.' }
        format.json { render json: @sys_user_phone, status: :created, location: @sys_user_phone }
      else
        format.html { render action: "new" }
        format.json { render json: @sys_user_phone.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sys_user_phones/1
  # PUT /sys_user_phones/1.json
  def update
    @sys_user_phone = SysUserPhone.find(params[:id])

    respond_to do |format|
      if @sys_user_phone.update_attributes(params[:sys_user_phone])
        format.html { redirect_to "/personal_accounts", notice: 'The phone was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sys_user_phone.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sys_user_phones/1
  # DELETE /sys_user_phones/1.json
  def destroy
    puts "-----destroy1-----"
    @sys_user_phone = SysUserPhone.find(params[:id])
    sid=@sys_user_phone.sys_user
    @sys_user_phone.destroy
    puts "-----destroy2-----"
    #recargar
    session[:activeUser]=Sys_User.find_by_user_id(sid)
    redirect_to "/personal_accounts", notice: 'The phone was successfully updated.'
    return
    puts "-----destroy3-----"
    respond_to do |format|
      format.html { redirect_to "/personal_accounts" }
      format.json { head :no_content }
    end
  end
end
