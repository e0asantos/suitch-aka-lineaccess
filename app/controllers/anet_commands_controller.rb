class AnetCommandsController < ApplicationController
  # GET /anet_commands
  # GET /anet_commands.json
  def index
    @anet_commands = AnetCommand.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @anet_commands }
    end
  end

  # GET /anet_commands/1
  # GET /anet_commands/1.json
  def show
    result = Hash.new
    result["result"] = "error"

    if session[:activeUser] != nil 
      ownership=AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
      if ownership!= nil
        @anet_command = AnetCommand.where(:arduino_id=>ownership.id)
        result["result"] = "success"
      end
    end

    respond_to do |format|
      if result["result"] == "success"
        format.json { render json: @anet_command.to_json, status:200}
      else
        format.json { render json: result.to_json, status:401 }
      end
    end

    
  end

  # GET /anet_commands/new
  # GET /anet_commands/new.json
  def new
    @anet_command = AnetCommand.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @anet_command }
    end
  end

  # GET /anet_commands/1/edit
  def edit
    @anet_command = AnetCommand.find(params[:id])
  end

  # POST /anet_commands
  # POST /anet_commands.json
  def create
    result = Hash.new
    result["result"] = "error"

    if session[:activeUser] != nil 
      my_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
      if my_device != nil
        @anet_command = AnetCommand.new
        # we receive the token now, not the device id anymore
        @anet_command.command=params[:command]
        @anet_command.arduino_id = my_device.id
        @anet_command.command_data=params[:command_data]
        @anet_command.is_only_question = params[:is_only_question].to_i
        if params.has_key?("unit")
          @anet_command.unit = params[:unit]
        end
        @anet_command.save
        result["result"] = "success"
        uri = URI.parse("https://s.suitch.network/smarthome/create")
        https = Net::HTTP.new(uri.host,uri.port)
        https.use_ssl = false
        req = Net::HTTP::Post.new(uri)
        req['Content-Type'] = 'application/json'
        req.body = {userId: session[:activeUser].id.to_s}.to_json
        response = https.request(req)
      end
    end
    
    respond_to do |format|
      if result["result"] == "success"
        
        format.html { redirect_to "/personal_accounts", notice: 'Anet command was successfully created.' }
        format.json { render json: @anet_command, status: :created }
      else
        format.html { render action: "new" }
        format.json { render json: result.to_json, status: :unprocessable_entity }
      end
    end
  end

  # PUT /anet_commands/1
  # PUT /anet_commands/1.json
  def update
    @anet_command = AnetCommand.find(params[:id])
    @anet_command.command=@anet_command.command.downcase
    @anet_command.location=@anet_command.location.downcase
    respond_to do |format|
      if @anet_command.update_attributes(params[:anet_command])
        format.html { redirect_to @anet_command, notice: 'Anet command was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @anet_command.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /anet_commands/1
  # DELETE /anet_commands/1.json
  def destroy
    result = Hash.new
    result["result"] = "error"
    @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
    if @anet_device.id != nil
      @anet_command = AnetCommand.where(:arduino_id=>@anet_device.id,:id=>params[:id]).last
      if @anet_command != nil
        # remove also all its history data, we dont need trash in the db
        command_values = AnetValue.where(:arduino_id=>@anet_device.id,:command_id=>@anet_command.id)
        command_values.destroy_all
        # finally, destroy the command
        @anet_command.destroy
        result["result"] = "success"
      end
    end

    respond_to do |format|
      if result["result"] == "success"
        # format.html { redirect_to "/personal_accounts",notice: 'Devices list was updated.' }
        format.json { render json: result.to_json, status:200}
      else
        # format.html { redirect_to "/personal_accounts",notice: 'Error while updating' }
        format.json { render json: result.to_json, status:401 }
      end
    end

    # respond_to do |format|
    #   format.html { redirect_to anet_commands_url }
    #   format.json { head :no_content }
    # end
  end
end
