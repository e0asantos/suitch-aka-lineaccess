require 'rubygems'
# require 'bing_translator'
require 'sys_phrases.rb'
require 'sys_region.rb'
require 'sys_region_lang.rb'
require 'sys_user_phone.rb'
require 'sys_phone_type.rb'
require 'api_interface.rb'
require 'api_interface.rb'
require 'sys_user.rb'
require 'anet_device.rb'
require 'likes.rb'

class PersonalAccountsController < ApplicationController
  # GET /personal_accounts
  # GET /personal_accounts.json
  def index
    @sys_user_phone=SysUserPhone.new
    @anet_device=AnetDevice.new
    if session[:activeUser]==nil
      redirect_to "/auth/facebook"
      return
    end
    attachDevices()
    # traemos 10 dispositivos nuevos que esten abiertos
    @open_devices=AnetDevice.where(:is_public=>1)
    @personal_accounts = Sys_Phrase.all
    @registeredInterfaces=Api_Interface.where("visible=1")
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @personal_accounts }
    end
  end

  # GET /personal_accounts/1
  # GET /personal_accounts/1.json
  def show
    redirect_to "/personal_accounts"
    return
    if session[:activeUser]==nil
      redirect_to "/"
      return
    end
    @personal_account = Sys_Phrase.find(params[:id])
    @registeredInterfaces=Api_Interface.where("visible=1")
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @personal_account }
    end
  end

  # GET /personal_accounts/new
  # GET /personal_accounts/new.json
  def new
    redirect_to "/personal_accounts"
    return
    @personal_account = Sys_Phrase.new
    @registeredInterfaces=Api_Interface.where("visible=1")
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @personal_account }
    end
  end

  # GET /personal_accounts/1/edit
  def edit
    redirect_to "/personal_accounts"
    return
    @registeredInterfaces=Api_Interface.where("visible=1")
    @personal_account = Sys_Phrase.find(params[:id])
  end

  # POST /personal_accounts
  # POST /personal_accounts.json
  def create
    redirect_to "/personal_accounts"
    return
    @registeredInterfaces=Api_Interface.where("visible=1")
    @personal_account = Sys_Phrase.new(params[:personal_account])

    respond_to do |format|
      if @personal_account.save
        format.html { redirect_to @personal_account, notice: 'Personal account was successfully created.' }
        format.json { render json: @personal_account, status: :created, location: @personal_account }
      else
        format.html { render action: "new" }
        format.json { render json: @personal_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /personal_accounts/1
  # PUT /personal_accounts/1.json
  def update
    redirect_to "/personal_accounts"
    return
    @registeredInterfaces=Api_Interface.where("visible=1")
    @personal_account = Sys_Phrase.find(params[:id])

    respond_to do |format|
      if @personal_account.update_attributes(params[:personal_account])
        format.html { redirect_to @personal_account, notice: 'Personal account was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @personal_account.errors, status: :unprocessable_entity }
      end
    end
  end

  #this changes the language of the spoken user
  def changeLanguage
    puts params.inspect
    if session[:activeUser]==nil
      redirect_to "/auth/facebook"
      return
    end
    session[:activeUser].lang=params[:lang]
    currentUser=Sys_User.find_by_id(session[:activeUser].id)
    currentUser.lang=params[:lang]
    currentUser.save
    session[:activeUser]=currentUser
    redirect_to "/personal_accounts"
    return
  end

  def likeIt
    if session[:activeUser]==nil
      redirect_to "/login"
      return
    end
    myLike=Likes.where(:user_liked=>session[:activeUser].id)
    if myLike.length>0
      #entonces ya hay un like ahi, solo lo vamos a quitarle el like
      myLike.each do |singleLike|
        singleLike.destroy
      end
    else
      ilikeit=Likes.new(:device_id=>params[:deviceid],:user_liked=>session[:activeUser].id)
      ilikeit.save
    end
    render :nothing => true, :status => 200
  end
  # DELETE /personal_accounts/1
  # DELETE /personal_accounts/1.json
  def destroy
    redirect_to "/personal_accounts"
    return
    @registeredInterfaces=Api_Interface.where("visible=1")
    @personal_account = Sys_Phrase.find(params[:id])
    @personal_account.destroy

    respond_to do |format|
      format.html { redirect_to personal_accounts_url }
      format.json { head :no_content }
    end
  end

  def logmeout
    puts "--------------LOG OUT-----------"
  end
end
