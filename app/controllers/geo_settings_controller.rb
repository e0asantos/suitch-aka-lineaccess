class GeoSettingsController < ApplicationController
  # GET /geo_settings
  # GET /geo_settings.json
  def index
    @geo_settings = GeoSetting.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @geo_settings }
    end
  end

  # GET /geo_settings/1
  # GET /geo_settings/1.json
  def show
    @geo_setting = GeoSetting.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @geo_setting }
    end
  end

  # GET /geo_settings/new
  # GET /geo_settings/new.json
  def new
    @geo_setting = GeoSetting.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @geo_setting }
    end
  end

  # GET /geo_settings/1/edit
  def edit
    @geo_setting = GeoSetting.find(params[:id])
  end

  # POST /geo_settings
  # POST /geo_settings.json
  def create
    @geo_setting = GeoSetting.new(params[:geo_setting])

    respond_to do |format|
      if @geo_setting.save
        format.html { redirect_to @geo_setting, notice: 'Geo setting was successfully created.' }
        format.json { render json: @geo_setting, status: :created, location: @geo_setting }
      else
        format.html { render action: "new" }
        format.json { render json: @geo_setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /geo_settings/1
  # PUT /geo_settings/1.json
  def update
    @geo_setting = GeoSetting.find(params[:id])

    respond_to do |format|
      if @geo_setting.update_attributes(params[:geo_setting])
        format.html { redirect_to @geo_setting, notice: 'Geo setting was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @geo_setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /geo_settings/1
  # DELETE /geo_settings/1.json
  def destroy
    @geo_setting = GeoSetting.find(params[:id])
    @geo_setting.destroy

    respond_to do |format|
      format.html { redirect_to geo_settings_url }
      format.json { head :no_content }
    end
  end
end
