class LayoutBuilderController < ApplicationController
  def create
    result = Hash.new
    result["result"] = "error"

    if session[:activeUser] != nil 
      my_layout = LayoutBuilder.new
      my_layout.description = params[:description]
      my_layout.name = params[:name]
      my_layout.owner_id = session[:activeUser].id
      my_layout.save
      result["result"] = "success"
    end

    respond_to do |format|
      if result["result"] == "success"
        format.json { render json: result.to_json, status: :created }
      else
        format.json { render json: result.to_json, status: :unprocessable_entity }
      end
    end
  end

  def show
    result = Hash.new
    result["result"] = "error"

    if session[:activeUser] != nil 
      my_layouts = LayoutBuilder.where(:owner_id=>session[:activeUser].id)
      result["result"] = "success"
    end

    respond_to do |format|
      if result["result"] == "success"
        format.json { render json: my_layouts, status: 200 }
      else
        format.json { render json: result.to_json, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    result = Hash.new
    result["result"] = "error"

    if session[:activeUser] != nil 
      @my_layout = LayoutBuilder.where(:owner_id=>session[:activeUser].id,:id=>params[:id]).last
      if @my_layout != nil
        result["result"] = "success"
        @my_layout.destroy
      end
    end

    respond_to do |format|
      if result["result"] == "success"
        format.json { render json: result.to_json, status: 200 }
      else
        format.json { render json: result.to_json, status: :unprocessable_entity }
      end
    end
  end

  def update
    result = Hash.new
    result["result"] = "error"

    if session[:activeUser] != nil 
      @my_layout = LayoutBuilder.where(:owner_id=>session[:activeUser].id,:id=>params[:id]).last
      if @my_layout != nil
        result["result"] = "success"
        @my_layout.content = params[:layout]
        @my_layout.save
      end
    end

    respond_to do |format|
      if result["result"] == "success"
        format.json { render json: result.to_json, status: 200 }
      else
        format.json { render json: result.to_json, status: :unprocessable_entity }
      end
    end
  end
end
