class FeedReadersController < ApplicationController
  # GET /feed_readers
  # GET /feed_readers.json
  def index
    @feed_readers = FeedReader.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @feed_readers }
    end
  end

  # GET /feed_readers/1
  # GET /feed_readers/1.json
  def show
    @feed_reader = FeedReader.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @feed_reader }
    end
  end

  # GET /feed_readers/new
  # GET /feed_readers/new.json
  def new
    @feed_reader = FeedReader.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @feed_reader }
    end
  end

  # GET /feed_readers/1/edit
  def edit
    @feed_reader = FeedReader.find(params[:id])
  end

  # POST /feed_readers
  # POST /feed_readers.json
  def create
    @feed_reader = FeedReader.new(params[:feed_reader])

    respond_to do |format|
      if @feed_reader.save
        format.html { redirect_to "/summaries", notice: 'Se creó una nueva fuente de alertas.' }
        format.json { render json: @feed_reader, status: :created, location: @feed_reader }
      else
        format.html { render action: "new" }
        format.json { render json: @feed_reader.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /feed_readers/1
  # PUT /feed_readers/1.json
  def update
    @feed_reader = FeedReader.find(params[:id])

    respond_to do |format|
      if @feed_reader.update_attributes(params[:feed_reader])
        format.html { redirect_to "/summaries", notice: 'Se actualizó la fuente de alertas.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @feed_reader.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /feed_readers/1
  # DELETE /feed_readers/1.json
  def destroy
    @feed_reader = FeedReader.find(params[:id])
    @feed_reader.destroy

    respond_to do |format|
      format.html { redirect_to feed_readers_url }
      format.json { head :no_content }
    end
  end
end
