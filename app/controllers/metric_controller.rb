class MetricController < ApplicationController
  protect_from_forgery :except => [:logInJSON]
  def show
  	respond_to do |format|
      format.html { render json: "ok" }
      format.json { render json: "ok" }
    end
  end

  def execJSON
    permission=params[:supertoken]
    device=params[:token]
    inaction=params[:command]
    respuesta="Something went wrong."
    # buscamos el token del reloj
    reloj=Smartwatch.find_by_token(permission)
    puts reloj.inspect
    if reloj!=nil
      dispositivos=AnetDevice.where(:id_owner=>reloj.sys_user_id,:token=>device).index_by(&:id).keys
      if dispositivos.length>0
        # ahora hay que checar si el token es del supertoken
        #
        commandsToExecute=AnetCommand.where(:arduino_id=>dispositivos,:command_data=>inaction)
        s = TCPSocket.new 'localhost', 3000
        while line = s.gets # Read lines from socket
        puts line.inspect
        if line.index("TOKEN?")!=nil
            puts "enviando"
           s.puts "025621de/d822339f\n" 
        elsif line.index("WLCME")!=nil
            puts "WLCME."
            respuesta= "The command was sent to the device."
            commandsToExecute.each do |singleCommand|
                dispositivo=AnetDevice.find_by_id(singleCommand.arduino_id)
                if dispositivo!=nil
                  puts dispositivo.token
                  s.puts "sendto "+dispositivo.token+" "+singleCommand.command_data+"\n"
                  break
                end
            end
            break
        end
    end
    
    s.close
      else
        respuesta="There are no devices available."
      end
    else
      respuesta="The token was not found, sorry."
    end
    
    
    #ahora comprobar la ejecucion
    

    respond_to do |format|
      format.html { render json: respuesta }
      format.json { render json: respuesta.to_json }
    end
    
  end

  def logInJSON
    arrayDatos=[]
  	
    # aqui debemos comprobar si este token corresponde a alguien
    watch=Smartwatch.find_by_token(params[:token])
    if watch!=nil
      # hay que buscar los dispositivos y bajar los tokens con sus respectivos metodos
      dispositivos=AnetDevice.where(:id_owner=>watch.sys_user_id)

      # obtener solo el token y sus metodos
      dispositivos.each do |singleDevice|
        comandos=AnetCommand.where(:arduino_id=>singleDevice.id)
        objetoDispositivo=Hash.new
        objetoDispositivo[:token]=singleDevice.token
        objetoDispositivo[:device_name]=singleDevice.object
        objetoDispositivo[:commands]=[]
        # formar el json
        comandos.each do |singleCommand|
          objetoHash=Hash.new
          objetoHash[:token]=singleDevice.token
          objetoHash[:command_name]=singleCommand.command
          objetoHash[:command_method]=singleCommand.command_data
          objetoDispositivo[:commands].push(objetoHash)
        end
        arrayDatos.push(objetoDispositivo)
        
      end

    else
      objetoHash=Hash.new
          objetoHash[:token]=params[:token]
          objetoHash[:device_name]="not_found"
          
      arrayDatos.push(objetoHash)
    end
  	
  	respond_to do |format|
      format.html { render json: "ok" }
      format.json { render json: arrayDatos.to_json }
    end
  end
end
