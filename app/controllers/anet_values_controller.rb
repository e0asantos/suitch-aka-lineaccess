class AnetValuesController < ApplicationController
  # GET /anet_values
  # GET /anet_values.json
  def index
    @anet_values = AnetValue.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @anet_values }
    end
  end

  # GET /anet_values/1
  # GET /anet_values/1.json
  def show
    @anet_value = AnetValue.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @anet_value }
    end
  end

  # GET /anet_values/new
  # GET /anet_values/new.json
  def new
    @anet_value = AnetValue.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @anet_value }
    end
  end

  # GET /anet_values/1/edit
  def edit
    @anet_value = AnetValue.find(params[:id])
  end

  # POST /anet_values
  # POST /anet_values.json
  def create
    @anet_value = AnetValue.new(params[:anet_value])

    respond_to do |format|
      if @anet_value.save
        format.html { redirect_to @anet_value, notice: 'Anet value was successfully created.' }
        format.json { render json: @anet_value, status: :created, location: @anet_value }
      else
        format.html { render action: "new" }
        format.json { render json: @anet_value.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /anet_values/1
  # PUT /anet_values/1.json
  def update
    @anet_value = AnetValue.find(params[:id])

    respond_to do |format|
      if @anet_value.update_attributes(params[:anet_value])
        format.html { redirect_to @anet_value, notice: 'Anet value was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @anet_value.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /anet_values/1
  # DELETE /anet_values/1.json
  def destroy
    @anet_value = AnetValue.find(params[:id])
    @anet_value.destroy

    respond_to do |format|
      format.html { redirect_to anet_values_url }
      format.json { head :no_content }
    end
  end
end
