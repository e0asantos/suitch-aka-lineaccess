class ChainReactionController < ApplicationController
    def create
      result = Hash.new
      result["result"] = "error"
      if session[:activeUser] != nil 
        current_device = AnetDevice.where(:id_owner=>session[:activeUser].id,:token=>params[:token]).last

        if current_device!= nil
          my_layout = SysReactions.new
          my_layout.device_id = current_device.id
          # my_layout.reactor = params[:name]
          my_layout.user_id = session[:activeUser].id
          my_layout.save
          result["result"] = "success"
        elsif params[:token]=="nest" || params[:token]=="acurite" || params[:token]=="ecowitt"
          my_layout = SysReactions.new

          my_layout.device_id = case params[:token]
          when "nest" then -4
          when "acurite" then -5
          when "ecowitt" then -6
          end

          # my_layout.reactor = params[:name]
          my_layout.user_id = session[:activeUser].id
          my_layout.save
          result["result"] = "success"
        end
      end
  
      respond_to do |format|
        if result["result"] == "success"
          format.json { render json: result.to_json, status: :created }
        else
          format.json { render json: result.to_json, status: :unprocessable_entity }
        end
      end
    end
  
    def show
      result = Hash.new
      result["result"] = "error"
  
      if session[:activeUser] != nil 
        # my_reactions = SysReactions.where(:user_id=>session[:activeUser].id)
        # select * from sys_reactions left join(select * from anet_devices) ad on sys_reactions.device_id=ad.id where user_id=337;
        reactions_sql = "CALL spChainReactionList(#{session[:activeUser].id})"
        temp_res= ActiveRecord::Base.connection.exec_query(reactions_sql).to_a
        result["result"] = "success"
      end
  
      respond_to do |format|
        if result["result"] == "success"
          format.json { render json: temp_res, status: 200 }
        else
          format.json { render json: result.to_json, status: :unprocessable_entity }
        end
      end
    end
  
    def destroy
      result = Hash.new
      result["result"] = "error"
  
      if session[:activeUser] != nil 
        @my_layout = SysReactions.where(:user_id=>session[:activeUser].id,:device_id=>params[:id]).last
        if @my_layout != nil
          result["result"] = "success"
          @my_layout.destroy
        end
      end
  
      respond_to do |format|
        if result["result"] == "success"
          format.json { render json: result.to_json, status: 200 }
        else
          format.json { render json: result.to_json, status: :unprocessable_entity }
        end
      end
    end
  
    def update
      result = Hash.new
      result["result"] = "error"
  
      if session[:activeUser] != nil 
        @my_layout = SysReactions.where(:user_id=>session[:activeUser].id,:device_id=>params[:id]).last
        if @my_layout != nil
          result["result"] = "success"
          @my_layout.reactor = params[:layout]
          @my_layout.device_ids = params[:devicesIds]
          
          @my_layout.save
        end
      end
  
      respond_to do |format|
        if result["result"] == "success"
          format.json { render json: result.to_json, status: 200 }
        else
          format.json { render json: result.to_json, status: :unprocessable_entity }
        end
      end
    end
  end
  