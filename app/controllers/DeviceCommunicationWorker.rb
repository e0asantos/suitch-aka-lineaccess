require 'anet_log.rb'
require 'anet_value.rb'
require 'anet_command.rb'
class DeviceCommunicationJob
  include SuckerPunch::Job

  def perform(params)
    puts "JOB START=====>"
    my_token=params["my_token"]
    my_secret_token=params["my_secret_token"]
    my_content=params["my_content"]
    puts params
    puts my_token
    puts my_secret_token
    puts my_content
    s = TCPSocket.new 'localhost', 3000
    while line = s.gets # Read lines from socket
        puts line.inspect
        if line.index("TOKEN?")!=nil
          puts "tokenizing sender... by job"
          # get a global account to send messages
          global_clients = AnetDevice.where(:location=>"server", :object=>"global")
          selected_global_client = global_clients[rand(global_clients.length)]
          puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
          s.puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
        elsif line.index("WLCME")!=nil
            puts "from Google Home"
            logger.info "from Google Home"
            @anet_device = AnetDevice.where(:token=>my_token).last 
            puts @anet_device.inspect
            @anet_commands=AnetCommand.where(:arduino_id=>@anet_device.id, :command_data=>my_content)
            @anet_commands_toggle=AnetCommand.where(:arduino_id=>@anet_device.id)
            if @anet_device.version == "sfir_alpha0"
              ["garagex","hallx","porchx"].each do |singleCommand|
                logger.warn("sfir.alpha0 sendto "+my_token+" "+singleCommand)
                logger.info("sfir.alpha0 sendto "+my_token+" "+singleCommand)
                s.puts "sendto "+my_token+" "+singleCommand+"\n"
                sleep 1.5
              end
            elsif @anet_device.version != "sfir"
              logger.info "sfir"
              puts "sendto "+my_token+" "+my_content
              s.puts "sendto "+my_token+" "+my_content+"\n\r"
              # sleep 1
            else
              puts "sendto "+my_token+" allx"
              # s.puts "sendto "+params[:token]+" allx\n"
              s.puts "sendto "+my_token+" "+my_content+"\n\r"
              # sleep 1
            end
            break
        end
    end
  s.close
  puts "JOB END=====>"
  end
end