class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_locale

  
  private
  def extract_locale_from_accept_language_header
  	begin
  		request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first	
  	rescue Exception => e
  		
  	end
    
  end

	def set_locale
		# cargar el idioma de acuerdo a la configuracion del usuario
		# -----> comento esta parte para no depender del idioma del usuario sino del navegador
		# if session[:activeUser]!=nil
		# 	puts "IDIOMA USUARIO:"+session[:activeUser].lang
		# 	I18n.locale = session[:activeUser].lang || I18n.default_locale
		# end
		
	  logger.debug "* Accept-Language: #{request.env['HTTP_ACCEPT_LANGUAGE']}"
	  I18n.locale = extract_locale_from_accept_language_header
	  logger.debug "* Locale set to '#{I18n.locale}'"
	end

	def attachDevices
		if session[:activeUser]!=nil
			# porAsignar=AnetDevice.where(:mip=>request.remote_ip.to_s).where("id_owner is NULL")
			# porAsignar.each do |singleDevice|
			# 	singleDevice.id_owner=session[:activeUser].id
			# 	singleDevice.save
			# end
		end
	end
end
