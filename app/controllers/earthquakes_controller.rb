require 'xmpp4r_facebook'
require 'xmpp4r/roster'
require 'fb_graph'
require "uuidtools"
require "open3"
require 'fileutils'
class EarthquakesController < ApplicationController
  # GET /earthquakes
  # GET /earthquakes.json
  def index
    @earthquakes = Earthquake.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @earthquakes }
    end
  end

  # GET /earthquakes/1
  # GET /earthquakes/1.json
  def show
    @earthquake = Earthquake.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @earthquake }
    end
  end

  # GET /earthquakes/new
  # GET /earthquakes/new.json
  def new
    @earthquake = Earthquake.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @earthquake }
    end
  end

  # GET /earthquakes/1/edit
  def edit
    @earthquake = Earthquake.find(params[:id])
  end

  # POST /earthquakes
  # POST /earthquakes.json
  def create
    @earthquake = Earthquake.new
    @earthquake.rawData=params[:plain]
    @earthquake.gateway="USGS"
    
    algo2=params[:plain].split(" ")
    zona=params[:plain].split(" of ")
    zona1=zona[1].split("Z=")
    flong=algo2[5][0..-1].to_f
    flat=algo2[4][0..-1].to_f
    if algo2[5].index("W")!=nil
      flong=-1*flong
    end
    if algo2[4].index("S")!=nil
      flat=-1*flat
    end
    @earthquake.latitude=flong
    @earthquake.longitude=flat
    @earthquake.save
    # PRELIM: M6.9 14:54 7/21 19.8S 178.4W 100 km NNE of Ndoi Island, Fiji FIJI REGION Z=616km us b000ruzk 80e81 reply STOP to unsub
    # todo=Sys_User.where("lat IS NOT NULL").where("lon IS NOT NULL").joins(:geo_settings).select("*")
    iDList=Sys_User.where("acos(sin(sys_users.lat * 0.0175) * sin("+flong.to_s+" * 0.0175) + cos(sys_users.lat * 0.0175) * cos("+flong.to_s+" * 0.0175) * cos(("+flat.to_s+" * 0.0175) - (sys_users.lon * 0.0175))) * 3959 <= geo_settings.add_distancia*.62137").joins(:geo_settings).select("geo_settings.add_distancia,sys_users.id").where("geo_settings.earthquakes=1").index_by(&:id).keys
    notifyToInbox(iDList,"se detectó un terremoto cerca de tu zona en "+zona1[0]+" te recomendamos tomar las precauciones necesarias.")
    # iDList.each do |singlesett|
    #     puts singlesett.add_distancia
        
    # end
    # hay que buscar a los usuarios
    render :nothing => true, :status => 200
  end

  def notifyToInbox(iDList,mensaje)
    token=Api_Token_User_Interface.where(:sys_user=>33,:api_interface=>2).last
    iDList.each do |singleList|
      direccionString=Sys_User.find_by_id(singleList)
      if direccionString!=nil
        cuerpoMensaje='Hola '+direccionString.name.to_s+', '+mensaje
        puts cuerpoMensaje.inspect
        id = '-100006392581606@chat.facebook.com'
        if direccionString.user_id!=nil
          
        
            to = '-'+direccionString.user_id+'@chat.facebook.com'
          #buscar producto
            body = cuerpoMensaje
            subject = 'no titulo'
            message = Jabber::Message.new to, body
            message.subject = subject
            
            client = Jabber::Client.new Jabber::JID.new(id)
            client.connect
            puts token.token
            client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)            
            client.send message
            client.close
          end
        end
    end
  end

  # PUT /earthquakes/1
  # PUT /earthquakes/1.json
  def update
    @earthquake = Earthquake.find(params[:id])

    respond_to do |format|
      if @earthquake.update_attributes(params[:earthquake])
        format.html { redirect_to @earthquake, notice: 'Earthquake was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @earthquake.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /earthquakes/1
  # DELETE /earthquakes/1.json
  def destroy
    @earthquake = Earthquake.find(params[:id])
    @earthquake.destroy

    respond_to do |format|
      format.html { redirect_to earthquakes_url }
      format.json { head :no_content }
    end
  end
end
