require 'uuidtools'
require 'sys_phrases.rb'
require 'anet_log.rb'
require 'anet_value.rb'
require 'anet_command.rb'
require 'anet_less_sensor.rb'
require 'battery_cycles.rb'
require 'user_device_notifications.rb'
require 'socket'
require 'json'
require "base64"
require '/usr/src/jarvixServer/db/model/sys_dhcp.rb'
require 'date'

require 'uri'
require 'net/http'
require 'net/https'
require 'json'

# require '/home/suitch/app/controllers/DeviceCommunicationWorker.rb'

class Object
  def is_number?
    to_f.to_s == to_s || to_i.to_s == to_s
  end
end
class AnetDevicesController < ApplicationController
  # GET /anet_devices
  # GET /anet_devices.json
  def index
    @anet_devices = AnetDevice.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @anet_devices }
    end
  end

  # GET /anet_devices/1
  # GET /anet_devices/1.json
  def show
    if params[:id] == "claim"
      @anet_device = AnetDevice.new
    else 
      if params[:id].is_number? == true
        @anet_device = AnetDevice.where(:id=>params[:id], :id_owner=>session[:activeUser].id).last
      else
        @anet_device = AnetDevice.where(:token=>params[:id],:id_owner=>session[:activeUser].id).last
      end
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @anet_device }
    end
  end

  # GET /anet_devices/new
  # GET /anet_devices/new.json
  def new
    @anet_device = AnetDevice.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @anet_device }
    end
  end

  # GET /anet_devices/1/edit
  def edit
    @anet_command=AnetCommand.new
    @anet_device = AnetDevice.find(params[:id])
    @anet_command.arduino_id=@anet_device.id
    @anet_commands=AnetCommand.where(:arduino_id=>@anet_device.id)
    @anet_schedules=SysSchedule.where(:device_id=>@anet_device.id)
    render :layout => false
  end

  def anet_device_params
    params.require(:anet_device).permit(:token,:schedule,:command)
  end
  # POST /anet_devices/1/edit
  def manage_schedule
    result = Hash.new
    result["result"]="error"
    if params.has_key?("device_id")
      isValidDevice = AnetDevice.where(:id => params[:device_id], :id_owner => session[:activeUser].id ).last
      if isValidDevice != nil and params.has_key?("command") and params[:command] != "---"
        newSchedule = SysSchedule.new
        newSchedule.device_id = isValidDevice.id
        newSchedule.schedule = params[:schedule]
        newSchedule.command = params[:command]
        newSchedule.save
        # newSchedule.schedule = newSchedule.schedule + 5.hour
        newSchedule.save
      end
    elsif params.has_key?("token")
      isValidDevice = AnetDevice.where(:token => params[:token], :id_owner => session[:activeUser].id ).last
      if isValidDevice != nil and params.has_key?("command") and params[:command] != "---"
        newSchedule = SysSchedule.new
        newSchedule.device_id = isValidDevice.id
        newSchedule.schedule = params[:schedule]
        newSchedule.command = params[:command]
        newSchedule.save
        result["result"]="success"
      end
    end
    respond_to do |format|
      if result["result"] == "error"
        format.json { render json: result.to_json, status:401 }
      else
        format.json { render json: result.to_json, status:200  }
      end
    end
  end

  # POST /anet_devices/1/edit
  def manage_pubsub
    return receive_nest_payload_v2(params[:subscription],Base64.decode64(params[:message][:data]))
  end

  # POST /anet_devices
  # POST /anet_devices.json
  def create
    @anet_device = nil
    if session[:activeUser] != nil
      if params.has_key?("claim") and params[:claim] == "yes"
        @anet_device =AnetDevice.where(:token=>params[:anet_device]["token"], :secret_token=>params[:anet_device]["secret_token"],:id_owner => nil).last
      elsif params.has_key?("ssid") and params[:ssid] == "yes"
        @anet_device =AnetDevice.where(:ssid=> params[:ssid],:id_owner => nil,:mip => request.ip).last
      else
        @anet_device =AnetDevice.new(params[:anet_device])
        @anet_device.token=UUIDTools::UUID.random_create.to_s()
        @anet_device.token=@anet_device.token[0..7]
        @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
        @anet_device.secret_token=@anet_device.secret_token[0..7]
      end
      #creamos un token y un token secret
      if @anet_device != nil
        @anet_device.id_owner=session[:activeUser].id
        @anet_device.description = params[:anet_device]["description"]
        @anet_device.location = params[:anet_device]["location"]
        @anet_device.object = params[:anet_device]["object"]
        @anet_device.notify_telegram = params[:anet_device]["notify_telegram"]
        @anet_device.notify_email = params[:anet_device]["notify_email"]
        @anet_device.notify_twitter = params[:anet_device]["notify_twitter"]
        @anet_device.notify_fbinbox = params[:anet_device]["notify_fbinbox"]
      end
    end
    
    respond_to do |format|
      responseHash = Hash.new
      responseHash["result"] = "error"
      if @anet_device != nil and @anet_device.save
        # this part allows google home to sync devices on demand
        # uri = URI.parse("https://s.suitch.network/smarthome/create")
        # this is fastr if called directly on the server, 3001 represents google home
        uri = URI.parse("http://localhost:3001/smarthome/create")
        https = Net::HTTP.new(uri.host,uri.port)
        # https.use_ssl = true
        req = Net::HTTP::Post.new(uri)
        req['Content-Type'] = 'application/json'
        req.body = {userId: session[:activeUser].id.to_s}.to_json
        response = https.request(req)

        responseHash["result"] = "success"
        format.html { redirect_to "/personal_accounts", notice: 'A new device was successfully added to the arduino and raspberry network.' }
        if params.has_key?("source") && params[:source] == 'web'
          format.json { render json: responseHash.to_json }
        else
          format.json { redirect_to "/personal_accounts", notice: 'A new device was successfully added to the arduino and raspberry network.' }
        end
      else
        format.html { redirect_to "/personal_accounts", notice: "No devices added" }
        if (params.has_key?("source") && params[:source] == 'web') or session[:activeUser] == nil
          format.json { render json: responseHash.to_json, status: 401 }
        else
          format.json { redirect_to "/personal_accounts", notice: "No devices added" }
        end
      end
    end
  end

  def fbbot
    render :nothing => true, :status => 200

    # respond_to do |format|
    #   format.html { render json: "QqIbJAgMJjHWmH1n8BIG7eWNq".to_json }
    #   format.json { render json: "QqIbJAgMJjHWmH1n8BIG7eWNq".to_json }
    # end
  end

  # PUT /anet_devices/1
  # PUT /anet_devices/1.json
  
  def update
    responseHash = Hash.new
    responseHash["result"] = "error"
    # verify if this is a version 1 request or v2 request by identifying the type of id
    # also we need to verify ownership
    if params[:id].is_number? == true
      @anet_device = AnetDevice.where(:id=>params[:id], :id_owner=>session[:activeUser].id).last
    else
      @anet_device = AnetDevice.where(:token=>params[:id],:id_owner=>session[:activeUser].id).last
    end
    if @anet_device != nil
      if @anet_device.location!=nil
          @anet_device.location=@anet_device.location.downcase
      end
      if @anet_device.object!=nil
          @anet_device.object=@anet_device.object.downcase
      end
      token_tmp = @anet_device.token
      token_secret_tmp = @anet_device.secret_token
      @anet_device.update_attributes(params[:anet_device])
      @anet_device.token = token_tmp
      @anet_device.secret_token = token_secret_tmp
      responseHash["result"] = "success"
      # finally notify google home about the change
      uri = URI.parse("http://localhost:3001/smarthome/create")
      https = Net::HTTP.new(uri.host,uri.port)
      # https.use_ssl = true
      req = Net::HTTP::Post.new(uri)
      req['Content-Type'] = 'application/json'
      req.body = {userId: session[:activeUser].id.to_s}.to_json
      response = https.request(req)
    end
    respond_to do |format|
      if responseHash["result"] == "success"
        format.json { render json: responseHash, status: 200 }
      else
        format.json { render json: responseHash, status: 401 }
      end
    end
  end

  def sendToDevicev2
    # debug by  tail -f /var/log/nginx/*.log
    if params.has_key?(:source) and params[:source] == "alexa"
      new_activity = SystemActivity.new
      new_activity.device_token = params[:device]
      new_activity.virtual_assistant = 3
      new_activity.command = "sendto"
      new_activity.value=params[:content]
      new_activity.raw_content=params[:content]
      # due to alexa doesnt know the secret token, we will extract it
      currentDevice = AnetDevice.where(:token=>params[:device]).last
      if currentDevice != nil
        new_activity.anet_device_id = currentDevice.id
      end
      new_activity.save
    end
    puts "TRYING TO SEND TEST TO DEVICE"
    # DeviceCommunicationJob.perform_async(params)
    # s = TCPSocket.new 'localhost', 3000
    # while line = s.gets # Read lines from socket
    #     puts line.inspect
    #     if line.index("TOKEN?")!=nil
    #         puts "tokenizing sender... "
    #         # get a global account to send messages
    #         global_clients = AnetDevice.where(:location=>"server", :object=>"global")
    #         selected_global_client = global_clients[rand(global_clients.length)]
    #         puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
    #         s.puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
    #     elsif line.index("WLCME")!=nil
    #         puts "WLCME"
    #         puts params[:device]
    #         # lets search if the device has the command only if it comes from
    #         # ALEXA
    #         if params.has_key?(:source) and params[:source] == "alexa"
    #           # search the command
    #           @anet_device = AnetDevice.where(:token=>params[:device]).last 
    #           @anet_commands=AnetCommand.where(:arduino_id=>@anet_device.id, :command_data=>params[:content])
    #           @anet_commands_toggle=AnetCommand.where(:arduino_id=>@anet_device.id)
    #           if @anet_commands.length > 0 and @anet_device.version != "sfir"
    #             s.puts "sendto "+params[:device]+" "+params[:content]+"\n"
    #           else
    #             if @anet_device.version == "sfir.alpha0"
    #               # toggle all commands
    #               @anet_commands_toggle.each do |singleCommand|
    #                 logger.warn("sendto "+params[:device]+" "+singleCommand.command_data)
    #                 s.puts "sendto "+params[:device]+" "+singleCommand.command_data+"\n"
    #                 sleep 1.5
    #               end
    #               # s.puts "sendto "+params[:device]+" allx\n"
    #             else
    #               s.puts "sendto "+params[:device]+" "+params[:content]+"\n"
    #             end
    #           end
    #         else
    #           puts "from Google Home"
    #           @anet_device = AnetDevice.where(:token=>params[:token]).last 
    #           puts @anet_device.inspect
    #           @anet_commands=AnetCommand.where(:arduino_id=>@anet_device.id, :command_data=>params[:content])
    #           @anet_commands_toggle=AnetCommand.where(:arduino_id=>@anet_device.id)
    #           puts "sendto "+params[:token]+" "+params[:content]
    #           if @anet_device.version != "sfir"
    #             puts "sendto "+params[:token]+" "+params[:content]
    #             s.puts "sendto "+params[:token]+" "+params[:content]+"\n"
    #             # sleep 1
    #           else
    #             if @anet_device.version == "sfir.alpha0"
    #               @anet_commands_toggle.each do |singleCommand|
    #                 logger.warn("sendto "+params[:device]+" "+singleCommand.command_data)
    #                 s.puts "sendto "+params[:device]+" "+singleCommand.command_data+"\n"
    #                 sleep 1.5
    #               end
    #             else
    #               puts "sendto "+params[:token]+" allx"
    #               # s.puts "sendto "+params[:token]+" allx\n"
    #               s.puts "sendto "+params[:token]+" "+params[:content]+"\n"
    #               # sleep 1
    #             end
    #           end
    #         end
    #         break
    #     end
    # end
    # s.close

    respond_to do |format|
      format.html { render json: "Ok".to_json }
      format.json { render json: "Ok".to_json }
    end
  end
  
  def sendToDevice
    # debug by  tail -f /var/log/nginx/*.log
    if params.has_key?(:source) and params[:source] == "alexa"
      new_activity = SystemActivity.new
      new_activity.device_token = params[:device]
      new_activity.virtual_assistant = 3
      new_activity.command = "sendto"
      new_activity.value=params[:content]
      new_activity.raw_content=params[:content]
      # due to alexa doesnt know the secret token, we will extract it
      currentDevice = AnetDevice.where(:token=>params[:device]).last
      if currentDevice != nil
        new_activity.anet_device_id = currentDevice.id
      end
      new_activity.save
    end
    puts "TRYING TO SEND TEST TO DEVICE"
    s = TCPSocket.new 'localhost', 3000
    while line = s.gets # Read lines from socket
        puts line.inspect
        if line.index("TOKEN?")!=nil
            puts "tokenizing sender... "
            # get a global account to send messages
            global_clients = AnetDevice.where(:location=>"server", :object=>"global")
            selected_global_client = global_clients[rand(global_clients.length)]
            puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
            s.puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
        elsif line.index("WLCME")!=nil
            puts "WLCME"
            puts params[:device]
            # lets search if the device has the command only if it comes from
            # ALEXA
            if params.has_key?(:source) and params[:source] == "alexa"
              # search the command
              @anet_device = AnetDevice.where(:token=>params[:device]).last 
              @anet_commands=AnetCommand.where(:arduino_id=>@anet_device.id, :command_data=>params[:content])
              @anet_commands_toggle=AnetCommand.where(:arduino_id=>@anet_device.id)
              if @anet_commands.length > 0 and @anet_device.version != "sfir"
                s.puts "sendto "+params[:device]+" "+params[:content]+"\n"
              else
                if @anet_device.version == "sfir.alpha0"
                  # toggle all commands
                  @anet_commands_toggle.each do |singleCommand|
                    logger.warn("sendto "+params[:device]+" "+singleCommand.command_data)
                    s.puts "sendto "+params[:device]+" "+singleCommand.command_data+"\n"
                    sleep 1.5
                  end
                  # s.puts "sendto "+params[:device]+" allx\n"
                else
                  s.puts "sendto "+params[:device]+" "+params[:content]+"\n"
                end
              end
            else
              puts "from Google Home"
              @anet_device = AnetDevice.where(:token=>params[:token]).last 
              puts @anet_device.inspect
              @anet_commands=AnetCommand.where(:arduino_id=>@anet_device.id, :command_data=>params[:content])
              @anet_commands_toggle=AnetCommand.where(:arduino_id=>@anet_device.id)
              puts "sendto "+params[:token]+" "+params[:content]
              if @anet_device.version != "sfir"
                puts "sendto "+params[:token]+" "+params[:content]
                s.puts "sendto "+params[:token]+" "+params[:content]+"\n"
                # sleep 1
              else
                if @anet_device.version == "sfir.alpha0"
                  @anet_commands_toggle.each do |singleCommand|
                    logger.warn("sendto "+params[:device]+" "+singleCommand.command_data)
                    s.puts "sendto "+params[:device]+" "+singleCommand.command_data+"\n"
                    sleep 1.5
                  end
                else
                  puts "sendto "+params[:token]+" allx"
                  # s.puts "sendto "+params[:token]+" allx\n"
                  s.puts "sendto "+params[:token]+" "+params[:content]+"\n"
                  # sleep 1
                end
              end
            end
            break
        end
    end
    s.close

    respond_to do |format|
      format.html { render json: "Ok".to_json }
      format.json { render json: "Ok".to_json }
    end
  end

  def showClaim
    @anet_device = AnetDevice.new
    respond_to do |format|
      format.html { render "claim", :layout => false }
      format.json { render json: "NOT_FOUND".to_json }
    end
  end

  def showAdd
    @anet_device = AnetDevice.new
    respond_to do |format|
      format.html { render "new_device", :layout => false }
      format.json { render json: "NOT_FOUND".to_json }
    end
  end

  def showPhone
    @sys_user_phone = SysUserPhone.new
    respond_to do |format|
      format.html { render "new_phone", :layout => false }
      format.json { render json: "NOT_FOUND".to_json }
    end
  end

  def showSWatch
    @smartwatch = Smartwatch.new
    respond_to do |format|
      format.html { render "new_swatch", :layout => false }
      format.json { render json: "NOT_FOUND".to_json }
    end
  end

  def showOpenFile
    s = TCPSocket.new 'localhost', 3000
    while line = s.gets # Read lines from socket
        puts line.inspect
        if line.index("TOKEN?")!=nil
            puts "enviando"
           s.puts "025621de/d822339f\n" 
        elsif line.index("WLCME")!=nil
            puts "WLCME"
            puts params[:device]
            s.puts "sendto "+params[:device]+" openFile "+params[:file]+"\n"
            break
        end
    end
    s.close
   
    respond_to do |format|
      format.json { render json: "OK".to_json }
    end
  end

  def showSaveFile
    s = TCPSocket.new 'localhost', 3000
    currentFile = Base64.decode64(params[:contents]).split("\n")
    currentCounter = 0
    while line = s.gets # Read lines from socket
        puts line.inspect
        if line.index("TOKEN?")!=nil
          puts "tokenizing sender... "
          # get a global account to send messages
          global_clients = AnetDevice.where(:location=>"server", :object=>"global")
          selected_global_client = global_clients[rand(global_clients.length)]
          puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
          s.puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
        elsif line.index("WLCME")!=nil
            puts "WLCME"
            puts params[:device]
            s.puts "sendto "+params[:device]+" saveFile "+params[:file]+"\n"
        elsif currentCounter < currentFile.length
          puts currentFile[currentCounter]
          s.puts "sendto "+params[:device]+" "+currentFile[currentCounter]+"\n"
          currentCounter = currentCounter + 1
        elsif currentCounter >= currentFile.length
            puts "<<<END\n"
            s.puts "sendto "+params[:device]+" <<<END\n"
            # s.puts "sendto "+params[:device]+" saveFile "+params[:file]+"\n"
            break
        end
        sleep 0.02
    end
    s.close
    
    respond_to do |format|
      format.json { render json: "OK".to_json }
    end
  end

  def showBrowser
    s = TCPSocket.new 'localhost', 3000
    while line = s.gets # Read lines from socket
        puts line.inspect
        if line.index("TOKEN?")!=nil
          puts "tokenizing sender... "
          # get a global account to send messages
          global_clients = AnetDevice.where(:location=>"server", :object=>"global")
          selected_global_client = global_clients[rand(global_clients.length)]
          puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
          s.puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
        elsif line.index("WLCME")!=nil
            puts "WLCME"
            puts params[:device]
            s.puts "sendto "+params[:device]+" fileBrowser\n"
            break
        end
    end
    s.close
    @device = params[:device]
    last_files = Sys_Phrase.where(:phone => params[:device],:phrase => 'fileBrowser').last
    @files = []
    if last_files != nil
      @files = JSON.parse(last_files.translate)
    end
    respond_to do |format|
      format.html { render "browseFiles", :layout => false }
      format.json { render json: "NOT_FOUND".to_json }
    end
  end

  def communication
    # this method sends information directly to the device
    # if no valid TOKEN and SECRET TOKEN are sent
    # then it will not replicate
    currentDevice = AnetDevice.where(:token=>params[:token],:secret_token=>params[:secret_token]).last
    if currentDevice == nil
      respond_to do |format|
        format.html { render json: "NOT_FOUND".to_json }
        format.json { render json: "NOT_FOUND".to_json }
      end
      return
    end
    puts "TRYING TO SEND COMMAND TO DEVICE"
    s = TCPSocket.new 'localhost', 3000
      while line = s.gets # Read lines from socket
          puts line.inspect
          if line.index("TOKEN?")!=nil
            puts "tokenizing sender... "
            # get a global account to send messages
            global_clients = AnetDevice.where(:location=>"server", :object=>"global")
            selected_global_client = global_clients[rand(global_clients.length)]
            puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
            s.puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
          elsif line.index("WLCME")!=nil
              puts "from Google Home"
              @anet_device = AnetDevice.where(:token=>params[:token]).last 
              puts @anet_device.inspect
              @anet_commands=AnetCommand.where(:arduino_id=>@anet_device.id, :command_data=>params[:content])
              @anet_commands_toggle=AnetCommand.where(:arduino_id=>@anet_device.id)
              if @anet_device.version != "sfir"
                puts "sendto "+params[:token]+" "+params[:content]
                s.puts "sendto "+params[:token]+" "+params[:content]+"\n"
                # sleep 1
              else
                puts "sendto "+params[:token]+" allx"
                # s.puts "sendto "+params[:token]+" allx\n"
                s.puts "sendto "+params[:token]+" "+params[:content]+"\n"
                # sleep 1
              end
              break
          end
      end
    s.close
    respond_to do |format|
      format.html { render json: "Ok".to_json }
      format.json { render json: "Ok".to_json }
    end
end

def dhcp_process
  # first search if the token exists
  if params.has_key?(:token) and params[:token] != nil
    exists = AnetDevice.where(:token => params[:token])
    if exists == nil
      # return an error
      return
    end
  end
  # this method tries to assign an IP address to the MQTT VPN by guessing the 
  # 3rd and 4th sections of the IP, if by the 4th try the IP is not guessed,
  # then it will random the 3rd member of the IP.
  fourthMember = rand(2...99)
  thirdMember = 1 #rand(1...4)
  finalIP = ""
  # search if the device had one previously but disconnected
  wasConnected = Sys_Dhcp.where("created_at >= ?", DateTime.now - 5.minute).where(:token => params[:token]).last
  # search a previous one when clockwork fails
  wasConnectedAndFail = Sys_Dhcp.where("created_at < ?", DateTime.now - 5.minute)
  wasConnectedAndFail.each do |failsingle|
    failsingle.destroy
  end
  if wasConnected == nil
    for x in [1, 2, 3, 4] do

      testIP = "10.0."+thirdMember.to_s+"."+fourthMember.to_s
      attachedIP = Sys_Dhcp.where(:ip => testIP)
      if attachedIP.length == 0
        finalIP = testIP
        # create ip assignment
        new_ip = Sys_Dhcp.new
        new_ip.ip = testIP
        new_ip.token = params[:token]
        new_ip.created_at = DateTime.now
        new_ip.save
        break
      else 
        fourthMember = rand(2...254)
      end
    end
  else
    finalIP = wasConnected.ip
    wasConnected.blocked = 0
    wasConnected.blocked_time = nil
    wasConnected.save
  end
  respond_to do |format|
    format.json { render json: finalIP.to_json }
  end
end

def alexa_sync
  respond_to do |format|
    format.json { render json: "Ok".to_json }
  end
end

def camera_download
  start_date = Time.at(params[:startdate].to_i).to_datetime
  end_date = Time.at(params[:enddate].to_i).to_datetime
  all_pictures = SysVideo.where(:token => params[:token]).where("created_at >= ?", start_date).where("created_at <= ?", end_date)
  # create file in the pictures directory
  receiving_img = Time.now.to_i.to_s
  open(receiving_img+'.txt', 'wb') do |file|
    all_pictures.each do |single_picture|
      file << "file '/home/suitch/public/recordings/"+params[:token]+"/"+single_picture.filename+"'\n"
    end
  end
  system("ffmpeg -y -r 1/2 -f concat -safe 0 -i '/home/suitch/#{receiving_img}.txt' -c:v libx264 -vf 'fps=25,format=yuv420p' 'public/#{receiving_img}.mp4'")
  respond_to do |format|
    format.json { render json: receiving_img.to_json }
    # send_file(
    #   "#{Rails.root}/public/#{receiving_img}.mp4",
    #   filename: "video.mp4",
    #   type: "video/mp4"
    # )
  end
end

def suitch_state_report_v2
  # this was meant as a todo task
  # result = Hash.new
  # result["result"] = "error"
  # if session[:activeUser] != nil
  #   @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
  #   if @anet_device != nil
  #     # sql_single_prop="select ac.command,ac.id,ac.command_data,ac.unit,av.value FROM anet_commands ac INNER JOIN (select * from anet_values where `id` in (select max(`id`) from anet_values group by `command_id`)) av ON av.command_id = ac.id where av.arduino_id="+@anet_device.id.to_s+" and ac.id="+params[:id].to_s
  #     # @device_status= ActiveRecord::Base.connection.exec_query(sql_single_prop).to_a
  #     # due to a performance issue with the previous one, we will extract now the prop by hand, which might be faster
  #     device_command = AnetCommand.where(:id=>params[:id]).last
  #     device_value = AnetValue.where(:arduino_id=>@anet_device.id,:command_id=>device_command.id).last
  #     @device_status=Hash.new
  #     @device_status["command"]=device_command.command
  #     @device_status["command_data"]=device_command.command_data
  #     @device_status["unit"]=device_command.unit
  #     @device_status["value"]=device_value.value
  #     @device_status["created_at"]=device_value.created_at
  #     result["result"] = "success"
  #   end
  # end
  # respond_to do |format|
  #   if result["result"] == "success"
  #     format.json { render json: @device_status.to_json, status:200}
  #   else
  #     format.json { render json: result.to_json, status:401 }
  #   end
  # end
end

def alexa_state_report_v2
#   {
#   directive: {
#     header: {
#       namespace: 'Alexa',
#       name: 'ReportState',
#       payloadVersion: '3',
#       messageId: '3f2b7aee-ed21-4cd2-8c2a-1128de33b026',
#       correlationToken: 'AAAAAAAAAAAaXNysWjcaOKUINH1tp0cA/AEAAAAAAADyergHqpvGL7n1g6duzz3iqaokk2wvrsXYB2zLj34AgNWtFO3DK6XUWK/0tlwRU3mnzz9LO7iuMxeOXFAubUTV3luIU9daRpFEI/nw3MnhVTycZkPHh3ypypLzXbuer1t8uaOOCdplpc+N35LN+x2Ue27c6dIHEpohLbB74k/DccKrKJsNlCo3kOPANIOfTng2+JZG8n23ty6vyVYW+pPcHaWJ4mcCVFEeHfRVGtkPy3JQdtFDMOsKV/qJElQBRwZA5LfW3AgUvz+9ta7MjONBpI0QikS1lBDtlEQNRnuC/V6vW/EJhMOMxuMPrgwWoqdRR3l3steH7H2nJ5ZTOimXPGPoiUlzMoY5eWhilyVPLRE3eRwexkK3L1B3jqks1qOGJ9/7IKo1TJisE1RAsbPDPluCG0Np8PLpajb5rUYg2OTXv33ufGlKKpMKYRAHDA4VjtYmdGe2ecXRw0OQS7ldKMTjMb8wFsxrdR23xPulXNo7y7X+F/0MSU14qnNKo5LQWSw5lYlU7GMDa5Fes5+jLTwR8viSV6G+LmpS9mIwl6cHiwQDTOCk+nn+8Tze2aau8qBE9crliXE/2TNgrYLCS6Ou9yiNi7u1/VQsIhiv+wvw7AioNAcHq9kj0rm6IgLWiFNPDW9VbK371zktNAdJEhLiN0A1dFbUU+Xb'
#     },
#     endpoint: { scope: [Object], endpointId: 'efdb0822', cookie: [Object] },
#     payload: {}
#   }
# }
# 
# search for the device first
current_device = AnetDevice.where(:token=>params[:raw]["directive"]["endpoint"]["endpointId"]).last

# activity = SystemActivity.where(:device_token=>params[:raw]["directive"]["endpoint"]["endpointId"]).last
state_result = {
    "event": {
        "header": {
            "namespace": "Alexa",
            "name": "StateReport",
            "payloadVersion": "3",
            "messageId": params[:raw]["directive"]["header"]["messageId"]+"-R",
            "correlationToken": params[:raw]["directive"]["header"]["correlationToken"]
        },
        "endpoint": {
          "scope":{
            "type": "BearerToken",
            "token":Sys_User.where(:id=>current_device.id_owner).last.user_id
          },
            "endpointId": params[:raw]["directive"]["endpoint"]["endpointId"]
        },
        "payload": {}
    }
}
if current_device!= nil && (current_device.version==nil || current_device.version=="suitchv3" || current_device.version=="bulbv1")
  # get last value
  activity_sql = "select * from system_activities where (device_token = '"+params[:raw]["directive"]["endpoint"]["endpointId"]+"' and (value='on' or value='off')) order by id desc limit 1"
  temp_res= ActiveRecord::Base.connection.exec_query(activity_sql).to_a
  if temp_res.length==0
    power_result=Hash.new
    power_result["value"]="off"
    temp_res.push(power_result)
  end

  state_result["context"]= {
            "properties": [{
                "namespace": params[:raw]["directive"]["endpoint"]["cookie"]["interface"],
                "name": "powerState",
                "value": temp_res[0]["value"].upcase,
                "timeOfSample": Time.now.utc.iso8601,
                "uncertaintyInMilliseconds": 1400
            }]
        }
elsif current_device!= nil && current_device.version=="doorv1"
  # select * from system_activities where (device_token = '739b1bb3' and (value='lock' or value='unlock')) order by id desc limit 1;
  door_sql = "select * from system_activities where (device_token = '#{current_device.token}' and (value='lock' or value='unlock')) order by id desc limit 1;"
  temp_res_door= ActiveRecord::Base.connection.exec_query(door_sql).to_a
  state_result["context"]= {
    "properties": [
      {
        "namespace": "Alexa.LockController",
        "name": "lockState",
        "value": temp_res_door[0]["value"]=="lock" ? "LOCKED" : "UNLOCKED",
        "timeOfSample": Time.now.utc.iso8601,
        "uncertaintyInMilliseconds": 1000
      }
    ]
  }
elsif current_device!= nil && (current_device.version=="sfir" || current_device.version=="sfir_alpha0")
  # get last value
  activity_sql = "select * from system_activities where (device_token = '"+params[:raw]["directive"]["endpoint"]["endpointId"]+"' and (value='on' or value='off')) order by id desc limit 1"
  temp_res= ActiveRecord::Base.connection.exec_query(activity_sql).to_a
  if temp_res.length==0
    power_result=Hash.new
    power_result["value"]="off"
    temp_res.push(power_result)
  end
  state_result["context"]= {
            "properties": [{
                "namespace": params[:raw]["directive"]["endpoint"]["cookie"]["interface"],
                "name": "powerState",
                "value": temp_res[0]["value"].upcase,
                "timeOfSample": Time.now.utc.iso8601,
                "uncertaintyInMilliseconds": 1400
            }]
        }

elsif current_device!= nil && current_device.version=="thermostatv1"
  thermo_sql = "select ac.unit,ac.command,ac.id,ac.command_data,av.value FROM anet_commands ac left JOIN (select * from anet_values where id in (select max(id) from anet_values group by command_id)) av ON av.command_id = ac.id where ac.arduino_id=#{current_device.id} and ac.is_only_question=0 order by av.id desc"
  temp_res= ActiveRecord::Base.connection.exec_query(thermo_sql).to_a
  thermo_props=[]
  temp_res.each do |single_prop|
    if single_prop["command_data"]=="mode"
      if single_prop["value"] == nil
        single_prop["value"] = "off"
      end
      thermo_props.push({
        "namespace": "Alexa.ThermostatController",
        "name": "thermostatMode",
        "value": single_prop["value"].upcase,
        "timeOfSample": Time.now.utc.iso8601,
        "uncertaintyInMilliseconds": 500
    })
    elsif single_prop["command_data"]=="set_temp"
      thermo_props.push({
        "namespace": "Alexa.ThermostatController",
        "name": "targetSetpoint",
        "value": {
            "value": single_prop["value"].to_f,
            "scale": "CELSIUS"
        },
        "timeOfSample": Time.now.utc.iso8601,
        "uncertaintyInMilliseconds": 500
    })
    elsif single_prop["command_data"]=="temp"
      thermo_props.push({
        "namespace": "Alexa.TemperatureSensor",
        "name": "temperature",
        "value": {
            "value": single_prop["value"].to_f,
            "scale": "CELSIUS"
        },
        "timeOfSample": Time.now.utc.iso8601,
        "uncertaintyInMilliseconds": 1000
    })
    end
  end
  state_result["context"]={
        "properties": thermo_props
    }
end
logger.info "alexa_state >>>>>"
logger.info state_result.inspect
logger.info "alexa_state <<<<<"
  respond_to do |format|
    format.json { render json: state_result.to_json }
  end
end

def alexa_scanning
  usuario = Sys_User.where(:alexa_oauth=>params[:token]).last
  lista = []
  if usuario != nil
    devices = AnetDevice.where(:id_owner=>usuario.id)
    devices.each do |singleDevice|
      
        device_descriptor = {
        "endpointId":singleDevice.token,
        "manufacturerName":"Consultware",
        "modelName":"Suitch",
        "version":"V1",
        "friendlyName":singleDevice.object,
        "description":singleDevice.description,
        "displayCategories":[
          "LIGHT","SWITCH","CHRISTMAS_TREE","SMARTPLUG"
        ],
        "capabilities":[
            {
              "type":"AlexaInterface",
              "interface":"Alexa.PowerController",
              "version":"3",
              "properties":{
                  "supported":[
                    {
                        "name":"powerState"
                    }
                  ],
                  "proactivelyReported":true,
                  "retrievable":true
              }
            }
        ],
        "cookie":{
            "deviceId":singleDevice.token,
            "interface":"Alexa.PowerController"
          }
        }
      if singleDevice.version == "thermostatv1"
        device_descriptor["displayCategories"]=["THERMOSTAT","TEMPERATURE_SENSOR"]
        device_descriptor["capabilities"]= [
          {
            "type": "AlexaInterface",
            "interface": "Alexa.ThermostatController",
            "version": "3",
            "properties": {
              "supported": [
                {
                  "name": "targetSetpoint"
                },
                {
                  "name": "thermostatMode"
                }
              ],
              "proactivelyReported": true,
              "retrievable": true
            },
            "configuration": {
              "supportedModes": [ "HEAT", "COOL","OFF" ],
              "supportsScheduling": false
            }
          },
          {
            "type": "AlexaInterface",
            "interface": "Alexa.TemperatureSensor",
            "version": "3",
            "properties": {
              "supported": [
                {
                  "name": "temperature"
                }
              ],
              "proactivelyReported": true,
              "retrievable": true
            }
          },
          {
            "type": "AlexaInterface",
            "interface": "Alexa",
            "version": "3"
          }
        ]
      elsif singleDevice.version == "doorv1"
        device_descriptor["displayCategories"]=["SMARTLOCK","DOOR"]
        device_descriptor["capabilities"] = [
          {
            "type": "AlexaInterface",
            "interface": "Alexa.LockController",
            "version": "3",
            "properties": {
              "supported": [
                {
                  "name": "lockState"
                }
              ],
              "proactivelyReported": true,
              "retrievable": true
            }
          },
          {
            "type": "AlexaInterface",
            "interface": "Alexa",
            "version": "3"
          }
        ]
      elsif singleDevice.version == "sfir"
        device_descriptor["displayCategories"]=["SWITCH","LIGHT"]
      end
      lista.push(device_descriptor)
    end
  end
  logger.info "alexa_scan >>>>"
  logger.info lista.inspect
  logger.info "alexa_scan <<<<<"
  respond_to do |format|
    format.json { render json: lista.to_json }
  end
end

def get_public_devices_v2
  devices = nil
  if session[:activeUser] != nil
    devices = AnetDevice.select("description,location,object,version").where(:is_public=>1)
    devices.each do |singleDevice|

    end
  end
  respond_to do |format|
    format.html { render json: devices.to_json }
    format.json { render json: devices.to_json }
  end
end

def get_devices_v2_bak
  devices = nil
  if session[:activeUser] != nil
    sql_devices = "select ad.*, al.connection_type from anet_devices ad LEFT join (select * from anet_logs where id in (select max(id) from anet_logs group by arduino_id)) al on ad.id=al.arduino_id where ad.id_owner="+session[:activeUser].id.to_s
    @temp_res= ActiveRecord::Base.connection.exec_query(sql_devices).to_a

  end
  respond_to do |format|
    format.html { render json: @temp_res.to_json }
    format.json { render json: @temp_res.to_json }
  end
end

def get_devices_v2
  devices = nil

  if session[:activeUser] != nil
    sql_devices = "SELECT ad.*, al.connection_type FROM anet_devices ad LEFT JOIN (SELECT * FROM anet_logs WHERE id IN (SELECT MAX(id) FROM anet_logs GROUP BY arduino_id)) al ON ad.id = al.arduino_id WHERE ad.id_owner = " + session[:activeUser].id.to_s
    @temp_res = ActiveRecord::Base.connection.exec_query(sql_devices).to_a

    # Filter records where connection_type is NULL
    filtered_results = @temp_res.select { |record| record["connection_type"].nil? }

    # Check for entries in anet_values from the last 3 hours and update the original results
    filtered_results.each do |filtered_record|
      arduino_id = filtered_record['id']
      recent_values_query = "SELECT COUNT(*) FROM anet_values WHERE arduino_id = #{arduino_id} AND created_at >= '#{3.hours.ago.to_s(:db)}'"
      recent_values_count = ActiveRecord::Base.connection.exec_query(recent_values_query).rows.flatten.first

      # Update connection_type in the original results if there are recent entries
      if recent_values_count.to_i > 0
        result_record = @temp_res.find { |r| r['id'] == arduino_id }
        result_record["connection_type"] = 1 if result_record
      end
    end
  end
  respond_to do |format|
    format.html { render json: @temp_res.to_json }
    format.json { render json: @temp_res.to_json }
  end
end


def receive_email_payload_v2
  # virtual assistant constants
  # suitch = 0
  # siri = 1
  # google = 2
  # alexa = 3
  # nest = 4
  # acurite = 5
  # identify the sender
  # {"headers":{"date":"Sun, 05 Dec 2021 01:38:47 +0000", "from":"myAcuRite Alerts <alerts@myacurite.com>", "to":"72bd39ca08ab9a757f79+1b3b9629-7e4a-413a-b5b7-c6e8e07addb7-acurite@cloudmailin.net", "message_id":"<0100017d883e589b-ecfb0b4b-3e80-466b-acb3-741515ee7c08-000000@email.amazonses.com>", "subject":"Low Feels Like Alert", "mime_version":"1.0", "content_type":"text/html; charset=UTF-8", "content_transfer_encoding":"quoted-printable", "dkim_signature":["v=1; a=rsa-sha256; q=dns/txt; c=relaxed/simple;\ts=dd4twgg5poswzzs3pqs6celkynmrkx73; d=myacurite.com; t=1638668327;\th=Date:From:To:Message-ID:Subject:Mime-Version:Content-Type:Content-Transfer-Encoding;\tbh=7v4eXNmdRhLCaCFZnDixOb5fMJWu6IvSaex7Doc8rfI=;\tb=Mu1Hft6IE+sPANLENBFJwOHGyTrK/0D/oy9+9JARtUEMKcogA2UUfPJLpqgCYWMS\tuz9oazyzMdUCI0ChITC04u+DEWb5DhyjWvJgfGp0bBuUIhBqbkd5ekuUpQtsEtm7DvU\tnLl5LxWQfTvdB89hBXqoXAmT/chMWi0ZavB7mLjs=", "v=1; a=rsa-sha256; q=dns/txt; c=relaxed/simple;\ts=ug7nbtf4gccmlpwj322ax3p6ow6yfsug; d=amazonses.com; t=1638668327;\th=Date:From:To:Message-ID:Subject:Mime-Version:Content-Type:Content-Transfer-Encoding:Feedback-ID;\tbh=7v4eXNmdRhLCaCFZnDixOb5fMJWu6IvSaex7Doc8rfI=;\tb=RvKVyt77/RCgJBmNSKxgeaL+a5vcy8ruDhjotWKBLcH9IGyQfgKq7NU10SVrIGBq\tRWFpMdvgPpYiXZP5aJ8Ti+r1z+mq4lCOUWWFF9xyxYN+3aAY93XXsQ41+9rmR3jKCdE\tzL39r9eNPw2K0wCTGSwvLQtyhcmPMI+ynrnvYqt8="], "feedback_id":"1.us-east-1.hd+11Ljec5lhHJFhBX7WjLWUDl7Ry54zL4Bms2duNws=:AmazonSES", "x_ses_outgoing":"2021.12.05-54.240.9.32"}, "envelope":{"to":"72bd39ca08ab9a757f79+1b3b9629-7e4a-413a-b5b7-c6e8e07addb7-acurite@cloudmailin.net", "recipients":["72bd39ca08ab9a757f79+1b3b9629-7e4a-413a-b5b7-c6e8e07addb7-acurite@cloudmailin.net"], "from":"0100017d883e589b-ecfb0b4b-3e80-466b-acb3-741515ee7c08-000000@amazonses.com", "helo_domain":"a9-32.smtp-out.amazonses.com", "remote_ip":"54.240.9.32", "tls":true, "tls_cipher":"TLSv1.2", "md5":"667cc1c371907cd25e5c4efacb08f8e9", "spf":{"result":"pass", "domain":"amazonses.com"}}, "plain":"", "html":"It Feels Like less than 26.0°C at AcuRite Atlas New, feeling like 25.8°C at 7:37 PM.", "reply_plain":"", "attachments":"", "anet_device":{}}
  logger.info params["headers"]["from"] 
  if params["headers"]["from"].index("alerts@myacurite.com")!=nil
    # then, this is somethinf from acurite
    # and we need now to extract the user
    potential_user_with_service = params["headers"]["to"].split("+")[1].split("@")[0]
    potential_user = potential_user_with_service.split("-acurite")
    # lasttly log the message into system activities
    logger.info potential_user_with_service  
    logger.info potential_user 
    new_activity = SystemActivity.new
    # in this case the device token represents the user account from acurite as a whole,
    # we are treating acurite as if it were a single big device
    new_activity.device_token = potential_user_with_service
    new_activity.virtual_assistant = 5
    new_activity.command = "activity"
    # for the sake of minimizingtheamout of characterscoiming from
    new_activity.value=params["html"][0..250]
    new_activity.raw_content=params.to_s
    new_activity.save

  end

  render :nothing => true, :status => 200
end

def get_recent_activity_v2
  devices = nil
  if session[:activeUser] != nil
    # devices = AnetDevice.where(:id_owner=>session[:activeUser].id).pluck(:token)
    # sql = "select anet_devices.token, anet_devices.location, anet_devices.object, anet_devices.version, anet_devices.description, system_activities.virtual_assistant, system_activities.created_at FROM `anet_devices` INNER JOIN `system_activities` ON `system_activities`.`anet_device_id` = `anet_devices`.`id`  LIMIT 30"
    # sql = "SELECT anet_devices.token, anet_devices.location, anet_devices.object, anet_devices.version, anet_devices.description, system_activities.virtual_assistant, system_activities.created_at FROM system_activities INNER JOIN anet_devices ON anet_devices.id = system_activities.anet_device_id WHERE anet_devices.id_owner = "+session[:activeUser].id.to_s+" ORDER BY system_activities.created_at DESC LIMIT 30;"
    # # recent_activities = SystemActivity.joins(:anet_device).select("anet_devices.token, anet_devices.location, anet_devices.object, anet_devices.version, anet_devices.description, system_activities.virtual_assistant, system_activities.created_at").limit(30).reverse!

    # recent_activities = ActiveRecord::Base.connection.execute(sql)
    # devices.each do |singleDevice|
    # end
    sql = "SELECT anet_devices.token, anet_devices.location, anet_devices.object, anet_devices.version, anet_devices.description, system_activities.virtual_assistant, system_activities.created_at FROM system_activities INNER JOIN anet_devices ON anet_devices.id = system_activities.anet_device_id WHERE anet_devices.id_owner = #{session[:activeUser].id} ORDER BY system_activities.created_at DESC LIMIT 30;"
    recent_activities = SystemActivity.find_by_sql(sql)
  end
  respond_to do |format|
    format.html { render json: recent_activities.to_json }
    format.json { render json: recent_activities.to_json }
  end
end
def receive_alexa_payload_v2
  # before getting into the payload analysis
  # sometimes alexa asks about the state of a specific device with this payload
  # {"raw":{"directive":{"header":{"namespace":"Alexa", "name":"ReportState", "payloadVersion":"3", "messageId":"2d99ea8d-ad66-46e8-a305-24b472f1a16b", "correlationToken":""}, "endpoint":{"scope":{"type":"BearerToken", "token":"0bdbc939-a27c-404b-bcc1-81810b2a9973"}, "endpointId":"07587974", "cookie":{"interface":"Alexa.PowerController", "deviceId":"07587974"}}, "payload":{}}}, "anet_device":{}}

  if params[:raw]["directive"]["header"]["name"] == "ReportState"
    return alexa_state_report_v2()
  end
  # virtual assistant constants
  # suitch = 0
  # siri = 1
  # google = 2
  # alexa = 3
  # nest = 4
  # in this part we should store the activity
  new_activity = SystemActivity.new
  new_activity.device_token = params[:device]
  new_activity.virtual_assistant = 3
  new_activity.command = "sendto"
  new_activity.value=params[:content]
  new_activity.raw_content=params[:raw].to_s
  # due to alexa doesnt know the secret token, we will extract it
  currentDevice = AnetDevice.where(:token=>params[:device]).last
  if currentDevice != nil
    new_activity.anet_device_id = currentDevice.id
    new_activity.save
    # if the device is a thermostat, extract the payload from alexa
    content_to_build=params[:content]
    if currentDevice.version=="thermostatv1"
      thermo_sql_1 = "select ac.unit,ac.command,ac.id,ac.command_data,av.value FROM anet_commands ac left JOIN (select * from anet_values where id in (select max(id) from anet_values group by command_id)) av ON av.command_id = ac.id where ac.arduino_id=#{currentDevice.id} and ac.is_only_question=0 order by av.id desc"
      temp_res_1= ActiveRecord::Base.connection.exec_query(thermo_sql_1).to_a
      temperature_num=temp_res_1.find { |obj| obj["command_data"] == 'set_temp' }
      temperature_mode=temp_res_1.find { |obj| obj["command_data"] == 'mode' }
      logger.info params[:raw]["directive"]["payload"]
      if params[:raw]["directive"]["payload"].has_key?("thermostatMode")
        logger.info "thermo MODE >>>>"
        case params[:raw]["directive"]["payload"]["thermostatMode"]["value"]
        when "OFF"
          content_to_build="off"
        when "COOL"
          content_to_build="cool:#{temperature_num["value"]}"
        when "HEAT"
          content_to_build="heat:#{temperature_num["value"]}"
        end
      elsif params[:raw]["directive"]["payload"].has_key?("targetSetpoint")
        if temperature_mode["value"]=="off"
          # dont do anything
          content_to_build="off"
        else
          content_to_build="#{temperature_mode["value"]}:#{params[:raw]["directive"]["payload"]["targetSetpoint"]["value"]}"
        end
      end
    elsif currentDevice.version=="doorv1"
      # search for the status of the door
      content_to_build=params[:raw]["directive"]["header"]["name"].downcase
    end
    # so far alexa wont rely on the default answer from the primitive sender, instead
    # we will respond with a custom answer
    primitive_sender_v2(params[:device], currentDevice.secret_token, content_to_build,true)
    response_object={
        "event": {
            "header": {
                "namespace": "Alexa",
                "payloadVersion": "3",
                "name": "Response",
                "messageId": params[:raw]["directive"]["header"]["messageId"]+"-R",
                "correlationToken": params[:raw]["directive"]["header"]["correlationToken"]
            },
            "endpoint": {
                "endpointId": currentDevice.token
            },
            "payload": {}
        }
    }
    if currentDevice!= nil && (currentDevice.version==nil || currentDevice.version=="suitchv3" || currentDevice.version=="bulbv1")
      response_object["context"]= {
        "properties": [{
            "namespace": "Alexa.PowerController",
            "name": "powerState",
            "value": params[:content].upcase,
            "timeOfSample": Time.now.utc.iso8601,
            "uncertaintyInMilliseconds": 50
        }]
    }
    elsif currentDevice!= nil && currentDevice.version=="doorv1"
      # select * from system_activities where (device_token = '739b1bb3' and (value='lock' or value='unlock')) order by id desc limit 1;
      door_sql = "select * from system_activities where (device_token = '#{currentDevice.token}' and (value='lock' or value='unlock')) order by id desc limit 1;"
      temp_res_door= ActiveRecord::Base.connection.exec_query(door_sql).to_a
      response_object["context"]= {
        "properties": [
          {
            "namespace": "Alexa.LockController",
            "name": "lockState",
            "value": temp_res_door[0]["value"]=="lock" ? "LOCKED" : "UNLOCKED",
            "timeOfSample": Time.now.utc.iso8601,
            "uncertaintyInMilliseconds": 1000
          }
        ]
      }
    elsif currentDevice!= nil && currentDevice.version=="thermostatv1"
      # extract the info
      thermo_sql = "select ac.unit,ac.command,ac.id,ac.command_data,av.value FROM anet_commands ac left JOIN (select * from anet_values where id in (select max(id) from anet_values group by command_id)) av ON av.command_id = ac.id where ac.arduino_id=#{currentDevice.id} and ac.is_only_question=0 order by av.id desc"
      temp_res= ActiveRecord::Base.connection.exec_query(thermo_sql).to_a
      thermo_props=[]
      temp_res.each do |single_prop|
        if single_prop["command_data"]=="mode"
          thermo_props.push({
            "namespace": "Alexa.ThermostatController",
            "name": "thermostatMode",
            "value": single_prop["value"].upcase,
            "timeOfSample": Time.now.utc.iso8601,
            "uncertaintyInMilliseconds": 500
        })
        elsif single_prop["command_data"]=="set_temp"
          thermo_props.push({
            "namespace": "Alexa.ThermostatController",
            "name": "targetSetpoint",
            "value": {
                "value": single_prop["value"].to_f,
                "scale": "CELSIUS"
            },
            "timeOfSample": Time.now.utc.iso8601,
            "uncertaintyInMilliseconds": 500
        })
        elsif single_prop["command_data"]=="temp"
          thermo_props.push({
            "namespace": "Alexa.TemperatureSensor",
            "name": "temperature",
            "value": {
                "value": single_prop["value"].to_f,
                "scale": "CELSIUS"
            },
            "timeOfSample": Time.now.utc.iso8601,
            "uncertaintyInMilliseconds": 1000
        })
        end
      end
      response_object["context"]={
        "properties": thermo_props
      }
    end
    logger.info "alexa_payload_response >>>>"
    logger.info response_object.inspect
    logger.info "alexa_payload_response <<<<<"
    respond_to do |format|
      format.html { render json: response_object.to_json }
      format.json { render json: response_object.to_json }
    end
  else 
    new_activity.save
    render :nothing => true, :status => 200
  end
end
def receive_api_payload_v2
  # virtual assistant constants
  # suitch = 0
  # siri = 1
  # google = 2
  # alexa = 3
  # nest = 4
  # in this part we should store the activity
  # only fully signed in people can use this
  # if session[:activeUser] != nil 
    new_activity = SystemActivity.new
    new_activity.device_token = params[:token]
    new_activity.virtual_assistant = 0
    # here we need to know if the command_data exist for this device
    # if the data to send is part of an ACTION of this device, then the command_data should be used instead
    new_activity.command = "sendto"
    new_activity.value=params[:content]
    new_activity.raw_content=params[:raw]
    currentDevice = AnetDevice.where(:token=>params[:token],:secret_token=>params[:secret_token]).last
    if currentDevice != nil
      is_command_data = AnetCommand.where(:command_data=>params[:content],:arduino_id=>currentDevice.id).last
      new_activity.anet_device_id = currentDevice.id
      if is_command_data!=nil
        new_activity.command = is_command_data.id
      end
    end
    new_activity.save
    return primitive_sender_v2(params[:token], params[:secret_token], params[:content])
  # else
  #   respond_to do |format|
  #     format.html { render json: "fuckit".to_json }
  #     format.json { render json: "fuckit".to_json }
  #   end
  # end
end
def receive_google_payload_v2
  # virtual assistant constants
  # suitch = 0
  # siri = 1
  # google = 2
  # alexa = 3
  # nest = 4
  # in this part we should store the activity
  new_activity = SystemActivity.new
  new_activity.device_token = params[:token]
  new_activity.virtual_assistant = 2
  new_activity.command = "sendto"
  new_activity.value=params[:content]
  new_activity.raw_content=params[:raw].to_s
  currentDevice = AnetDevice.where(:token=>params[:token],:secret_token=>params[:secret_token]).last
  if currentDevice != nil
    new_activity.anet_device_id = currentDevice.id
  end
  new_activity.save
  return primitive_sender_v2(params[:token], params[:secret_token], params[:content])
end

def receive_nest_payload_v2(token, message)
  # virtual assistant constants
  # suitch = 0
  # siri = 1
  # google = 2
  # alexa = 3
  # nest = 4
  # in this part we should store the activity,
  # in the case of nest, the token represents the unique token provided by google
  # the raw is the content message
  new_activity = SystemActivity.new
  new_activity.device_token = token
  new_activity.virtual_assistant = 4
  new_activity.command = "activity"

  # problem found with this object
#   {
#   "timestamp": "2022-05-30T01:29:23.221916Z",
#   "relationUpdate": {
#     "type": "UPDATED",
#     "subject": "enterprises/cbe4bb2b-0cf0-49cb-aa99-a0af81312b1a/structures/AVPHwEuC0bPJ5nYWgyeaft0tTD6yDnNTeaLBEolcKykgkP_XshAxr3r6FBB02TuPLuZSaRIWcsiPdGpyZPby_eUPyJD7Ww/rooms/AVPHwEtJQw9By4AvryLRH-qvM1yCfYymF6IgWPWzp2y1XslDBa03ZQd0Jrh7IqCmHdzd1qyMzTgdFFf6URIkS4201bnlD2kpgx0pKNpu0GNfKCEMCUeDgrFpeX92A6JYJ03A-jcqViTL0y8",
#     "object": "enterprises/cbe4bb2b-0cf0-49cb-aa99-a0af81312b1a/devices/AVPHwEu6Di4a59yZidG7HF12EsU9loLj_KMdgjQbQg_zUb57OnU3KZm33L0b29TOobiEJoQXX299pGUaVrwJRrChv2FW_Q"
#   },
#   "userId": "AVPHwEszXooAzorc8M0mS_oM9R-HhGP3wlC1iVBfSj4J"
# }
  if JSON.parse(message)["resourceUpdate"] != nil
    # identify if it has events or traits
    if JSON.parse(message)["resourceUpdate"]["events"] == nil
      new_activity.value=JSON.parse(message)["resourceUpdate"]["traits"].keys[0]
    else 
      new_activity.value=JSON.parse(message)["resourceUpdate"]["events"].keys[0]
    end
    new_activity.raw_content=message
    new_activity.save
    uri = URI.parse("http://localhost:3002/update-data")
    header = {'Content-Type'=> 'text/json'}
    https = Net::HTTP.new(uri.host,uri.port)
    # https.use_ssl = true
    req = Net::HTTP::Post.new(uri)
    req['Content-Type'] = 'application/json'
    # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
    req.body = {user_channel:nil,token: token,type:'propservice',id:new_activity.value,value:JSON.parse(message), serviceId: -4}.to_json
    # Send the request
    response = https.request(req)
    puts response.body
  end



  render :nothing => true, :status => 200
end

def random_test_v2
  # search for temp
  # allTemp=AnetCommand.where("command_data LIKE '%temp%'")
  # allTemp.each do |singleItem|
  #   comandon5=AnetCommand.new
  #   comandon5.arduino_id=singleItem.arduino_id
  #   comandon5.command="Target temperature"
  #   comandon5.command_data="set_temp"
  #   comandon5.is_only_question=0
  #   comandon5.unit="°C"
  #   comandon5.save
  #   comandon6=AnetCommand.new
  #   comandon6.arduino_id=singleItem.arduino_id
  #   comandon6.command="Mode"
  #   comandon6.command_data="mode"
  #   comandon6.is_only_question=0
  #   comandon6.save
  # end
  respond_to do |format|
    format.html { render json: "NOT_FOUND".to_json }
    format.json { render json: "NOT_FOUND".to_json }
  end
end

def primitive_sender_v2(my_token, my_secret_token, my_content,disable_response=false)
  # this method sends information directly to the device
  # if no valid TOKEN and SECRET TOKEN are sent
  # then it will not replicate
  currentDevice = AnetDevice.where(:token=>my_token,:secret_token=>my_secret_token).last
  if currentDevice == nil
    respond_to do |format|
      format.html { render json: "NOT_FOUND".to_json }
      format.json { render json: "NOT_FOUND".to_json }
    end
    return
  else
    # this is a capture to identify the type of device and convert
    # some properties for google home and alexa
    if currentDevice.version == "thermostatv1"
      # lets start with google home
      thermo_mode=AnetCommand.where(:arduino_id=>currentDevice.id,:command_data=>"mode").last
      target_temp=AnetCommand.where(:arduino_id=>currentDevice.id,:command_data=>"set_temp").last
      # OFF
      logger.info "my_content >>>>"
      logger.info my_content
      logger.info my_content.index("off")
      logger.info "my_content <<<<<"
      if my_content.index("off")!=nil
        # find the -prop- about the mode and create a new value
        thermo_mode_value=AnetValue.new(:arduino_id=>currentDevice.id,:command_id=>thermo_mode.id,:value=>"off").save
      elsif my_content.index("cool")!=nil
        thermo_mode_value=AnetValue.new(:arduino_id=>currentDevice.id,:command_id=>thermo_mode.id,:value=>"cool").save
        target_temp_value = AnetValue.new(:arduino_id=>currentDevice.id,:command_id=>target_temp.id,:value=>my_content[my_content.index(":")+1..-1]).save
      elsif my_content.index("heat")!=nil
        thermo_mode_value=AnetValue.new(:arduino_id=>currentDevice.id,:command_id=>thermo_mode.id,:value=>"heat").save
        target_temp_value = AnetValue.new(:arduino_id=>currentDevice.id,:command_id=>target_temp.id,:value=>my_content[my_content.index(":")+1..-1]).save
      end
    end
  end
  puts "TRYING TO SEND COMMAND TO DEVICE" 
  s = TCPSocket.new 'localhost', 3000
    while line = s.gets # Read lines from socket
        puts line.inspect
        if line.index("TOKEN?")!=nil
          puts "tokenizing sender... "
          # get a global account to send messages
          global_clients = AnetDevice.where(:location=>"server", :object=>"global")
          selected_global_client = global_clients[rand(global_clients.length)]
          puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
          s.puts selected_global_client.token+"/"+selected_global_client.secret_token+"\n" 
        elsif line.index("WLCME")!=nil
            puts "from Google Home"
            logger.info "from Google Home"
            @anet_device = AnetDevice.where(:token=>my_token).last 
            puts @anet_device.inspect
            @anet_commands=AnetCommand.where(:arduino_id=>@anet_device.id, :command_data=>my_content)
            @anet_commands_toggle=AnetCommand.where(:arduino_id=>@anet_device.id)
            if @anet_device.version == "sfir_alpha0"
              ["garagex","hallx","porchx"].each do |singleCommand|
                logger.warn("sfir.alpha0 sendto "+my_token+" "+singleCommand)
                logger.info("sfir.alpha0 sendto "+my_token+" "+singleCommand)
                s.puts "sendto "+my_token+" "+singleCommand+"\n"
                sleep 1.5
              end
            elsif @anet_device.version != "sfir"
              logger.info "sfir"
              puts "sendto "+my_token+" "+my_content
              s.puts "sendto "+my_token+" "+my_content+"\n\r"
              # sleep 1
            else
              puts "sendto "+my_token+" allx"
              # s.puts "sendto "+params[:token]+" allx\n"
              s.puts "sendto "+my_token+" "+my_content+"\n\r"
              # sleep 1
            end
            break
        end
    end
  s.close
  # ======= the code above is parelized with these calls
  # sendParams=Hash.new
  # sendParams["my_token"]=my_token
  # sendParams["my_secret_token"]=my_secret_token
  # sendParams["my_content"]=my_content
  # DeviceCommunicationJob.perform_async(sendParams)

  if disable_response==false
    respond_to do |format|
      format.html { render json: "Ok".to_json }
      format.json { render json: "Ok".to_json }
    end
  end
end

def get_screensaver_v2
  result = Hash.new
  result["result"] = "error"
  result["version"]="0"
  result["created_at"]="empty"
  result["images"]="cemex"
  result["total"]="0"
#   {
#     "version": "1.0.29",
#     "images": "cemex",
#     "total": "26"
# }
if params[:token] != nil
  ownership=AnetDevice.where(:token=>params[:token]).last
  if ownership!= nil
    screensaver = Screensaver.where(:device_id=>ownership.id).last
    if screensaver!= nil
      result["version"]=screensaver.id.to_s
      result["created_at"]=screensaver.created_at
      result["images"]="cemex"
      result["total"]=screensaver.amount.to_s
      result["result"] = "success"
    end
  end
end

  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: result.to_json, status:200}
    else
      format.json { render json: result.to_json, status:200 }
    end
  end
end

def post_screensaver_v2
  result = Hash.new
  result["result"] = "error"
#   {
#     "version": "1.0.29",
#     "images": "cemex",
#     "total": "26"
# }
if session[:activeUser] != nil && params[:token] != nil
  ownership=AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
  if ownership!=nil && params[:screensaver]!=nil
    result["result"] = "success"
    if File.exists?('/home/suitch/public/sfir/'+params[:token])
      FileUtils.rm_rf Dir.glob("#{'/home/suitch/public/sfir/'+params[:token]}/*") 
    end
    Dir.mkdir('/home/suitch/public/sfir/'+params[:token]) unless File.exists?('/home/suitch/public/sfir/'+params[:token])

    # because we are uploading an array, lets run a loop
    counter = 0
    params[:screensaver].each do |singleFile|
      path = File.join(Rails.root + "public/sfir/"+params[:token], "screen-"+counter.to_s+".jpg")
      counter = counter+1
      # write the file
      File.open(path, "w+b") { |f| f.write(singleFile.read) }
    end
    path_json = File.join(Rails.root + "public/sfir/", params[:token]+".json")
      # write the file
    screensaver=Screensaver.new
    screensaver.device_id=ownership.id
    screensaver.amount = counter
    screensaver.save
      result_json = Hash.new
      result_json["result"] = "success"
      result_json["version"]=screensaver.id.to_s
      result_json["created_at"]="empty"
      result_json["images"]=params[:token]
      result_json["total"]=counter.to_s
    File.open(path_json, "w+b") { |f| f.write(result_json.to_json) }
    # limpiamos
  end
end

  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: result.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end

def get_device_logs_v2
  result = Hash.new
  result["result"] = "error"
  @device_logs = []
  if session[:activeUser] != nil && params[:token] != nil
    # first verify ownership
    ownership=AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
    if ownership != nil 
      result["result"] = "success"
      # we need some things to extract such as the command names and ids for further use
      commands = AnetCommand.where(:arduino_id=>ownership.id)
      sql_connection_log = "select anet_logs.connection_type, anet_logs.created_at, anet_logs.updated_at FROM `anet_devices` INNER JOIN `anet_logs` ON `anet_logs`.`arduino_id` = `anet_devices`.`id` where `anet_devices`.`token`='"+ownership.token+"' order by anet_logs.id desc LIMIT 30"
      # @device_logs = AnetDevice.sql(:token=>params[:token]).joins(:anet_log).select("anet_devices.token, anet_logs.connection_type, anet_logs.created_at, anet_logs.updated_at").limit(30)
      temp_res= ActiveRecord::Base.connection.exec_query(sql_connection_log).to_a
      temp_res.each do |single_result|
        single_result["type"]="connection"
      end
      @device_logs += temp_res;
      # the first part gives us the connection logs, but we also need the props send back to suitch to store values
      # sql_commands_log = "select  anet_commands.command, anet_values.created_at, anet_values.updated_at FROM `anet_values` INNER JOIN `anet_commands` ON `anet_values`.`command_id` = `anet_commands`.`id` where `anet_values`.`arduino_id`="+ownership.id.to_s+" order by anet_values.id desc LIMIT 30;"
      # performance issue
      # sql_commands_log = "select ac.command, anet_values.created_at, anet_values.updated_at FROM `anet_values` INNER JOIN (select * from anet_commands where is_only_question!=0) ac ON `anet_values`.`command_id` = `ac`.`id` where `anet_values`.`arduino_id`="+ownership.id.to_s+" order by anet_values.id desc LIMIT 30"
      sql_commands_log = "SELECT ac.command, anet_values.created_at, anet_values.updated_at FROM anet_values INNER JOIN anet_commands ac ON anet_values.command_id = ac.id WHERE anet_values.arduino_id ="+ownership.id.to_s+"  AND ac.is_only_question != 0 ORDER BY anet_values.id DESC LIMIT 30;"
      temp_res= ActiveRecord::Base.connection.exec_query(sql_commands_log).to_a
      temp_res.each do |single_result|
        single_result["type"]="command"
      end
      @device_logs += temp_res;
      # finally we will need external interaction from assistants such as alexa
      sql_assistant_interactions = "select virtual_assistant, created_at, updated_at from system_activities where device_token = '"+ownership.token+"' order by id desc limit 30;"
      temp_res= ActiveRecord::Base.connection.exec_query(sql_assistant_interactions).to_a
      temp_res.each do |single_result|
        single_result["type"]="assistant"
      end
      @device_logs += temp_res;
      @device_logs=@device_logs.sort { |a,b| a["created_at"] <=> b["created_at"] }
      @device_logs.reverse!
    end
  end
  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: @device_logs.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end

def get_schedules_v2
  result = Hash.new
  result["result"] = "error"
  if session[:activeUser] != nil
    @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
    if @anet_device != nil
      sql_schedule = "select sys_schedules.id,sys_schedules.schedule, anet_commands.command,anet_commands.command_data FROM `anet_commands` INNER JOIN `sys_schedules` ON `sys_schedules`.`command` = `anet_commands`.`id` where sys_schedules.`device_id`="+@anet_device.id.to_s
      @schedules = ActiveRecord::Base.connection.exec_query(sql_schedule)
      result["result"] = "success"
    end
  end
  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: @schedules.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end
def get_status_v2
  result = Hash.new
  result["result"] = "error"
  if session[:activeUser] != nil
    @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
    if @anet_device != nil
      sql_status = "select * from anet_logs where arduino_id="+@anet_device.id.to_s+" order by id desc limit 1"
      @device_status= ActiveRecord::Base.connection.exec_query(sql_status).to_a
      @device_status= @device_status[0]
      # if @device_status == nil
      #     @device_status=Hash.new
      #     @device_status["connection_type"] == -1
      # end
      result["result"] = "success"
    end
  end
  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: @device_status.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end
def get_props_v2
  result = Hash.new
  result["result"] = "error"
  if session[:activeUser] != nil
    if params[:token]=="nest" || params[:token]=="acurite" || params[:token]=="ecowitt" 
      virtual_assistant = case params[:token]
      when "nest" then 4
      when "acurite" then 5
      when "ecowitt" then 6
      end

      sql_status = "SELECT null as unit, value as command, value as id, value as command_data, virtual_assistant as value FROM lineAccess.system_activities where value is not null AND value != '' and virtual_assistant=#{virtual_assistant} group by value, virtual_assistant,command;"
      @device_status= ActiveRecord::Base.connection.exec_query(sql_status).to_a
      result["result"] = "success"
    else 
      @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
      if @anet_device != nil
        # old query with performance issues
        # sql_status = "select ac.unit,ac.command,ac.id,ac.command_data,av.value FROM anet_commands ac left JOIN (select * from anet_values where `id` in (select max(`id`) from anet_values group by `command_id`)) av ON av.command_id = ac.id where ac.arduino_id="+@anet_device.id.to_s+" and ac.is_only_question=0 order by av.id desc"
        sql_status = "SELECT ac.unit,ac.command,ac.id,ac.command_data,av.value FROM anet_commands ac LEFT JOIN anet_values av ON av.command_id = ac.id INNER JOIN (SELECT command_id, MAX(id) AS max_id FROM anet_values GROUP BY command_id) groupedav ON av.command_id = groupedav.command_id AND av.id = groupedav.max_id WHERE ac.arduino_id="+@anet_device.id.to_s+" AND ac.is_only_question=0 ORDER BY av.id DESC;"
        @device_status= ActiveRecord::Base.connection.exec_query(sql_status).to_a
        # due to a performance issue with the previous one, we will extract now the prop by hand, which might be faster
        result["result"] = "success"
      end
    end
    

  end
  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: @device_status.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end

def get_props_v2_1
  status, @device_status = if session[:activeUser]
    case params[:token]
    when "nest", "acurite", "ecowitt"
      virtual_assistant = case params[:token]
      when "nest" then 4
      when "acurite" then 5
      when "ecowitt" then 6
      end
      @activities = SystemActivity.where.not(value: [nil, ''])
                                    .where(virtual_assistant: virtual_assistant)
                                    .select(:unit, :value, :command, :command_data, :virtual_assistant)
                                    .group(:value, :virtual_assistant, :command)
      ['success', @activities]
    else
      @anet_device = AnetDevice.where(token: params[:token], id_owner: session[:activeUser].id).first
      if @anet_device
        subquery = AnetValue.select("MAX(id) as id").group(:command_id).to_sql
        @device_status = AnetCommand.joins("LEFT JOIN (#{subquery}) av ON av.id = anet_commands.id")
                                    .where(arduino_id: @anet_device.id, is_only_question: false)
                                    .order('av.id desc')
        ['success', @device_status]
      else
        ['error', nil]
      end
    end
  else
    ['error', nil]
  end

  respond_to do |format|
    if status == "success"
      format.json { render json: @device_status.to_json, status: 200 }
    else
      format.json { render json: { result: status }.to_json, status: 401 }
    end
  end
end

def get_prop_chart_v2
  result = Hash.new
  result["result"] = "error"
  result["values"] = []
  if session[:activeUser] != nil
    @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
    if @anet_device != nil
      # sql_single_prop="select ac.command,ac.id,ac.command_data,ac.unit,av.value FROM anet_commands ac INNER JOIN (select * from anet_values where `id` in (select max(`id`) from anet_values group by `command_id`)) av ON av.command_id = ac.id where av.arduino_id="+@anet_device.id.to_s+" and ac.id="+params[:id].to_s
      # @device_status= ActiveRecord::Base.connection.exec_query(sql_single_prop).to_a
      # due to a performance issue with the previous one, we will extract now the prop by hand, which might be faster
      device_command = AnetCommand.where(:id=>params[:id]).last
      device_value = AnetValue.where(:arduino_id=>@anet_device.id,:command_id=>device_command.id).last(100)
      @device_status=Hash.new
      @device_status["command"]=device_command.command
      @device_status["command_data"]=device_command.command_data
      @device_status["unit"]=device_command.unit
      @device_status["values"]=device_value
      result["result"] = "success"
    end
  end
  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: @device_status.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end
def get_prop_chart_24h_v2
  # select avg(value),created_at from anet_values where (arduino_id=394 and created_at > '2021-11-11 12:12:03') group by hour(created_at) order by id desc;
  # sql_single_prop="select ac.command,ac.id,ac.command_data,ac.unit,av.value FROM anet_commands ac INNER JOIN (select * from anet_values where `id` in (select max(`id`) from anet_values group by `command_id`)) av ON av.command_id = ac.id where av.arduino_id="+@anet_device.id.to_s+" and ac.id="+params[:id].to_s
  # @device_status= ActiveRecord::Base.connection.exec_query(sql_single_prop).to_a
  # due to a performance issue with the previous one, we will extract now the prop by hand, which might be faster
  # earthquake validated 2021-12-07 23:18:57, id 6926724
  # result = Hash.new
  # result["result"] = "error"
  # result["values"] = []
  # if session[:activeUser] != nil
  #   @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
  #   if @anet_device != nil
  #     device_command = AnetCommand.where(:id=>params[:id]).last
  #     device_value = AnetValue.where(:arduino_id=>@anet_device.id,:command_id=>device_command.id).last(310)
  #     @device_status=Hash.new
  #     @device_status["command"]=device_command.command
  #     @device_status["command_data"]=device_command.command_data
  #     @device_status["unit"]=device_command.unit
  #     @device_status["values"]=device_value
  #     result["result"] = "success"
  #   end
  # end
  # respond_to do |format|
  #   if result["result"] == "success"
  #     format.json { render json: @device_status.to_json, status:200}
  #   else
  #     format.json { render json: result.to_json, status:401 }
  #   end
  # end

  result = Hash.new
  result["result"] = "error"
  result["values"] = []
  if session[:activeUser] != nil
    @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
    if @anet_device != nil
      device_command = AnetCommand.where(:id=>params[:id]).last
      device_value = AnetValue.where(
        :arduino_id => @anet_device.id,
        :command_id => device_command.id,
        :created_at => (Time.now - 24.hours)..Time.now
      ).last(310)
      @device_status=Hash.new
      @device_status["command"]=device_command.command
      @device_status["command_data"]=device_command.command_data
      @device_status["unit"]=device_command.unit
      @device_status["values"]=device_value
      result["result"] = "success"
    end
  end
  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: @device_status.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end
def get_prop_chart_7day_v2
  # sql_single_prop="select ac.command,ac.id,ac.command_data,ac.unit,av.value FROM anet_commands ac INNER JOIN (select * from anet_values where `id` in (select max(`id`) from anet_values group by `command_id`)) av ON av.command_id = ac.id where av.arduino_id="+@anet_device.id.to_s+" and ac.id="+params[:id].to_s
  # @device_status= ActiveRecord::Base.connection.exec_query(sql_single_prop).to_a
  # due to a performance issue with the previous one, we will extract now the prop by hand, which might be faster
  result = Hash.new
  result["result"] = "error"
  result["values"] = []
  if session[:activeUser] != nil
    @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
    if @anet_device != nil
      device_command = AnetCommand.where(:id=>params[:id]).last
      time_range = (Time.now - 7.days)..Time.now
      device_value = AnetValue.where(
        :arduino_id => @anet_device.id,
        :command_id => device_command.id,
        :created_at => time_range
      ).last(100)
      @device_status=Hash.new
      @device_status["command"]=device_command.command
      @device_status["command_data"]=device_command.command_data
      @device_status["unit"]=device_command.unit
      @device_status["values"]=device_value
      result["result"] = "success"
    end
  end
  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: @device_status.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end
def get_prop_chart_month_v2
  # sql_single_prop="select ac.command,ac.id,ac.command_data,ac.unit,av.value FROM anet_commands ac INNER JOIN (select * from anet_values where `id` in (select max(`id`) from anet_values group by `command_id`)) av ON av.command_id = ac.id where av.arduino_id="+@anet_device.id.to_s+" and ac.id="+params[:id].to_s
  # @device_status= ActiveRecord::Base.connection.exec_query(sql_single_prop).to_a
  # due to a performance issue with the previous one, we will extract now the prop by hand, which might be faster
  result = Hash.new
  result["result"] = "error"
  result["values"] = []
  if session[:activeUser] != nil
    @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
    if @anet_device != nil
      device_command = AnetCommand.where(:id=>params[:id]).last
      time_range = Time.now.beginning_of_month..Time.now
      device_value = AnetValue.where(
        :arduino_id => @anet_device.id,
        :command_id => device_command.id,
        :created_at => time_range
      ).last(100)
      @device_status=Hash.new
      @device_status["command"]=device_command.command
      @device_status["command_data"]=device_command.command_data
      @device_status["unit"]=device_command.unit
      @device_status["values"]=device_value
      result["result"] = "success"
    end
  end
  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: @device_status.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end

def get_prop_v2
  result = Hash.new
  result["result"] = "error"
  if session[:activeUser] != nil
    @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
    if @anet_device != nil
      # sql_single_prop="select ac.command,ac.id,ac.command_data,ac.unit,av.value FROM anet_commands ac INNER JOIN (select * from anet_values where `id` in (select max(`id`) from anet_values group by `command_id`)) av ON av.command_id = ac.id where av.arduino_id="+@anet_device.id.to_s+" and ac.id="+params[:id].to_s
      # @device_status= ActiveRecord::Base.connection.exec_query(sql_single_prop).to_a
      # due to a performance issue with the previous one, we will extract now the prop by hand, which might be faster
      device_command = AnetCommand.where(:id=>params[:id]).last
      device_value = AnetValue.where(:arduino_id=>@anet_device.id,:command_id=>device_command.id).last
      
      @device_status=Hash.new
      @device_status["command"]=device_command.command
      @device_status["command_data"]=device_command.command_data
      @device_status["unit"]=device_command.unit
      if device_value!= nil
        @device_status["value"]=device_value.value
        @device_status["created_at"]=device_value.created_at
      end
      result["result"] = "success"
    end
  end
  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: @device_status.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end

def get_fav_v2
  result = Hash.new
  result["result"] = "error"
  if session[:activeUser] != nil
    @favs = AnetDevice.where(:fav=>1,:id_owner=>session[:activeUser].id)
    result["result"] = "success"
  end
  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: @favs.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end

def post_fav_v2
  result = Hash.new
  result["result"] = "error"
  if session[:activeUser] != nil
    @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
    if @anet_device != nil
      @anet_device.fav=params[:fav]
      @anet_device.save
      result["result"] = "success"
    end
  end
  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: @anet_device.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end

def delete_schedule_v2
  result = Hash.new
  result["result"] = "error"
  if session[:activeUser] != nil
    @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
    if @anet_device != nil
      schedule = SysSchedule.where(:device_id=>@anet_device.id,:id=>params[:id]).last
      result["result"] = "success"
      if schedule!= nil
        schedule.destroy
      end
    end
  end
  respond_to do |format|
    if result["result"] == "success"
      format.json { render json: @schedules.to_json, status:200}
    else
      format.json { render json: result.to_json, status:401 }
    end
  end
end
  # DELETE /anet_devices/1
  # DELETE /anet_devices/1.json
  def destroy
    result = Hash.new
    result["result"] = "error"
    if params[:id].is_number? == true
      @anet_device = AnetDevice.where(:id=>params[:id], :id_owner=>session[:activeUser].id).last
    else
      @anet_device = AnetDevice.where(:token=>params[:id],:id_owner=>session[:activeUser].id).last
    end
    # instead of removing the device, lets unlink it from the user
    # @anet_device.destroy
    if @anet_device != nil
      @anet_device.id_owner = nil
      @anet_device.save
      result["result"] = "success"
    end
    # as a last step for the unassign, we resync google home
    uri = URI.parse("http://localhost:3001/smarthome/create")
    https = Net::HTTP.new(uri.host,uri.port)
    # https.use_ssl = true
    req = Net::HTTP::Post.new(uri)
    req['Content-Type'] = 'application/json'
    req.body = {userId: session[:activeUser].id.to_s}.to_json
    response = https.request(req)

    respond_to do |format|
      if result["result"] == "success"
        format.html { redirect_to "/personal_accounts",notice: 'Devices list was updated.' }
        format.json { render json: result.to_json, status:200}
      else
        format.html { redirect_to "/personal_accounts",notice: 'Error while updating' }
        format.json { render json: result.to_json, status:401 }
      end
    end
  end


  def test_api 
    respond_to do |format|

        format.json { render json: "hola edgar".to_json, status:200}
        format.html { render json: "hola edgar".to_json, status:401 }
    end
  end

  def getAvailableDevicePerSsid
    @availableDevices = []
    # get client ip 
    @clientIp = request.remote_ip
    @anet_devices = AnetDevice.where(:ssid=>params[:ssid], :id_owner=> nil, :mip=>@clientIp)
    # @anet_devices = AnetDevice.where(:ssid=>params[:ssid], :mip=>"189.158.47.116")
    # @anet_devices = AnetDevice.where(:ssid=>params[:ssid], :id_owner=> nil, :mip=>"189.158.47.116")
    
    if(@anet_devices != nil and @anet_devices.length > 0) 
      @anet_devices.each do |item|
        @availableDevices.push({
          :id=> item.id, 
          :ssid=> item.ssid, 
          :version=> item.version, 
          :description=> item.description, 
          :object=> item.object
      })
      end 
    end
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @availableDevices }
    end
  end

  def setIdOwnerToDevices
    @devices = JSON.parse request.body.read 
    @test = []

    @devices["devices"].each do |item| 
      # idOwner_sql = "update anet_devices set id_owner=#{session[:activeUser].id} where id=#{item["deviceId"]};"
      # idOwner_sql = ";"
      # result = ActiveRecord::Base.connection.exec_query(idOwner_sql).to_a
      @anet_device = AnetDevice.where(:id=>item["deviceId"] ).last
      @anet_device.update_attribute(:id_owner, session[:activeUser].id.to_i)

      @test.push({
        :elid=>item["deviceId"]
      })
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @test }
    end
  end

  def post_notifications_dismiss
    # notifications_sql = "update user_device_notifications set seen=0 where id_owner=#{session[:activeUser].id};"
    # result = ActiveRecord::Base.connection.exec_query(notifications_sql)
    UserDeviceNotifications.where(id_owner: session[:activeUser].id).where(seen: nil).update_all(seen: 0)
    respond_to do |format|
      format.json { render json: "ok".to_json }
    end
  end

  def getDeviceNotifications 
    @id_owner=session[:activeUser].id
    notifications_sql = "select udn.*,anet_devices.version FROM user_device_notifications udn INNER JOIN `anet_devices` ON `anet_devices`.`token` = `udn`.`token` where udn.id_owner=#{session[:activeUser].id} order BY created_at DESC limit 30;"
    result = ActiveRecord::Base.connection.exec_query(notifications_sql).to_a
    # @userDeviceNotifications = UserDeviceNotifications.where(:id_owner=>session[:activeUser].id)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: result }
    end
  end

  # POST /anet_devices
  # POST /anet_devices.json
  def findmy_create
    @anet_device = AnetDevice.new
    if session[:activeUser] != nil
      # if params.has_key?("claim") and params[:claim] == "yes"
      #   @anet_device =AnetDevice.where(:token=>params[:anet_device]["token"], :secret_token=>params[:anet_device]["secret_token"],:id_owner => nil).last
      # elsif params.has_key?("ssid") and params[:ssid] == "yes"
      #   @anet_device =AnetDevice.where(:ssid=> params[:ssid],:id_owner => nil,:mip => request.ip).last
      # else
      #   @anet_device =AnetDevice.new(params[:anet_device])
      #   @anet_device.token=UUIDTools::UUID.random_create.to_s()
      #   @anet_device.token=@anet_device.token[0..7]
      #   @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
      #   @anet_device.secret_token=@anet_device.secret_token[0..7]
      # end
      #creamos un token y un token secret
      # @anet_device.id_owner=session[:activeUser].id
      @anet_device.description = params[:anet_device]["description"]
      @anet_device.location = params[:anet_device]["location"]
      @anet_device.object = params[:anet_device]["object"]
      @anet_device.token = params[:anet_device]["token"]
      @anet_device.secret_token = params[:anet_device]["secret_token"]
      @anet_device.notify_telegram = 0
      @anet_device.notify_email = 0
      @anet_device.notify_twitter = 0
      @anet_device.notify_fbinbox = 0
      @anet_device.is_public = 0
      @anet_device.version = params[:anet_device]["version"]
      
    end
    
    respond_to do |format|
      responseHash = Hash.new
      responseHash["result"] = "error"
      if @anet_device != nil and @anet_device.save

        # depending of the type of the device the commands and command data is different
        if @anet_device.version == "trackerv1"
          comandon=AnetCommand.new
          comandon.arduino_id=@anet_device.id
          comandon.command="Location"
          comandon.command_data="loc"
          comandon.unit=""
          comandon.is_only_question=0
          comandon.save
          comandon=AnetCommand.new
          comandon.arduino_id=@anet_device.id
          comandon.command="BT_KEY"
          comandon.command_data="btkey"
          # the unit on the Bluetooth devices is the hashed key
          comandon.unit=params[:anet_device]["device_bt_key"]
          comandon.is_only_question=0
          comandon.save
          comandon=AnetCommand.new
          comandon.arduino_id=@anet_device.id
          comandon.command="BT_PRIV"
          comandon.command_data="btpriv"
          comandon.unit=params[:anet_device]["device_bt_privatekey"]
          comandon.is_only_question=0
          comandon.save
        end

        responseHash["result"] = "success"
        # format.html { redirect_to "/personal_accounts", notice: 'A new device was successfully added to the arduino and raspberry network.' }
        if params.has_key?("source") && params[:source] == 'web'
          format.json { render json: responseHash.to_json }
        else
          format.json { redirect_to "/personal_accounts", notice: 'A new device was successfully added to the arduino and raspberry network.' }
        end
      else
        # format.html { redirect_to "/personal_accounts", notice: "No devices added" }
        if (params.has_key?("source") && params[:source] == 'web') or session[:activeUser] == nil
          format.json { render json: responseHash.to_json, status: 401 }
        else
          format.json { redirect_to "/personal_accounts", notice: "No devices added" }
        end
      end
    end
  end

  def get_findmy_all
    # get all devices that have BT_KEY command and BT_PRIV command as a json
    notifications_sql = "SELECT MAX(CASE WHEN command = 'BT_KEY' THEN unit ELSE NULL END) AS bt_key,MAX(CASE WHEN command = 'BT_PRIV' THEN unit ELSE NULL END) AS bt_priv FROM anet_commands GROUP BY arduino_id HAVING bt_key IS NOT NULL AND bt_priv IS NOT NULL;"
    result = ActiveRecord::Base.connection.exec_query(notifications_sql).to_a
    
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: result }
    end
  end

  def get_findmy_sensors
    # get all devices that have BT_KEY command and BT_PRIV command as a json
    notifications_sql = "SELECT * from anet_less_sensors;"
    result = ActiveRecord::Base.connection.exec_query(notifications_sql).to_a
    
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: result }
    end
  end

  def findmy_located_old
    # Map all unique location values
    if params[:anet_device]["reports"] != nil
      locations = params[:anet_device]["reports"].map{|report| report["location"]}.uniq

      # Find existing AnetValue records
      existing_anet_values = AnetValue.where("value NOT LIKE '%location%' AND value IN (?)", locations)
      
      # Map all unique client values
      clients = params[:anet_device]["reports"].map{|report| report["client"]}.uniq

      # Find existing AnetCommand records
      # existing_anet_commands = AnetCommand.where("unit IN (?) OR (command = 'Location' AND arduino_id IN (?))", clients, existing_anet_values.map(&:arduino_id)).group_by(&:unit)
      existing_anet_commands = AnetCommand.where("unit IN (?)", clients).group_by(&:unit)
      existing_anet_commands_location = AnetCommand.where("command = 'Location'").group_by(&:arduino_id)


      ActiveRecord::Base.transaction do
        params[:anet_device]["reports"].each do |report|
          anet_value = existing_anet_values.find{|av| av.value == report["location"]}
          
          if anet_value.nil?
            anet_value = AnetValue.new(value: report["location"])
            

            # find the device that has the client using the anet_command as pivot table
            anet_command = existing_anet_commands[report["client"]].first
            if anet_command.present?
              anet_value.arduino_id=anet_command.arduino_id

              # find the anet_command for this device that has the key location
              # anet_command = existing_anet_commands.select{|ac| ac.command == 'Location' && ac.arduino_id == anet_value.arduino_id}.first
              anet_command = existing_anet_commands_location[anet_value.arduino_id].find{|ac| ac.command == 'Location'}
              if anet_command.present?
                anet_value.command_id=anet_command.id
              end
              # anet_value.save
            end
            anet_value.save
          end
        end
      end
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: "ok" }
    end
  end

  def findmy_located
    # Early return if no reports
    if params[:anet_device]["reports"].blank?
      render json: "ok"
      return
    end
  
    # Map all unique location and client values
    reports = params[:anet_device]["reports"]
    locations = reports.map { |report| report["location"] }.uniq
    clients = reports.map { |report| report["client"] }.uniq
    
    tracker_list = AnetDevice.where(version: 'trackerv1').pluck(:id)
    
    # Now, map locations to strings with single quotes around them for SQL query
    locations_string = locations.map { |location| "'#{location}'" }.join(',')
    sql = "SELECT * FROM anet_devices JOIN anet_values ON anet_devices.id = anet_values.arduino_id WHERE anet_devices.version = 'trackerv1' and (value IN (#{locations_string}));"
    existing_anet_values = AnetDevice.find_by_sql(sql).index_by(&:value)
  
    # Fetch all AnetCommand records for given units, then group them by unit and command
    existing_anet_commands = AnetCommand.where(unit: clients).group_by { |ac| ac.unit }
    logger.warn(existing_anet_commands)
    ActiveRecord::Base.transaction do
      reports.each do |report|
        anet_value = existing_anet_values[report["location"]]
    
        # Only create new AnetValue record if it doesn't exist
        if anet_value.nil?
          anet_value = AnetValue.new(value: report["location"])
    
          # Try to find the anet_command with the corresponding client and assign arduino_id
          if existing_anet_commands[report["client"]] != nil
            if (anet_command = existing_anet_commands[report["client"]].first)
              anet_value.arduino_id = anet_command.arduino_id
              anet_value.command_id = anet_command.id
            end
          end
    
          anet_value.save!
          datetime_obj = Time.at(report["timestamp"]).to_datetime
          anet_value.update_column(:created_at, datetime_obj)
          anet_value.save!
          # Add new value to the existing_anet_values for future lookups in the same transaction
          existing_anet_values[anet_value.value] = anet_value
        end
      end
    end
    
    render json: "ok"
  end
  
  def get_findmy_location
    # get all entries from anet_values that have the location command and the token of the device is the params[:token]
    notifications_sql = "SELECT value,created_at from anet_values where command_id in (select id from anet_commands where command='BT_KEY' and arduino_id in (select id from anet_devices where token='#{params[:token]}')) ORDER BY created_at DESC limit 50"
    result = ActiveRecord::Base.connection.exec_query(notifications_sql).to_a
    notifications_sql_old = "SELECT value,created_at from anet_values where command_id in (select id from anet_commands where command='Location' and arduino_id in (select id from anet_devices where token='#{params[:token]}')) ORDER BY created_at DESC limit 50"
    result2 = ActiveRecord::Base.connection.exec_query(notifications_sql_old).to_a
    # concat result and result2
    result = result + result2
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: result }
    end
  end

  # POST /anet_devices
  # POST /anet_devices.json
  def findmy_create_sensor
    @anet_device = AnetDevice.new
    if session[:activeUser] != nil
     
      # Iterate through the items
      
      @anet_device.location = params[:anet_device]["location"]
      @anet_device.object = params[:anet_device]["object"]
      @anet_device.token = params[:anet_device]["token"]
      @anet_device.secret_token = params[:anet_device]["secret_token"]
      @anet_device.notify_telegram = 0
      @anet_device.notify_email = 0
      @anet_device.notify_twitter = 0
      @anet_device.notify_fbinbox = 0
      @anet_device.is_public = 0
      @anet_device.version = params[:anet_device]["version"]
      
    end
    
    respond_to do |format|
      responseHash = Hash.new
      responseHash["result"] = "error"
      if @anet_device != nil and @anet_device.save

        # depending of the type of the device the commands and command data is different
        if @anet_device.version == "templessv1"
          description_str = params[:anet_device]["description"]
          description_str = description_str[1...-1] if description_str.start_with?('"') && description_str.end_with?('"')
          
          # Replace single quotes with double quotes for valid JSON
          description_str = description_str.gsub("'", '"')

          # Parse the JSON string into a Ruby object
          description_json = JSON.parse(description_str)
          description_json.each do |item|
            # You can access values like item["Private key"], item["Advertisement key"], etc.
            # Perform your logic here.
            anetless_key = AnetLessSensor.new
            anetless_key.adv_key = item["adv_key"]
            anetless_key.priv_key = item["priv_key"]
            anetless_key.pub_key = item["hash_adv_key"]
            anetless_key.value = item["value"]
            anetless_key.id_sensor = @anet_device.id
            anetless_key.save
    
          end
        elsif @anet_device.version == "soilblessv1"
           # Remove extra quotes at the beginning and the end, if present
          description_str = params[:anet_device]["description"]
          description_str = description_str[1...-1] if description_str.start_with?('"') && description_str.end_with?('"')
          
          # Replace single quotes with double quotes for valid JSON
          description_str = description_str.gsub("'", '"')

          # Parse the JSON string into a Ruby object
          description_json = JSON.parse(description_str)
          description_json.each do |item|
            # You can access values like item["Private key"], item["Advertisement key"], etc.
            # Perform your logic here.
            anetless_key = AnetLessSensor.new
            anetless_key.adv_key = item["adv_key"]
            anetless_key.priv_key = item["priv_key"]
            anetless_key.pub_key = item["hash_adv_key"]
            anetless_key.value = item["value"]
            anetless_key.id_sensor = @anet_device.id
            anetless_key.save
    
          end
        else
          # this commands will depend on the device to create, for the time being these are placeholders
          comandon=AnetCommand.new
          comandon.arduino_id=@anet_device.id
          comandon.command="Location"
          comandon.command_data="loc"
          comandon.unit=""
          comandon.is_only_question=0
          comandon.save
          comandon=AnetCommand.new
          comandon.arduino_id=@anet_device.id
          comandon.command="BT_KEY"
          comandon.command_data="btkey"
          # the unit on the Bluetooth devices is the hashed key
          comandon.unit=params[:anet_device]["device_bt_key"]
          comandon.is_only_question=0
          comandon.save
          comandon=AnetCommand.new
          comandon.arduino_id=@anet_device.id
          comandon.command="BT_PRIV"
          comandon.command_data="btpriv"
          comandon.unit=params[:anet_device]["device_bt_privatekey"]
          comandon.is_only_question=0
          comandon.save
        end

        responseHash["result"] = "success"
        # format.html { redirect_to "/personal_accounts", notice: 'A new device was successfully added to the arduino and raspberry network.' }
        if params.has_key?("source") && params[:source] == 'web'
          format.json { render json: responseHash.to_json }
        else
          format.json { redirect_to "/personal_accounts", notice: 'A new device was successfully added to the arduino and raspberry network.' }
        end
      else
        # format.html { redirect_to "/personal_accounts", notice: "No devices added" }
        if (params.has_key?("source") && params[:source] == 'web') or session[:activeUser] == nil
          format.json { render json: responseHash.to_json, status: 401 }
        else
          format.json { redirect_to "/personal_accounts", notice: "No devices added" }
        end
      end
    end
  end

  def post_findmy_sensors
    if params[:anet_device]["reports"].blank?
      render json: "ok"
      return
    end
  
    # Map all unique location and client values
    reports = params[:anet_device]["reports"]
    # locations contain the lat, long of the located key, it is hashed as base64
    locations = reports.map { |report| report["location"] }.uniq
    # this is the key for identifying the value that can be matched
    clients = reports.map { |report| report["client"] }.uniq
    
    # instead of extracting the anetcommands for sensors without internet, we are getting the reference from each client
    # the reference from the AnetLessSensor will bring the map of the value and the pub key and then we can store the direct value with its corresponding date and time
    
    # Fetch all AnetCommand records for given units, then group them by unit and command
    findmy_sensor_map_values = AnetLessSensor.where(pub_key: clients).group_by { |ac| ac.pub_key }
    logger.warn(findmy_sensor_map_values)

    ActiveRecord::Base.transaction do
      reports.each do |report|
        anet_value = findmy_sensor_map_values[report["client"]][0]["value"]
        # create a new anet_value without command reference
        newAnetValue = AnetValue.new(value: anet_value,arduino_id: findmy_sensor_map_values[report["client"]][0]["id_sensor"])
        # Only create new AnetValue record if it doesn't exist
        newAnetValue.save!
        # update the date and time of the created_at
        datetime_obj = Time.at(report["timestamp"]).to_datetime
        newAnetValue.update_column(:created_at, datetime_obj)
        # newAnetValue.created_at = findmy_sensor_map_values[report["client"]][0]["datetime"]
        newAnetValue.save!
      end
    end
    
    # render json: "ok"
    render json: findmy_sensor_map_values
  end

  def get_findmy_soil
    # get all entries from anet_values that have the location command and the token of the device is the params[:token]
    notifications_sql = "SELECT value,created_at from anet_values where arduino_id in (select id from anet_devices where token='#{params[:token]}') ORDER BY created_at DESC limit 100"
    result = ActiveRecord::Base.connection.exec_query(notifications_sql).to_a

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: result }
    end
  end

  def get_batteries
    # get all entries from anet_values that have the location command and the token of the device is the params[:token]
    # notifications_sql = "SELECT value,created_at from anet_values where arduino_id in (select id from anet_devices where token='#{params[:token]}') ORDER BY created_at DESC limit 50"
    battery_details_sql = "SELECT id, name, capacity, voltage, device_type, days from battery_types;"
    result = ActiveRecord::Base.connection.exec_query(battery_details_sql).to_a
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: result }
    end
  end

  def get_battery_details
    # get all entries from anet_values that have the location command and the token of the device is the params[:token]
    # notifications_sql = "SELECT value,created_at from anet_values where arduino_id in (select id from anet_devices where token='#{params[:token]}') ORDER BY created_at DESC limit 50"
    isValidDevice = AnetDevice.where(:token => params[:token], :id_owner => session[:activeUser].id ).last
    battery_details_sql = "SELECT bc.`id` AS battery_cycle_id,bc.`battery_id`,bc.`created_at`,bt.`name` AS battery_type_name FROM anet_devices AS ad JOIN battery_cycles AS bc ON ad.id = bc.arduino_id JOIN battery_types AS bt ON bc.battery_id = bt.id WHERE ad.token = '#{params[:token]}' AND ad.version = '#{isValidDevice.version}';"
    result = ActiveRecord::Base.connection.exec_query(battery_details_sql).to_a
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: result }
    end
  end

  def post_battery_update
    result = Hash.new
    result["result"]="error"
    if session[:activeUser] != nil
      new_battery = params[:selectedBattery]
      # find the device based on the token
      isValidDevice = AnetDevice.where(:token => params[:token], :id_owner => session[:activeUser].id ).last
      if isValidDevice != nil
        newBattery = BatteryCycles.new
        newBattery.arduino_id = isValidDevice.id
        newBattery.battery_id = new_battery
        newBattery.save
        result["result"]="success"
      end
      respond_to do |format|
        if result["result"] == "error"
          format.json { render json: result.to_json, status:401 }
        else
          format.json { render json: result.to_json, status:200  }
        end
      end
    else 
      format.json { render json: "error", status: 401 }
    end
  end
end
