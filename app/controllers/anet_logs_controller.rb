class AnetLogsController < ApplicationController
  # GET /anet_logs
  # GET /anet_logs.json
  def index
    @anet_logs = AnetLog.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @anet_logs }
    end
  end

  # GET /anet_logs/1
  # GET /anet_logs/1.json
  def show
    @anet_log = AnetLog.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @anet_log }
    end
  end

  # GET /anet_logs/new
  # GET /anet_logs/new.json
  def new
    @anet_log = AnetLog.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @anet_log }
    end
  end

  # GET /anet_logs/1/edit
  def edit
    @anet_log = AnetLog.find(params[:id])
  end

  # POST /anet_logs
  # POST /anet_logs.json
  def create
    @anet_log = AnetLog.new(params[:anet_log])

    respond_to do |format|
      if @anet_log.save
        format.html { redirect_to @anet_log, notice: 'Anet log was successfully created.' }
        format.json { render json: @anet_log, status: :created, location: @anet_log }
      else
        format.html { render action: "new" }
        format.json { render json: @anet_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /anet_logs/1
  # PUT /anet_logs/1.json
  def update
    @anet_log = AnetLog.find(params[:id])

    respond_to do |format|
      if @anet_log.update_attributes(params[:anet_log])
        format.html { redirect_to @anet_log, notice: 'Anet log was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @anet_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /anet_logs/1
  # DELETE /anet_logs/1.json
  def destroy
    @anet_log = AnetLog.find(params[:id])
    @anet_log.destroy

    respond_to do |format|
      format.html { redirect_to anet_logs_url }
      format.json { head :no_content }
    end
  end
end
