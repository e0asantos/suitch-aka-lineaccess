class SmartwatchesController < ApplicationController
  # GET /smartwatches
  # GET /smartwatches.json
  def index
    @smartwatches = Smartwatch.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @smartwatches }
    end
  end

  # GET /smartwatches/1
  # GET /smartwatches/1.json
  def show
    @smartwatch = Smartwatch.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @smartwatch }
    end
  end

  # GET /smartwatches/new
  # GET /smartwatches/new.json
  def new
    @smartwatch = Smartwatch.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @smartwatch }
    end
  end

  # GET /smartwatches/1/edit
  def edit
    @smartwatch = Smartwatch.find(params[:id])
    render :layout => false
  end

  # POST /smartwatches
  # POST /smartwatches.json
  def create
    @smartwatch = Smartwatch.new(params[:smartwatch])
    if params[:smartwatch]["watch_type"].index("Sony SmartWatch 1")!=nil
      @smartwatch.watch_type=0
    elsif params[:smartwatch]["watch_type"].index("Sony SmartWatch 2")!=nil
      @smartwatch.watch_type=1
    elsif params[:smartwatch]["watch_type"].index("I'm Watch")!=nil
        @smartwatch.watch_type=2
    end
    @smartwatch.sys_user_id=session[:activeUser].id
    respond_to do |format|
      if @smartwatch.save
        format.html { redirect_to "/summaries", notice: 'Smartwatch was successfully created.' }
        format.json { render json: @smartwatch, status: :created, location: @smartwatch }
      else
        format.html { render action: "new" }
        format.json { render json: @smartwatch.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /smartwatches/1
  # PUT /smartwatches/1.json
  def update
    @smartwatch = Smartwatch.find(params[:id])

    respond_to do |format|
      if @smartwatch.update_attributes(params[:smartwatch])
        format.html { redirect_to @smartwatch, notice: 'Smartwatch was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @smartwatch.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /smartwatches/1
  # DELETE /smartwatches/1.json
  def destroy
    @smartwatch = Smartwatch.find(params[:id])
    @smartwatch.destroy

    respond_to do |format|
      format.html { redirect_to smartwatches_url }
      format.json { head :no_content }
    end
  end
end
