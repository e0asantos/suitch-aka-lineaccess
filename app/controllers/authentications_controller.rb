# require '/hacks/lineAccess/app/models/user.rb'
require 'sys_user.rb'
require 'api_token_user_interface.rb'
require 'sys_user_phone.rb'
require 'api_interface.rb'

require 'google/api_client/client_secrets'
require 'google/api_client/auth/installed_app'
require 'securerandom'
require "base64"

require 'uri'
require 'net/http'
require 'net/https'
require 'json'
# require 'sendgrid-ruby'
# include SendGrid

class AuthenticationsController < ApplicationController
  
  # GET /authentications
  # GET /authentications.json
  def index
    puts "______________INDEX___________"
    @authentications = Api_Token_User_Interface.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @authentications }
    end
  end

  # GET /authentications/1
  # GET /authentications/1.json
  def show
    puts "______________SHOW___________"
    @authentication = Api_Token_User_Interface.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @authentication }
    end
  end

  # GET /authentications/new
  # GET /authentications/new.json
  def new
    puts "______________NEW___________"
    @authentication = Api_Token_User_Interface.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @authentication }
    end
  end

  # GET /authentications/1/edit
  def edit
    puts "______________EDIT___________"
    @authentication = Api_Token_User_Interface.find(params[:id])
  end

  # POST /authentications
  # POST /authentications.json
  # def create
  #   puts "______________CREATE___________"
  #   @authentication = Authentication.new(params[:authentication])

  #   respond_to do |format|
  #     if @authentication.save
  #       format.html { redirect_to @authentication, notice: 'Authentication was successfully created.' }
  #       format.json { render json: @authentication, status: :created, location: @authentication }
  #     else
  #       format.html { render action: "new" }
  #       format.json { render json: @authentication.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PUT /authentications/1
  # PUT /authentications/1.json
  def update
    puts "______________UPDATE___________"
    @authentication = Api_Token_User_Interface.find(params[:id])

    respond_to do |format|
      if @authentication.update_attributes(params[:authentication])
        format.html { redirect_to @authentication, notice: 'Authentication was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @authentication.errors, status: :unprocessable_entity }
      end
    end
  end

  

  # DELETE /authentications/1
  # DELETE /authentications/1.json
  def destroy
    puts "______________DESTROY_____________"
    @authentication = Api_Token_User_Interface.find(params[:id])
    @authentication.destroy

    respond_to do |format|
      format.html { redirect_to authentications_url }
      format.json { head :no_content }
    end
  end

  # def failed
  #   redirect_to "/summaries"

  # end
  def create  

    auth = request.env["omniauth.auth"]
    puts "----------auth-from-facebook---------"
    puts auth.credentials.token
    puts auth.uid
    puts auth.info.image
    puts auth.provider
    puts auth

    
    puts session[:activeUser].inspect
    if auth.provider == "facebook"
      
      puts auth.extra.raw_info.email
      #guardamos token
      # 
      # usuario=Sys_User.find_by_user_id(auth.uid)
      # we verify if the user is logged in
      if session[:activeUser]==nil
        #el usuario no ingreso el telefono
        session[:badmix]="wrong"
        redirect_to "/"
        return
      end
      if session[:activeUser] != nil
        usuario = session[:activeUser]
        #crear usuario
        # usuario=Sys_User.new
        # usuario.user_id=auth.uid
        # usuario.name=auth.extra.raw_info.first_name
        # usuario.last_name=auth.extra.raw_info.last_name
        # usuario.email=auth.extra.raw_info.email
        # usuario.avatar=auth.info.image
        # usuario.sys_rol=2
        # usuario.save
        # #guardar telefono
        # if session[:phone]!=nil
          
        
        #   userPhone=SysUserPhone.find_by_number(session[:phone])
        #   if userPhone==nil
        #     #crear objeto telefono
        #     userPhone=SysUserPhone.new
        #     userPhone.sys_phone_type=1
        #     userPhone.number=session[:phone]
        #     userPhone.sys_user=usuario.id
        #     userPhone.save
        #   end
        # end
        #usuario.phonenumber=session[:phone]
        #usuario.token=auth.credentials.token
        

        autenticacion=Api_Token_User_Interface.new
        #buscar el provider
        apiInterface=Api_Interface.find_by_name(auth.provider)
        #autenticacion.provider=auth.provider
        autenticacion.api_interface=apiInterface.id
        autenticacion.token=auth.credentials.token
        autenticacion.sys_user=session[:activeUser].user_id
        autenticacion.save
        puts "***USER CREATED****"
      end
      apiInterface=Api_Interface.where(:name=>auth.provider).last
      puts apiInterface.inspect
      updateAuth=Api_Token_User_Interface.where("api_interface='"+apiInterface.id.to_s+"' and sys_user='"+usuario.id.to_s+"'")
      if updateAuth.length>0
        #tomamos el primero
        firstQuery=updateAuth[0]
        firstQuery.token=auth.credentials.token
        firstQuery.uid = auth.uid
        firstQuery.save
        #acutalizamos token
        puts "***UPDATING FACEBOOK****"
      else
         
        autenticacion=Api_Token_User_Interface.new
        #buscar el provider
        apiInterface=Api_Interface.find_by_name(auth.provider)
        #autenticacion.provider=auth.provider
        autenticacion.api_interface=apiInterface.id
        autenticacion.token=auth.credentials.token
        autenticacion.sys_user=usuario.id
        autenticacion.uid = auth.uid
        autenticacion.save
      end
      #creamos sesion de facebook
      session[:activeUser] = usuario
      puts session[:activeUser].inspect
    elsif auth.provider == "evernote"
      puts "*****EVERNOTE*****"
      #recuperamos usuario de fb y enlazamos ambas cuentas
      apiInterface=Api_Interface.find_by_name(auth.provider)
      autenticacion=Api_Token_User_Interface.new
      autenticacion.api_interface=apiInterface.id
      autenticacion.token=auth.credentials.token
      autenticacion.sys_user=session[:activeUser].id
      autenticacion.save
    elsif auth.provider=="twitter"
      puts "*****TWITTER******"
      apiInterface=Api_Interface.find_by_name(auth.provider)
      autenticacion=Api_Token_User_Interface.new
      autenticacion.api_interface=apiInterface.id
      autenticacion.token=auth.credentials.token
      autenticacion.sys_user=session[:activeUser].id
      autenticacion.token_secret=auth.credentials.secret
      autenticacion.save
    elsif auth.provider=="google_oauth2"
      puts "***** GOOGLE ******"
      apiInterface=Api_Interface.find_by_name("google")
      autenticacion=Api_Token_User_Interface.new
      currentTokens=Api_Token_User_Interface.where(:sys_user=>session[:activeUser].id,:api_interface=>apiInterface.id)
      if currentTokens.length==0
        autenticacion.api_interface=apiInterface.id
        autenticacion.token=auth.credentials.token
        autenticacion.token_secret=auth.credentials.refresh_token
        autenticacion.sys_user=session[:activeUser].id
        autenticacion.expires=auth.credentials.expires_at
        autenticacion.save  
      else
        # just update the key
        autenticacion=currentTokens.last
        autenticacion.token=auth.credentials.token
        autenticacion.expires=auth.credentials.expires_at
        autenticacion.save
      end
      # con los permisos ya podemos poner las suscripciones
      client = Google::APIClient.new(:application_name=>"Suitch glass",:application_version=>"1.0.0")
      client.authorization.client_id = '356453616759.apps.googleusercontent.com'
      client.authorization.client_secret = 'OTWKozU0YTptxA4yqn4AeNuc'
      client.authorization.scope = 'https://www.googleapis.com/auth/glass.timeline'
      calendar = client.discovered_api('mirror', 'v1')
      msession=Hash.new
      msession[:access_token] = autenticacion.token#"ya29.ZwCG5KG_F-KJeCEAAACtzrvW7Qaj3lHVY9pAVJOu-gM3ZOOrtLSTBxipYYefNGKsNbB8kSpLDsBRcexj9QQ"
      msession[:refresh_token] = autenticacion.token_secret
      @authorization ||= (
          auth = client.authorization.dup
          auth.update_token!(msession)
          auth
      )
      result = client.execute(:api_method => calendar.timeline.list,
                              :authorization => @authorization)
      jsonObjeto=JSON.parse(result.body)['items']

      jsonObjeto.each do |singleCard|
        puts singleCard['id']
        client.execute(:api_method => calendar.timeline.delete,
                                                     :parameters => {'id'=>singleCard['id']},
                                     :authorization => @authorization)
      end
      susbscription_item_timeline = calendar.subscriptions.insert.request_schema.new({collection:"timeline",userToken:"user_"+autenticacion.sys_user.to_s,callbackUrl:"https://www.suitch.co/glasses/notify.json",operation:["UPDATE","INSERT","DELETE","REPLY"]})
      susbscription_item_locations = calendar.subscriptions.insert.request_schema.new({collection:"locations",userToken:"user_"+autenticacion.sys_user.to_s,callbackUrl:"https://www.suitch.co/glasses/notify.json",operation:["UPDATE","INSERT","DELETE","REPLY"]})
      result_o = client.execute(:api_method => calendar.subscriptions.insert,
                :body_object=>susbscription_item_timeline,
                              :authorization => @authorization)
      result_t = client.execute(:api_method => calendar.subscriptions.insert,
                :body_object=>susbscription_item_locations,
                              :authorization => @authorization)

      # aqui insertamos la tarjeta de suitch en glass
      timeline_item = calendar.timeline.insert.request_schema.new
      timeline_item.html='<article><figure><img src="http://lineaccess.mx/images/240.png"></figure><section><h1 class="text-large">Suitch glass</h1><p class="text-x-small">Hit reply and say your Suitch command.</p><hr><p class="text-x-small">see the videos at www.suitch.co for more help.</p></section></article>'
      timeline_item.menu_items=[{action: "REPLY"},{action: "CUSTOM"}]
      timeline_item.notification = { 'level' => "DEFAULT" }
      result = client.execute(:api_method => calendar.timeline.insert,
              :body_object=>timeline_item,:authorization => @authorization)
      puts "+++++ google +++++"
    end
    redirect_to "/summaries"
    # render :text => auth.inspect
    puts "----------auth----------"
  end

  def backwards
    result = Hash.new
    backwards_words = ["car","disney","google","amazon","suitch","iot","hello","hi","gps"]
    result["result"]="success"
    result["word"]=backwards_words.shuffle.first
    result["it"]=backwards_words.index(result["word"])
    # if session[:activeUser] != nil
    #   @anet_device = AnetDevice.where(:token=>params[:token],:id_owner=>session[:activeUser].id).last
    #   if @anet_device != nil
    #     sql_status = "select * from anet_logs where arduino_id="+@anet_device.id.to_s+" order by created_at desc limit 1"
    #     @device_status= ActiveRecord::Base.connection.exec_query(sql_status).to_a
    #     @device_status= @device_status[0]
    #     # if @device_status == nil
    #     #     @device_status=Hash.new
    #     #     @device_status["connection_type"] == -1
    #     # end
    #     result["result"] = "success"
    #   end
    # end
    respond_to do |format|
      if result["result"] == "success"
        format.json { render json: result.to_json, status:200}
      else
        format.json { render json: result.to_json, status:401 }
      end
    end
  end

  def put_user_v2
    responseHash = Hash.new
    responseHash["result"] = "error"

    # lets verify if the user is the same on the session
    if session[:activeUser] != nil
      usuario=Sys_User.where(:id=>session[:activeUser].id).last
      usuarioPhone=SysUserPhone.where(:sys_user=>session[:activeUser].id).last
      usuario.name=params[:name]
      # usuario.password=params[:password]
      usuario.last_name=params[:last_name]
      usuario.email=params[:email]
      usuario.company=params[:company]
      usuario.lang=params[:lang]
      # usuario.avatar=auth.info.image
      # usuario.sys_rol=2
      usuario.save
      session[:activeUser] = usuario
      #guardar telefono
      #crear objeto telefono
      # userPhone=SysUserPhone.new
      # userPhone.sys_phone_type=1
      # userPhone.number=params[:phone]
      
      # userPhone.sys_user=usuario.id
      usuarioPhone.number=params[:phone]
      usuarioPhone.save
      responseHash["result"] = "success"
    end
    respond_to do |format|
      if responseHash["result"] == "success"
        format.json { render json: responseHash, status: 200 }
      else
        format.json { render json: responseHash, status: 401 }
      end
    end
  end

  def put_user_pwd_v2
    responseHash = Hash.new
    responseHash["result"] = "error"

    # lets verify if the user is the same on the session
    if session[:activeUser] != nil
      usuario=Sys_User.where(:id=>session[:activeUser].id).last
      usuario.password=params[:password]
      usuario.save
      session[:activeUser] = usuario
      responseHash["result"] = "success"
    end
    respond_to do |format|
      if responseHash["result"] == "success"
        format.json { render json: responseHash, status: 200 }
      else
        format.json { render json: responseHash, status: 401 }
      end
    end
  end

  def post_forgot_v2
    mreponse = Hash.new
    mreponse["status"] = "success"

    # loof for an email
    usuarioEmail=Sys_User.where(:email=>params[:email]).last
    if usuarioEmail != nil
      # send an email witht the password
      uri = URI.parse("http://localhost:3002/mail-notification") 
      header = {'Content-Type'=> 'text/json'}
      https = Net::HTTP.new(uri.host,uri.port)
      # https.use_ssl = true
      req = Net::HTTP::Post.new(uri)
      req['Content-Type'] = 'application/json'
      finalArrayEmail=[]

      finalArrayEmail.push({"email": usuarioEmail.email, "name":usuarioEmail.name })
      # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
      req.body = {recipients:finalArrayEmail,template:"forgot_v1",last_password:usuarioEmail.password}.to_json
      # Send the request
      response = https.request(req)
      puts response.body
    end
    respond_to do |format|
      if mreponse["status"] == "success"
        format.json { render json: mreponse.to_json, status:200}
      else
        format.json { render json: mreponse.to_json, status:401 }
      end
    end
  end

  def signup
    mreponse = Hash.new
    mreponse["status"] = "error"
    mredirect = "/login.html?signup=error"
    # buscar primero el mail y el telefono
    usuarioEmail=Sys_User.where(:email=>params[:email])
    usuarioPhone=SysUserPhone.where("number like '%"+params[:phone]+"%'")
    
    if usuarioEmail.length > 0 or usuarioPhone.length > 0 or params.has_key?("it")==false or params.has_key?("backwards")==false
      puts "HACKING_ATTEMPT"
    else
      # lets verify if the backwards is ok
      backwards_words = ["car","disney","google","amazon","suitch","iot","hello","hi","gps"]
      my_reverse=backwards_words[params[:it]].reverse
      if my_reverse == params[:backwards]
        #crear usuario
        usuario=Sys_User.new
        usuario.user_id=SecureRandom.uuid
        usuario.name=params[:name]
        usuario.password=params[:password]
        usuario.last_name=params[:lastname]
        usuario.email=params[:email]
        # usuario.avatar=auth.info.image
        usuario.sys_rol=2
        usuario.save
        session[:activeUser] = usuario
        #guardar telefono
        #crear objeto telefono
        userPhone=SysUserPhone.new
        userPhone.sys_phone_type=1
        userPhone.number=params[:phone]
        
        userPhone.sys_user=usuario.id
        userPhone.save
        mreponse["status"] = "created"
        session[:phone] = params[:phone]
        # at this point we should send an email

        uri = URI.parse("http://localhost:3002/mail-notification") 
        header = {'Content-Type'=> 'text/json'}
        https = Net::HTTP.new(uri.host,uri.port)
        # https.use_ssl = true
        req = Net::HTTP::Post.new(uri)
        req['Content-Type'] = 'application/json'
        finalArrayEmail=[]

        finalArrayEmail.push({"email": usuario.email, "name":usuario.name })
        # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
        req.body = {recipients:finalArrayEmail,template:"welcome_v1"}.to_json
        # Send the request
        response = https.request(req)
        puts response.body
      end
    end
    if session[:phone]==nil and usuarioEmail==nil
      #el usuario no ingreso el telefono
      session[:badmix]="wrong"
      # redirect_to "/"
      # return
    end
    respond_to do |format|
      if mreponse["status"] == "created"
        format.json { render json: mreponse.to_json, status:200}
      else
        format.json { render json: mreponse.to_json, status:401 }
      end
    end
  end

  def random_test2_v2
    # clientMail = SendGrid::API.new(api_key: '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')
    #   singleMail = SendGrid::Mail.new
    #   singleMail.from = Email.new(email: 'noreply@consultware.mx')
    #   singleMail.subject = 'Suitch - Connected'
    #   personalization = Personalization.new
    #   finalArrayEmail=[]
    #   Sys_User.where(:id=>33).each do |singleEmail|
    #     personalization.to=Email.new(email: singleEmail.email, name: "Suitch usuario")
    #   end
    #   singleMail.personalizations=personalization
    #   content = SendGrid::Content.new(type: 'text/plain', value: "msgToSend")
    #   singleMail.contents = content;
    #   response=clientMail.client.mail._('send').post(request_body: singleMail.to_json)
    #   puts response.status_code
    #   puts response.body
    #   puts response.headers

    #   respond_to do |format|
    #     format.json { render json: "ok".to_json, status:200}
    #   end
    messageData = Hash.new
    messageData["subject"]="Welcome"
    messageData["from"]=Hash.new
    messageData["from"]["email"] = "noreply@suitch.network"
    to_addresses=Hash.new
    to_addresses["email"] = "andres.santos@consultware.mx"
    messageData["to"]=[to_addresses]
    recipients = Hash.new
    recipients["email"]="andres.santos@consultware.mx"
    recipients["substitutions"]=[
      {
        "var":"name", "value":"Your Name"},
        {
          "var": "account.name",
          "value": "account---name"
        },
        {
          "var": "support_email",
          "value": "noreply@suitch.network"
        }
      ]
    messageData["variables"] = [recipients]
    messageData["template_id"]="ynrw7gy5pk42k8e3"
    body = messageData.to_json
    uri = URI.parse("https://api.mailersend.com/v1/email")
    https = Net::HTTP.new(uri.host,uri.port)
    
    https.use_ssl = false
    req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' =>'application/json'})
    req['Authorization'] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYjM1NGZkOTA5ZWY1NzBkNjJjNzA4ZDQzYmJmNDUwNTdjMWYzNzFlNDZlNmVhODg5YTU3MzliNjFiYzgxMTM4YzFkYjA2N2FkYWY4OGQyMjIiLCJpYXQiOjE2NDAwMzI5NzcuODgyMzY0LCJuYmYiOjE2NDAwMzI5NzcuODgyMzY5LCJleHAiOjQ3OTU3MDY1NzcuNjg3NDQ0LCJzdWIiOiIxNzU5OSIsInNjb3BlcyI6WyJlbWFpbF9mdWxsIiwiZG9tYWluc19mdWxsIiwiYWN0aXZpdHlfZnVsbCIsImFuYWx5dGljc19mdWxsIiwidG9rZW5zX2Z1bGwiLCJ3ZWJob29rc19mdWxsIiwidGVtcGxhdGVzX2Z1bGwiLCJzdXBwcmVzc2lvbnNfZnVsbCJdfQ.N5UC7mq41MFFTnvRUyN_cFzt0cGmS-koSBScCsPxufwhZdMBn0H1YPZDONNbrb70DjM_z112icRzPdPLUgFEWrAMHGK4g9_qzMzPR0AHTAROP9ieuMjBkTQFBxJgLNeUe0bItCUTVHVChotcjhGYm8eH4K4NMFb2GQ_jHw-_lKaw7YEyPNxEXUdNM98sYD2mdoDEUj0420P6QlardJYq5UDuU1ZpWSrwUZ_PBMgJSbp7EvHVLoVzOmlNQ5u4p9WrWObuSIOYrQ4iuzFxCw1yNjLwIrw88_p_gGKaQrKJesoyK7eNXFbk1NyASlEHoPwCDHnlIlQx7R64K1YERe3aBzJMI9sbY1VKZ0ClN4svwcjs1Y__5dXqTv4UOH1uLvFB1AZ9fecPwvV5hRp8hqhii-xr9crspsdGABoMLzEvf2Dq_Jz19K6q-pl2obBctSgsCSk0c5QvzNVcnKGWLOLsJ1MRWcF9y11kxMDD9-BZss7F_pi1URwpcAvtDge3K1JpXXbikyeJGQVFp4IONcA5PaBPdj--PAl5GfG35RXqYFZOjMJvoL5EgXcBbC8ZCLo94FI9sMvqQo6xZHcSgOHqn2OpexHV95ZiGrSQYytB8JI8yOKOx7Gxf5urgad0adPVb5E70DcMfAzus1A-FOJUSdM385WJlMLcS76h3wLvXHE"

    req.body = messageData.to_json
    
    res = https.request(req)
    puts "Response #{res.code} #{res.message}: #{res.body}"
      respond_to do |format|
        format.json { render json: res.message.to_json, status:200}
      end
  end

  def google_tokenize
    # this is the second step to be executed from GOOGLE assistant
    # we receive the CODE and this code is the user_id, a base64 id that will be used
    # to identify the user transactions from the google home
    returnObj = Hash.new
    rstatus="success"
    logger.debug "=====TOKENIZE"
    secondsInDay = 86400
    if params[:grant_type] == "authorization_code"
      # create a token specific for google
      user_to_token=Sys_User.where(:user_id=>Base64.decode64(params[:code])).last
      if params[:redirect_uri].index("amazon")!=nil
        user_to_token.alexa_oauth=SecureRandom.uuid
      else
        user_to_token.google_oauth=SecureRandom.uuid
      end
      user_to_token.save
      logger.debug "=====authorization_code"
      returnObj["token_type"] = "Bearer"
      if params[:redirect_uri].index("amazon")!=nil
        returnObj["access_token"] = user_to_token.alexa_oauth
        returnObj["refresh_token"] = Base64.encode64(user_to_token.alexa_oauth)
      else
        returnObj["access_token"] = user_to_token.google_oauth
        returnObj["refresh_token"] = Base64.encode64(user_to_token.google_oauth)
      end
      returnObj["expires_in"] = secondsInDay * 365
      logger.debug returnObj.inspect
    elsif params[:grant_type] == "refresh_token"
      # verify if the request token is valid
      # google >>>> Parameters: {"grant_type"=>"refresh_token", "refresh_token"=>"invalidRefreshToken", "client_id"=>"0873953678", "client_secret"=>"7500611540"}
      if params[:refresh_token]!= "invalidRefreshToken"
        decoded = Base64.decode64(params[:refresh_token])
        usuarioEmail=Sys_User.where(:google_oauth=>decoded).last
        if usuarioEmail==nil
          usuarioEmail=Sys_User.where(:alexa_oauth=>decoded).last
        end
        if usuarioEmail!=nil
          if params.has_key?("redirect_uri")==true and params[:redirect_uri].index("amazon")!=nil
            usuarioEmail.alexa_oauth=SecureRandom.uuid
            returnObj["access_token"] = usuarioEmail.alexa_oauth
          else
            usuarioEmail.google_oauth=SecureRandom.uuid
            returnObj["access_token"] = usuarioEmail.google_oauth
          end
          usuarioEmail.save
          returnObj["token_type"] = "Bearer"
          returnObj["expires_in"] = secondsInDay * 365
        else
          rstatus="error"
          returnObj["error"]="invalid_grant"
          returnObj["error_description"]="The provided access grant is invalid, expired, or revoked."
        end
      else
        rstatus="error"
        returnObj["error"]="invalid_grant"
        returnObj["error_description"]="The provided access grant is invalid, expired, or revoked."
      end
    else
      logger.debug "=====TOKENIZE=ERROR"
      returnObj["token_type"] = "Bearer"
      returnObj["access_token"] = Base64.decode64(params[:code])
      returnObj["expires_in"] = secondsInDay * 365
    end
    
    # logger.debug Base64.decode64(params[:_token])
    logger.debug "=====TOKENIZE"
    if rstatus=="error"
      respond_to do |format|
        format.html { render json:  returnObj.to_json,status: 401 }
        format.json { render json:  returnObj.to_json,status: 401 }
      end
    else
      respond_to do |format|
        format.html { render json:  returnObj.to_json }
        format.json { render json:  returnObj.to_json}
      end
    end
  end

  def alexa_tokenize
    # this is the second step to be executed from GOOGLE assistant
    # we receive the CODE and this code is the user_id, a base64 id that will be used
    # to identify the user transactions from the google home
    returnObj = Hash.new
    rstatus="success"
    logger.debug "=====TOKENIZE"
    secondsInDay = 86400
    if params[:grant_type] == "authorization_code"
      # create a token specific for google
      user_to_token=Sys_User.where(:user_id=>Base64.decode64(params[:code])).last
      user_to_token.alexa_oauth=SecureRandom.uuid
      user_to_token.save
      logger.debug "=====authorization_code"
      returnObj["token_type"] = "Bearer"
      returnObj["access_token"] = user_to_token.alexa_oauth
      returnObj["refresh_token"] = Base64.encode64(user_to_token.alexa_oauth)[0..-2]
      returnObj["expires_in"] = secondsInDay * 365
      logger.debug returnObj.inspect
    elsif params[:grant_type] == "refresh_token"
      # verify if the request token is valid
      # google >>>> Parameters: {"grant_type"=>"refresh_token", "refresh_token"=>"invalidRefreshToken", "client_id"=>"0873953678", "client_secret"=>"7500611540"}
      if params[:refresh_token]!= "invalidRefreshToken"
        decoded = Base64.decode64(params[:refresh_token])
        usuarioEmail=Sys_User.where(:alexa_oauth=>decoded).last
        if usuarioEmail!=nil
          usuarioEmail.alexa_oauth=SecureRandom.uuid
          usuarioEmail.save
          returnObj["token_type"] = "Bearer"
          returnObj["access_token"] = usuarioEmail.alexa_oauth
          returnObj["expires_in"] = secondsInDay * 365
        else
          rstatus="error"
          returnObj["error"]="invalid_grant"
          returnObj["error_description"]="The provided access grant is invalid, expired, or revoked."
        end
      else
        rstatus="error"
        returnObj["error"]="invalid_grant"
        returnObj["error_description"]="The provided access grant is invalid, expired, or revoked."
      end
    else
      logger.debug "=====TOKENIZE=ERROR"
      returnObj["token_type"] = "Bearer"
      returnObj["access_token"] = Base64.decode64(params[:code])
      returnObj["expires_in"] = secondsInDay * 365
    end
    
    # logger.debug Base64.decode64(params[:_token])
    logger.debug "=====TOKENIZE"
    if rstatus=="error"
      respond_to do |format|
        format.html { render json:  returnObj.to_json,status: 401 }
        format.json { render json:  returnObj.to_json,status: 401 }
      end
    else
      respond_to do |format|
        format.html { render json:  returnObj.to_json }
        format.json { render json:  returnObj.to_json}
      end
    end
  end

  def google_auth
    # this is the first step to run from the google assistant
    # here we received the first handshake to tell google what is the CODE of the user
    # the CODE is a transaction exchange to receive a token, aftwerwards
    session[:assistant_url] = params[:redirect_uri]+'?state='+params[:state]
    redirect_to "/login?token="+Base64.encode64(params[:redirect_uri]+'?state='+params[:state])
  end

  def alexa_auth
    # this is the first step to run from the alexa account linking
    # here we received the first handshake to tell google what is the CODE of the user
    # the CODE is a transaction exchange to receive a token, aftwerwards
    session[:assistant_url] = params[:redirect_uri]+'?state='+params[:state]+'&scope=suitch-home'
    # redirect_to "/login?token="+Base64.encode64(params[:redirect_uri]+'?code='+SecureRandom.hex+'&state='+params[:state])
    redirect_to "/login?token="+Base64.encode64(params[:redirect_uri]+'?state='+params[:state]+'&scope=suitch-home&assist=alexa')
  end

  def messenger_auth
    # this is the first step to run from the webhook to the facebook messenger
    verify_token = "321456123"
    challenge = ""
    vars = request.query_parameters
    if vars['hub.verify_token'] == verify_token
      challenge = vars["hub.challenge"]
    end
    
    respond_to do |format|
      format.json { render json:  challenge}
    end
  end

  def receivedMessage(event)
    logger.debug event["message"].inspect
    
  
  senderID = event["sender"]["id"];
  recipientID = event["recipient"]["id"];
  timeOfMessage = event["timestamp"];
  message = event["message"];
  
  messageId = message["mid"];
  messageText = message["text"];
  # messageAttachments = message.attachments;
  greetingsen = ["hey", "hello", "whats up",  "sup", "hi", "whats'up", "whatapp"]
  askinfoen = ["info", "information", "help"]
  greetingses = ["hola", "holi", "ola", "qué onda", "qué hay", "que tal", "qué tal", "buen día", "buen dia", "buen", "buenas", "buenos"]
  askinfoes = ["informacion", "ayuda", "pregunta", "duda"]
  if askinfoen.index(messageText) != nil
    sendTextMessage(senderID, "Hello, the Suitch network can use messenger to notify you when your IoT devices connect or disconnect, for this you must create an account at https://www.suitch.network and send me the union token. From there on, I will notify you when your devices connect to the network.");
  elsif greetingsen.index(messageText) != nil
    sendTextMessage(senderID, "Hello, to register and receive messages from your connected devices, just send me your union token that appears in the 'Apps for Facebook' window on the www.suitch.network site");
  elsif askinfoes.index(messageText) != nil
    sendTextMessage(senderID, "Hola, la red de Suitch puede utilizar messenger para notificarte cuando tus dispositivos IoT se conecten o desconecten, para ello deberás crear una cuenta en https://www.suitch.network y enviarme el token de unión.");
  elsif greetingses.index(messageText) != nil
    sendTextMessage(senderID, "Hola, para registrarte y recibir mensajes de tus dispositivos conectados, solo debes enviarme tu token de unión que aparece en la ventana de 'Apps para facebook' en el sitio de www.suitch.network");
  elsif messageText.index("anet_")!= nil and messageText.length < 40
    pseudo_key = messageText[5..-1]
    find_user = Sys_User.where("user_id like ?","#{pseudo_key}%").last
    if find_user != nil and pseudo_key != "" and pseudo_key != " " and pseudo_key != nil and pseudo_key.index("\n") == nil and pseudo_key.index("\r") == nil
      sendTextMessage(senderID, find_user.name+", You just registered correctly the union token, now the IoT devices in suitch configured to send messages to Facebook, will notify you here.\nAcabas de registrar correctamente el token de unión, ahora los dispositivos que hayas configurado para recibir notificaciones de facebook, las recibirás por este chat :D");
      token_facebook=Api_Token_User_Interface.new
      token_facebook.sys_user=find_user.id
      token_facebook.api_interface=16
      token_facebook.uid = senderID
      token_facebook.save
    else 
        sendTextMessage(senderID, "There seems to be a problem, the union token doesnt look to be valid, I am still unable to identify your account on Suitch.\nTengo un problema, el token de unión no es valido, no he podido unir tu cuenta :(");
    end
  else
    token_facebook=Api_Token_User_Interface.where(:uid => senderID).last
    if token_facebook == nil
      sendTextMessage(senderID, "Looks like you haven't given me the union token from your Suitch account.\nParece que aún no me has compartido tu token de union de tu cuenta Suitch.");
    else
      sendTextMessage(senderID, "Yay! we are linked, just remember at this moment I will only notify you on events generated from the IoT devices from Suitch.\n :D Estamos unidos, recuerda que solo te notificaré de cambios en tus dispositivos IoT que hayas configurado con notificaciones de facebook.")
    end
  end
end

def sendTextMessage(recipientId, messageText) 
  messageData = Hash.new
  messageData["recipient"]=Hash.new
  messageData["recipient"]["id"] = recipientId
  messageData["message"]=Hash.new
  messageData["message"]["text"] = messageText
  messageData["messaging_type"] = "RESPONSE"
  callSendAPI(messageData);
  end
  def callSendAPI(messageData) 
    body = messageData.to_json
    uri = URI.parse("https://graph.facebook.com/v4.0/me/messages")
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    logger.debug uri.path
    req = Net::HTTP::Post.new(uri.path+"?access_token=EAAFFJdrXiPABAFB3a9U1ZAqcZC7TxF3smIPGtAbN8ZBXgO5EQZA4BcdAacdFXNW8IrhUE6weDZA8rVnUHBLvnjeLa4Y4DWk0ASg7fUcRm6JWXPyR5E6SN2i5NVEHbRPWKPIaKTfkLhGlvg0KN19SQFwtFvKKOnwJl3x3ZAEhjVQrdt0YOu0pMl", initheader = {'Content-Type' =>'application/json'})
    req.body = messageData.to_json
    
    res = https.request(req)
    logger.debug "Response #{res.code} #{res.message}: #{res.body}"
   
  end
  def messenger_messages
    # this is the first step to run from the webhook to the facebook messenger
    logger.debug params
    if params[:object] == "page"
      params[:entry].each do |singleMessage|
        logger.debug singleMessage
        pageID = singleMessage["id"];
        timeOfEvent = singleMessage["time"];

        singleMessage["messaging"].each do |msg|
          if msg["message"]
            receivedMessage(msg);
          else
            logger.debug "Webhook received unknown event: "
            logger.debug event.inspect
          end
        end
      end
    end
    
    respond_to do |format|
      format.json { render json:  "ok"}
    end
  end

  def alexa_poc
    # this is the first step to run from the google assistant
    # here we received the first handshake to tell google what is the CODE of the user
    # the CODE is a transaction exchange to receive a token, aftwerwards
    respond_to do |format|
      format.json { render json:  "OK".to_json}
    end
  end

  def internal_token
    objeto = Hash.new
    objeto["token"] = form_authenticity_token
    respond_to do |format|
      format.html { redirect_to authentications_url }
      format.json { render json:  objeto.to_json}
    end
  end

  def webLogin
    reponse = Hash.new
    reponse["status"] = "error"
    mredirect = "/login.html?login=error"
    # buscar primero el mail y el telefono
    usuarioEmail=Sys_User.where(:email=>params[:email], :password=>params[:password]).last
    

    if usuarioEmail != nil
      session[:activeUser] = usuarioEmail
      session[:phone] = SysUserPhone.find_by_sys_user(usuarioEmail.id)
      reponse["status"] = "success"
      logger.debug session[:assistant_url]
      if params[:_token] != nil and params[:_token] != ""
        logger.debug "=====ASSISTANT>>>>"
        logger.debug "          _token>>>>"
        logger.debug Base64.decode64(params[:_token])
        logger.debug "          _token<<<<"
        logger.debug Base64.decode64(params[:_token])+"&code="+Base64.encode64(usuarioEmail.user_id)
        logger.debug "=====ASSISTANT<<<<"
        redirect_to Base64.decode64(params[:_token])+"&code="+Base64.encode64(usuarioEmail.user_id)
        return
      end
      mredirect = "/summaries"
    end
    
    respond_to do |format|
      format.html { redirect_to mredirect }
      format.json { redirect_to mredirect }
    end
  end

  def get_user_v2
    myuser = nil
    if session[:activeUser] != nil
      myuser = Sys_User.select("name,last_name,user_id,email,avatar,lang,id").where(:id=>session[:activeUser].id).last
      myuser["token"]=form_authenticity_token
    end
    respond_to do |format|
      format.html { render json: myuser.to_json }
      format.json { render json: myuser.to_json }
    end
  end

  def post_avatar_v2
    result = Hash.new
    result["result"] = "error"
    logger.warn("session>>>> ")
    logger.warn(session[:activeUser].inspect)
    logger.warn("file>>>> ")
    logger.warn(params[:file].inspect)
    if session[:activeUser] != nil && params[:file] != nil
      result["result"] = "success"
      if session[:activeUser].avatar!=nil
        File.delete('/home/suitch/public/avatar/'+session[:activeUser].avatar) if File.exist?('/home/suitch/public/avatar/'+session[:activeUser].avatar)
      end
      image_to_create=SecureRandom.uuid+"."+params[:file].original_filename.split(".")[1]
      # once removed the old one, create the new one
      path_avatar = File.join(Rails.root + "public/avatar/", image_to_create)
      File.open(path_avatar, "w+b") { |f| f.write(params[:file].read) }
      thisUser=Sys_User.find_by_id(session[:activeUser].id)
      thisUser.avatar=image_to_create
      thisUser.save
      session[:activeUser] = thisUser
    end
  
    respond_to do |format|
      if result["result"] == "success"
        format.json { render json: result.to_json, status:200}
      else
        format.json { render json: result.to_json, status:401 }
      end
    end
  end

  def post_assistant_link_v2
    # params for this API
    # assistant in string format alexa, accurite, google etc
    # code in string format, it would normally be a hash code

    reponse = Hash.new
    reponse["status"] = "error"
    if session[:activeUser] != nil
      # thisUser=Sys_User.find_by_id(session[:activeUser].id)
      thisUser=Sys_User.where(:id=>session[:activeUser].id).last
      if params[:assistant] == "nest"
        thisUser.nest_channel=params[:nest_code]
        thisUser.save 
        reponse["status"] = "success"
        session[:activeUser]=thisUser
      end
    end

    if reponse["status"] === "success"
      respond_to do |format|
        format.html { render json:  session[:activeUser].to_json }
        format.json { render json:  session[:activeUser].to_json }
      end
    else
      respond_to do |format|
        format.html { render :json => {:response => 'Unable to authenticate' },:status => 401  }
        format.json { render :json => {:response => 'Unable to authenticate' },:status => 401  }
      end
    end
  end

  def get_assistant_link_v2
    # params for this API
    # assistant in string format alexa, accurite, google etc
    # code in string format, it would normally be a hash code

    reponse = Hash.new
    reponse["status"] = "error"
    if session[:activeUser] != nil
      thisUser=Sys_User.find_by_id(session[:activeUser].id)
      reponse["alexa"]=thisUser.alexa_oauth
      reponse["nest"]=thisUser.nest_channel
      reponse["google"]=thisUser.google_oauth
      reponse["status"] = "success"
    end

    if reponse["status"] === "success"
      respond_to do |format|
        format.html { render json:  reponse.to_json }
        format.json { render json:  reponse.to_json }
      end
    else
      respond_to do |format|
        format.html { render :json => {:response => 'Unable to authenticate' },:status => 401  }
        format.json { render :json => {:response => 'Unable to authenticate' },:status => 401  }
      end
    end
  end

  def loginv2
    reponse = Hash.new
    reponse["status"] = "error"
    mredirect = "/login.html?login=error"
    # buscar primero el mail y el telefono
    usuarioEmail=Sys_User.select("name,last_name,user_id,email,avatar,lang,company").where(:email=>params[:email], :password=>params[:password]).last
    sessionResponse = Sys_User.where(:email=>params[:email], :password=>params[:password]).last

    if usuarioEmail != nil
      session[:activeUser] = sessionResponse
      session[:phone] = SysUserPhone.find_by_sys_user(sessionResponse.id)
      reponse["status"] = "success"
      usuarioEmail["token"]=form_authenticity_token
      usuarioEmail["phone"]=session[:phone].number
      logger.debug session[:assistant_url]
      if params[:_token] != nil and params[:_token] != ""
        logger.debug "=====ASSISTANT"
        logger.debug Base64.decode64(params[:_token])
        redirect_to Base64.decode64(params[:_token])+"&code="+Base64.encode64(usuarioEmail.user_id)
        return
        logger.debug "=====ASSISTANT"
      end
    end
    if reponse["status"] === "success"
      respond_to do |format|
        format.html { render json:  usuarioEmail.to_json }
        format.json { render json:  usuarioEmail.to_json }
      end
    else
      respond_to do |format|
        format.html { render :json => {:response => 'Unable to authenticate' },:status => 401  }
        format.json { render :json => {:response => 'Unable to authenticate' },:status => 401  }
      end
    end
  end
end
