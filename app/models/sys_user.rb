class Sys_User < ActiveRecord::Base
  has_many :geo_settings
  attr_accessible :id,:sys_rol,:name,:last_name,:user,:password,:removed,:user_id,:email,:avatar,:created_at,:updated_at,:lang,:lat,:lon, :telegram_id, :fb_id, :company,:nest_channel,:alexa_oauth,:google_oauth
  
end