class SystemActivity < ActiveRecord::Base
  attr_accessible :command, :device_token, :raw_content, :value, :virtual_assistant, :anet_device_id
  belongs_to :anet_device
end
