class AnetDevice < ActiveRecord::Base
  attr_accessible :description, :id, :id_owner, :is_public, :location, :object, :secret_token, :token,:likes,:cliente, :version
  attr_accessible :notify_hangout, :notify_fb, :notify_email, :notify_twitter, :notify_fbinbox,:mip, :notify_telegram
  has_many :system_activities
end
