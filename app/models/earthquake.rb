class Earthquake < ActiveRecord::Base
  attr_accessible :id, :latitude, :location, :longitude, :magnitude, :rawData, :reportedTime
end
