class Jarvix_Personality < ActiveRecord::Base
	attr_accessible :id,:my_name,:my_status,:my_description,:external_description,:my_property,:my_dates,:my_noun,:sex,:last_login,:updated_at
	has_many :jarvix_conversation_logs
	has_many :custom_objects
	has_many :jarvix_custom_properties

	
end