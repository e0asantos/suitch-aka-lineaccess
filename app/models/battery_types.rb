class BatteryTypes < ActiveRecord::Base
  attr_accessible :capacity, :days, :device_type, :name, :voltage
end
