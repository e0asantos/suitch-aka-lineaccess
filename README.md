# README #
1) correr servidor de arduinoNetwork
2) correr sitio con nginx-mysql
3) correr mqtt con mqtt-vpn
4) correr clockwork para DHCP
5) correr google assistant/ forever
6) correr NGINX con phussion passenger en la ubicacion adicional creada en /opt/nginx/
7) servidor en tiempo real para distribuir a los clientes los datos como las propiedades (temperature, humedad, etc), este es un servidor en nodejs


Run this server with PUMA
puma -b tcp://0.0.0.0:80 -b 'ssl://0.0.0.0:443?key=/etc/letsencrypt/live/suitch.network/privkey.pem&cert=/etc/letsencrypt/live/suitch.network/fullchain.pem' -d -w 5 -t 16:16

Para ejecutar un comando en un dispositivo hardware usamos

****POST 
**https://suitch.network/metric/execJSON
parametros

* supertoken es un token que se usa para ejecutar desde HTTPS
* token es el UID identificador del hardware
* command es el comando q se va a ejecutar

Parametros de prueba
****supertoken:yub89s34
****token:heg5u19a
****command:buyProduct-45

ejemplo
https://suitch.network/metric/execJSON?supertoken=123asd&token=qwe789&command=buyProduct-3

# TUNNEL por MQTT #
Pimero hay que instalar Mosquitto y tener el puerto 1883 abierto por default

# ejecutar en modo background el tunnel #

sudo ./mqtt_vpn -i mq0 -a 10.0.1.1 -b tcp://localhost.org:1883 -k secret -d

# Iniciar servidor de Suitch #

ruby arduioNetworkDaemon.rb start

# Iniciar servidor que controla el DHCP para el tunnel #

clockworkd -c cronJobDHCP.rb start

Para Alexa no hay que hacer nada, las APIs estan conectadas en AWS

Para Google home hay que iniciar un servidor en un reverse proxy

el g.suitch.network, corresponde a un proceso para iniciar google home

´´´
server {
    server_name suitch.network www.suitch.network;

    # Tell Nginx and Passenger where your app's 'public' directory is
    root /home/suitch/public;

    # Turn on Passenger
    passenger_enabled on;
    passenger_ruby /usr/local/rvm/gems/ruby-2.2.10/wrappers/ruby;
    passenger_app_env development;
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/suitch.network/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/suitch.network/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot


}
server {
    server_name g.suitch.network;

    # Tell Nginx and Passenger where your app's 'public' directory is
    # Turn on Passenger
    
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/suitch.network/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/suitch.network/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    location / {
        proxy_set_header   X-Forwarded-For $remote_addr;
        proxy_set_header   Host $http_host;
        proxy_pass         http://127.0.0.1:8081;
        proxy_ssl_verify              off;
    }

}
server {
    server_name s.suitch.network;

    # Tell Nginx and Passenger where your app's 'public' directory is
    # Turn on Passenger
    
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/suitch.network/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/suitch.network/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    location / {
        proxy_set_header   X-Forwarded-For $remote_addr;
        proxy_set_header   Host $http_host;
        proxy_pass         http://127.0.0.1:3001;
        proxy_ssl_verify              off;
    }

}
server {
    server_name f.suitch.network;

    # Tell Nginx and Passenger where your app's 'public' directory is
    # Turn on Passenger
    # TCP/IP access
    mysql_host 127.0.0.1;

    # unix socket access (default)
    # mysql_host localhost;

    mysql_user andres;
    mysql_password Alambre01..;
    mysql_port 3306;

    mysql_database lineAccess;
    mysql_connections 32;

    mysql_charset utf8;

    # # this turns on multiple statements support
    # # and is REQUIRED when calling stored procedures
    mysql_multi on;

    mtask_stack 65536;
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/suitch.network/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/suitch.network/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    location /vpn_tunnel {
        mysql_escape $escaped_name $arg_name;
        # proxy_set_header   X-Forwarded-For $remote_addr;
        # proxy_set_header   Host $http_host;
        # proxy_pass         http://10.0.1.2:8000;
        # proxy_ssl_verify              off;
        mysql_query "SELECT ip FROM sys_dhcps WHERE token='$escaped_name'";
    }

    location / {
        mysql_escape $escaped_name $arg_name;

        mysql_subrequest /vpn_tunnel?name=$arg_name $vpn_ip;
        proxy_set_header   X-Forwarded-For $remote_addr;
        proxy_set_header   Host $http_host;
        proxy_pass         http://$vpn_ip:8000;
        proxy_ssl_verify              off;
    }
    # mysql_query "SELECT description FROM anet_devices WHERE id=9";
    # location / {
    #     proxy_set_header   X-Forwarded-For $remote_addr;
    #     proxy_set_header   Host $http_host;
    #     proxy_pass         http://127.0.0.1:3001;
    #     proxy_ssl_verify              off;
    # }
    


}
server {
    server_name p2p.suitch.network;

    # Tell Nginx and Passenger where your app's 'public' directory is
    # Turn on Passenger
    # TCP/IP access
    mysql_host 127.0.0.1;

    # unix socket access (default)
    # mysql_host localhost;

    mysql_user andres;
    mysql_password Alambre01..;
    mysql_port 3306;

    mysql_database lineAccess;
    mysql_connections 32;

    mysql_charset utf8;

    # # this turns on multiple statements support
    # # and is REQUIRED when calling stored procedures
    mysql_multi on;

    mtask_stack 65536;
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/suitch.network/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/suitch.network/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    location /vpn_tunnel {
        mysql_escape $escaped_name $arg_name;
        # proxy_set_header   X-Forwarded-For $remote_addr;
        # proxy_set_header   Host $http_host;
        # proxy_pass         http://10.0.1.2:8000;
        # proxy_ssl_verify              off;
        mysql_query "SELECT ip FROM sys_dhcps WHERE token='$escaped_name'";
    }

    location / {
        mysql_escape $escaped_name $arg_name;

        mysql_subrequest /vpn_tunnel?name=$arg_name $vpn_ip;
        proxy_set_header   X-Forwarded-For $remote_addr;
        proxy_set_header   Host $http_host;
        proxy_pass         http://$vpn_ip:8000;
        proxy_ssl_verify              off;
    }
    # mysql_query "SELECT description FROM anet_devices WHERE id=9";
    # location / {
    #     proxy_set_header   X-Forwarded-For $remote_addr;
    #     proxy_set_header   Host $http_host;
    #     proxy_pass         http://127.0.0.1:3001;
    #     proxy_ssl_verify              off;
    # }
    


}
server {
    if ($host = www.suitch.network) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    if ($host = suitch.network) {
        return 301 https://$host$request_uri;
    } # managed by Certbot

    if ($host = g.suitch.network) {
        return 301 https://g.suitch.network;
    } # managed by Certbot

    if ($host = s.suitch.network) {
        return 301 https://s.suitch.network;
    } # managed by Certbot

    if ($host = f.suitch.network) {
        return 301 https://f.suitch.network;
    } # managed by Certbot
    if ($host = p2p.suitch.network) {
        return 301 https://f.suitch.network;
    } # managed by Certbot

    listen 80;
    server_name www.suitch.network suitch.network g.suitch.network f.suitch.network s.suitch.network p2p.suitch.network;
    return 404; # managed by Certbot
}

´´´

Hay un servidor adicional para controlar los procesos desastres naturales, en cronJosb

#NGINX conf
Nginx tiene una configuracion especial para poder ejecutar queries en el archivo conf, se puede bajar de acá
https://github.com/arut/nginx-mysql-module
Se debe descargar el source de nginx y en la configuracion se puede configurar los dos modulos que hacen falta, se debe apagar el nginx que se controla con "sudo service nginx", ubicado en /usr/sbin
Para acceder a un tunnel por URL se debe hacer de la siguiente forma
https://p2p.suitch.network/p2p?name=7adfa4c5

se ejecuto el comando passenger-install-nginx-module para actualizar nginx, originalmente nginx se installa en  /usr/sbin/nginx (este debe detenerse) e iniciar solo el de [/opt/nginx]

The Nginx configuration file (/opt/nginx/conf/nginx.conf)
must contain the correct configuration options in order for Phusion Passenger
to function correctly.

This installer has already modified the configuration file for you! The
following configuration snippet was inserted:

  http {
      ...
      passenger_root /usr/lib/ruby/vendor_ruby/phusion_passenger/locations.ini;
      passenger_ruby /usr/local/rvm/gems/ruby-2.2.10/wrappers/ruby;
      ...
  }

#certbot
Para los certificados de seguridad usar certbot, hay que tener cuidado al instalarlos por que el nginx
modificado va a requerir que no tengan referencias de mysql

# Real time props NODEJS
el servidor esta en la carpeta de /home/notification-server, 22-May-2023 se hace cambio para evitar procesar los mensajes que no tengan user_channel