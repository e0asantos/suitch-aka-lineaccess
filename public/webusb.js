const requestOptions = {
  // Filter on devices with the Arduino USB vendor ID.
  filters: [{ vendorId: 0x1a86 }],
};
var mydevice;
// Request an Arduino from the user.
var port =  navigator.serial.requestPort(requestOptions).then(device => {
  console.log(device);
  // mydevice = device;
  device.open({ baudrate: 115200 }).then(connection => {
    var reader = device.readable.getReader();
    while (true) {
      const {done, data} = reader.read();
      if (done) break;
      console.log(data);
    }
  }).catch(error => console.log(error));
}).catch(error => console.log(error));

// Open and begin reading.
// port.open({ baudrate: 115200 });
// var reader = port.in.getReader();
// while (true) {
//   const {done, data} = reader.read();
//   if (done) break;
//   console.log(data);
// }