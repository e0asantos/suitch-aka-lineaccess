new Chartist.Line('.ct-chart', {
    labels: ['First quarter of the year',
'Second quarter of the year',
'Third quarter of the year',
'Fourth quarter of the year'],
    series: [
        { "name": "Money A", "data": [60000, 40000, 80000, 70000] },
        { "name": "Money B", "data": [40000, 30000, 70000, 65000] },
        { "name": "Money C", "data": [8000, 3000, 10000, 6000] },
        { "name": "Money D", "data": [15000, 80000, 60000, 40000] },
        { "name": "Money E", "data": [80000, 60000, 40000, 20000] }
     ]
}, {
    width: '100%',
    height: '300px',
    fullWidth: true,
    chartPadding: {
        right: 40
    },
    plugins: [
        Chartist.plugins.legend({
        })
    ]
});