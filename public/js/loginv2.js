const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');
signUpButton.addEventListener('click', () => {
    container.classList.add('right-panel-active');
});
signInButton.addEventListener('click', () => {
    container.classList.remove('right-panel-active');
});
var url_string = window.location.href;
var url = new URL(url_string);
var c = url.searchParams.get("status");
var mysignup = url.searchParams.get("signup");
var mylogin = url.searchParams.get("login");
if (mysignup != undefined && mysignup != null) {
    container.classList.add('right-panel-active');
    document.getElementById("msgCreate").innerHTML = "Email or phone already registered!";
}
if (mylogin != undefined && mylogin != null) {
    document.getElementById("msgCreate3").innerHTML = "Wrong user combination!";
}
$(document).ready(function() {
    // $.post("/auth/cookie.json", {}, function (result) {
    //     console.log(result);
    //     $("#authenticity_token").val(result.token);
    // });
    $("#_token").val(getParameterByName("token"));
});
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

