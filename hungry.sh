#!/bin/bash

# Display the top 10 CPU consuming processes
echo "Top CPU consuming processes:"
echo "============================"
top -b -n 1 | head -17 | tail -11

# Alternatively, using 'ps' command to sort processes by CPU usage
echo -e "\nAlternative method using ps:"
echo "============================="
ps -eo pid,ppid,cmd,%cpu --sort=-%cpu | head -11
